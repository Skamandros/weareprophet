/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.settings;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;
import de.ggj14.wap.common.I18NProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;

import java.util.Locale;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class I18NBundleTest {
    private static final Logger LOGGER = LogManager.getLogger(I18NBundleTest.class);

    private static final SortedSet<String> expectedProperties = new TreeSet<>();

    @BeforeClass
    public static void determineExpectedProperties() {
        Reflections reflections = new Reflections(
                new ConfigurationBuilder().forPackages("de.weareprophet.client", "de.ggj14.wap.common"));
        Set<Class<? extends I18NProperty>> types = reflections.getSubTypesOf(I18NProperty.class);

        for(final Class<? extends I18NProperty> t : types) {
            I18NProperty[] enumConstants = t.getEnumConstants();
            if(enumConstants != null) {
                for(I18NProperty p : enumConstants) {
                    expectedProperties.add(p.getKey());
                }
            }
        }
        LOGGER.info("Expected properties: {}", expectedProperties);
    }

    @Test
    public void english() {
        Assert.assertFalse(expectedProperties.isEmpty());
        I18NBundle i18NBundle = I18NBundle.createBundle(new FileHandle("assets/" + SettingsPersistenceHelper.I18N_BUNDLE_NAME), Locale.ENGLISH);
        for(String expected : expectedProperties) {
            Assert.assertNotNull("Expected property '" + expected + "' not found in bundle", i18NBundle.get(expected));
        }
    }

    @Test
    public void german() {
        Assert.assertFalse(expectedProperties.isEmpty());
        I18NBundle i18NBundle = I18NBundle.createBundle(new FileHandle("assets/" + SettingsPersistenceHelper.I18N_BUNDLE_NAME), Locale.GERMAN);
        for(String expected : expectedProperties) {
            Assert.assertNotNull("Expected property '" + expected + "' not found in bundle", i18NBundle.get(expected));
        }
    }

}
