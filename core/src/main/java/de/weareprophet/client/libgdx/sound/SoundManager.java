/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.sound;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import de.ggj14.wap.common.I18NProperty;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SoundManager {
    public enum Sound implements I18NProperty {
        MOVE_MINION,
        SELECT_MINION,
        DESELECT_MINION,
        GAME_COUNTDOWN_4_TO_1,
        GAME_COUNTDOWN_0,
        TUTORIAL_SELECT_PROPHET,
        TUTORIAL_EXPLAIN_CHILD,
        TUTORIAL_EXPLAIN_CONVERSATION,
        TUTORIAL_MULTISELECT,
        TUTORIAL_MULTISELECT_DONE,
        TUTORIAL_TAB_KEY,
        TUTORIAL_TAB_KEY_DONE,
        TUTORIAL_SPEED_DEMO,
        TUTORIAL_PERSUASION_DEMO_EXPLANATION,
        TUTORIAL_PERSUASION_DEMO_SUCCESS,
        TUTORIAL_POWER_DEMO_INTRO,
        TUTORIAL_POWER_DEMO_EXPLANATION_1,
        TUTORIAL_POWER_DEMO_EXPLANATION_2,
        TUTORIAL_POWER_DEMO_EXPLANATION_3,
        TUTORIAL_POWER_DEMO_EXPLANATION_4,
        TUTORIAL_MAP_DOMINATION_EXPLANATION,
        TUTORIAL_SUCCESS_MESSAGE,
        TUTORIAL_PERSUASION_DEMO_FAILED,
        TUTORIAL_MAP_DOMINATION_DEMO_FAILED,
        TUTORIAL_INVULNERABILITY_DEMO_PROPHET_EXPLANATION,
        TUTORIAL_INVULNERABILITY_DEMO_RECAPTURE_EXPLANATION,
        TUTORIAL_INVULNERABILITY_DEMO_COMPLETE,
        TUTORIAL_INVULNERABILITY_DEMO_FAILED,
        ;
    }

    private final Map<Sound, com.badlogic.gdx.audio.Sound> sounds = new ConcurrentHashMap<>();
    private final Music music;
    private final SettingsPersistenceHelper settingsHelper;

    public SoundManager(final SettingsPersistenceHelper settingsHelper) {
        this.settingsHelper = settingsHelper;
        music = Gdx.audio.newMusic(Gdx.files.internal("sounds/bu-offensive-birds.ogg"));
        music.setLooping(true);
        music.setVolume(settingsHelper.getSettings().getVolume());
        if(settingsHelper.getSettings().isMusicOn()) {
            music.play();
        }
        rebuildSoundCache();
    }

    public void rebuildSoundCache() {
        this.sounds.values().parallelStream().forEach(com.badlogic.gdx.audio.Sound::dispose);
        this.sounds.clear();
        Arrays.stream(Sound.values()).parallel().forEach(s -> this.sounds.put(s, Gdx.audio.newSound(Gdx.files.internal("sounds/" + settingsHelper.getLocalizedProperty(s)))));
    }

    public void setMusicEnabled(final boolean enableMusic) {
        if(enableMusic) {
            music.play();
        } else {
            music.pause();
        }
    }

    public void setVolume(final float volumeToSet) {
        music.setVolume(volumeToSet);
    }

    public void dispose() {
        music.stop();
        music.dispose();
    }

    public void playSound(Sound sound) {
        if(settingsHelper.getSettings().isSoundOn()) {
            playSound(sound, settingsHelper.getSettings().getVolume());
        }
    }

    private void playSound(Sound sound, float volume) {
        sounds.get(sound).play(volume);
    }

    public void stopSound(Sound sound) {
        sounds.get(sound).stop();
    }
}
