/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.datamodel.Faction;
import de.weareprophet.client.LocalOnlyGameState;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.connectivity.Server;
import de.weareprophet.client.libgdx.frontend.WapFrontend;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A game screen which shows the tutorial.
 */
public class TutorialScreen extends ScreenAdapter {
    private static final Logger LOG = LogManager.getLogger(TutorialScreen.class);

    private final WapGame game;
    private final TutorialOverlay tutorialOverlay;
    private final ScreenHelper screenHelper;
    private FrameBuffer frameBuffer;
    private LocalOnlyGameState gameState;
    private WapFrontend frontend;

    private final Faction myFaction = Faction.PLAYER1;
    private final GameInformation gameInformation;

    private SpriteBatch spriteBatch;
    private ShapeRenderer shapeRenderer;
    private ScheduledExecutorService updateExecutor;

    public TutorialScreen(final WapGame game, final GameInformation gameInformation) {
        super();
        this.game = game;
        this.gameInformation = gameInformation;
        this.tutorialOverlay = new TutorialOverlay(game.getSettingsPersistenceHelper(), game.getFont12flipped(), game.getFont12boldFlipped(), game.getFontShader());
        this.screenHelper = new ScreenHelper(game, Server.offline().getConnector());
    }

    @Override
    public void show() {
        LOG.info("Entering Tutorial");
        screenHelper.show();
        this.frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, WapGame.WORLD_WIDTH, WapGame.WORLD_HEIGHT, false);
        game.getSoundManager().setVolume(game.getSettingsPersistenceHelper().getSettings().getVolume() * 0.2f);
        frontend = new WapFrontend(game.getFont12(), game.getFont48(), game.getFontShader(),
                game.getSettingsPersistenceHelper());
        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(game.getViewport().getCamera().combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(game.getViewport().getCamera().combined);

        gameState = new LocalOnlyGameState();
        gameState.initTutorial();

        frontend.init(() -> gameState, myFaction, gameInformation, game.getSoundManager(), game.getViewport(),
                m -> gameState.doMoveAction(m));

        final Update updateCommand = new Update();
        tutorialOverlay.show(gameState, frontend, updateCommand);
        final InputMultiplexer inputProcessor = new InputMultiplexer();
        inputProcessor.addProcessor(screenHelper.getIngameInputHandler());
        inputProcessor.addProcessor(tutorialOverlay.getInputProcessor());
        inputProcessor.addProcessor(frontend.getInputProcessor());
        Gdx.input.setInputProcessor(inputProcessor);

        updateExecutor = Executors.newScheduledThreadPool(1);
        updateExecutor.scheduleAtFixedRate(updateCommand, 0, 10, TimeUnit.MILLISECONDS);
    }

    @Override
    public void hide() {
        LOG.info("Leaving Tutorial");
        game.getSoundManager().setVolume(game.getSettingsPersistenceHelper().getSettings().getVolume());
        spriteBatch.dispose();
        shapeRenderer.dispose();
        tutorialOverlay.hide();
        screenHelper.hide();
        if(updateExecutor != null) {
            updateExecutor.shutdownNow();
            try {
                if(!updateExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
                    LOG.error("Update executor didn't shut down in time");
                }
            } catch(InterruptedException e) {
                LOG.warn("Update executor shutdown interrupted");
            }
        }
        frameBuffer.dispose();
    }

    @Override
    public synchronized void render(float deltaInSeconds) {
        try {
            frameBuffer.begin();
            Gdx.gl.glClearColor(0, 0, 0, 0);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            frontend.render(spriteBatch, shapeRenderer);
            screenHelper.render(deltaInSeconds);
            frameBuffer.end();
            Gdx.gl.glEnable(Gdx.gl20.GL_BLEND);
            final Texture maskTexture = tutorialOverlay.renderToMask();
            game.getViewport().apply(); // https://stackoverflow.com/a/28752311
            spriteBatch.begin();
            spriteBatch.draw(frameBuffer.getColorBufferTexture(), 0, 0, WapGame.WORLD_WIDTH, WapGame.WORLD_HEIGHT, 0, 0,
                    1, 1);
            spriteBatch.draw(maskTexture, 0, 0);
            spriteBatch.end();
            final Texture overlayTexture = tutorialOverlay.renderToOverlay();
            game.getViewport().apply(); // https://stackoverflow.com/a/28752311
            spriteBatch.begin();
            spriteBatch.draw(overlayTexture, 0, 0);
            spriteBatch.end();
            Gdx.gl.glDisable(Gdx.gl20.GL_BLEND);
        } catch(Exception e) {
            LOG.fatal("Uncaught exception during rendering", e);
            throw e;
        }
        if(tutorialOverlay.isFinished()) {
            game.switchToTitleScreen();
        }
    }

    final class Update implements Runnable {
        private long lastUpdate = System.nanoTime();

        private final AtomicBoolean activeFrontendUpdates = new AtomicBoolean(false);

        @Override
        public void run() {
            try {
                final long currentTime = System.nanoTime();
                final int deltaInMillis = (int) (currentTime - lastUpdate) / 1_000_000;

                synchronized(TutorialScreen.this) {
                    if(activeFrontendUpdates.get()) {
                        frontend.update(deltaInMillis, gameState);
                    }
                    tutorialOverlay.update(deltaInMillis);
                }

                lastUpdate = currentTime;
            } catch(Exception e) {
                LOG.warn("Exception during tutorial screen update", e);
            }
        }

        public void disableFrontendUpdates() {
            activeFrontendUpdates.set(false);
            LOG.debug("Enabled frontend updates");
        }

        public void enableFrontendUpdates() {
            activeFrontendUpdates.set(true);
            LOG.debug("Disabled frontend updates");
        }
    }

}
