/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.frontend;

import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.datamodel.Faction;
import de.weareprophet.client.libgdx.WapGame;

import java.awt.geom.Point2D;

public class FrontendUtils {
    public static Point2D.Float calculateStartingPoint(Faction faction) {
        int x = 0;
        int y = 0;

        if(faction == Faction.PLAYER1) {
            x = CommonValues.WIDTH_OF_MAP_ELEMENT / 2;
            y = CommonValues.HEIGHT_OF_MAP_ELEMENT / 2;
        } else if(faction == Faction.PLAYER2) {
            x = CommonValues.WIDTH_OF_MAP_ELEMENT * CommonValues.NUMBER_OF_COLUMNS - CommonValues.WIDTH_OF_MAP_ELEMENT / 2;
            y = CommonValues.HEIGHT_OF_MAP_ELEMENT * CommonValues.NUMBER_OF_ROWS - CommonValues.HEIGHT_OF_MAP_ELEMENT / 2;
        } else {
            x = CommonValues.WIDTH_OF_MAP_ELEMENT * CommonValues.NUMBER_OF_COLUMNS / 2;
            y = CommonValues.HEIGHT_OF_MAP_ELEMENT * CommonValues.NUMBER_OF_ROWS / 2;
        }
        Point2D.Float p = new Point2D.Float(x, y);
        return p;
    }

    /**
     * In the original prophet implementation an engine was used where the top left point is 0,0. Now, using LibGDX this
     * point is in the bottom left corner which would break several things. Thus, all ingame Y coordinates need to be
     * flipped which is accomplished by this method.
     *
     * @param yFromTopLeft The y value as expected from the top edge.
     * @return The y value from the bottom as needed by LibGDX.
     */
    public static float flipY(final float yFromTopLeft) {
        return (float) WapGame.WORLD_HEIGHT - yFromTopLeft;
    }
}
