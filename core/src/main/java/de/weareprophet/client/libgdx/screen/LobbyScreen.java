/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import de.ggj14.wap.common.communication.dto.LobbySettings;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.MatchMode;
import de.ggj14.wap.common.datamodel.lobby.Lobby;
import de.ggj14.wap.common.datamodel.lobby.LobbyPhase;
import de.ggj14.wap.common.server.ProphetConnector;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.SortedSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class LobbyScreen extends ScreenAdapter {
    private static final Logger LOG = LogManager.getLogger(TitleScreen.class);
    private static final int DEFAULT_PADDING = 10;
    /**
     * How often the lobby state is refreshed, in milliseconds.
     */
    private static final int REFRESH_INTERVAL_MILLIS = 100;


    private final Skin skin;
    private final ProphetConnector connector;
    private final WapGame game;
    private final ScheduledExecutorService executor;
    private final Faction assignedFaction;
    private final Stage stage;
    private final ScreenHelper screenHelper;
    private List<String> observerList;
    private Label player1name;
    private Label player2name;
    private LobbyRefresh refreshRunnable;
    private SelectBox<MatchModeSelectionItem> matchMode;
    private CheckBox player2ready;
    private CheckBox tournamentMode;
    private TextButton readyButton;
    private Label score1;
    private Label score2;
    private VerticalGroup bottomRight;
    private boolean rejectedWindowShownBefore = false;
    private TextTooltip matchModeTooltip;

    public LobbyScreen(WapGame game,
                       ProphetConnector serverConnector,
                       Skin skin,
                       ScheduledExecutorService executor,
                       Faction assignedFaction) throws InterruptedException {
        if(assignedFaction == null) {
            throw new NullPointerException("Faction must not be null");
        }
        this.skin = skin;
        this.connector = serverConnector;
        this.game = game;
        this.executor = executor;
        this.assignedFaction = assignedFaction;
        this.screenHelper = new ScreenHelper(game, serverConnector);
        stage = new Stage(game.getViewport());
        final SettingsPersistenceHelper settingsPersistenceHelper = game.getSettingsPersistenceHelper();
        stage.setDebugTableUnderMouse(settingsPersistenceHelper.getSettings().isDebugMode());
        buildUI(settingsPersistenceHelper);
        LOG.info("Switched to lobby screen for lobby {} as faction {}", connector.getCurrentLobby().getId(),
                assignedFaction);
    }

    @Override
    public void show() {
        this.screenHelper.show();
        final InputMultiplexer inputProcessor = new InputMultiplexer();
        inputProcessor.addProcessor(this.screenHelper.getLobbyInputHandler());
        inputProcessor.addProcessor(stage);
        Gdx.input.setInputProcessor(inputProcessor);
        refreshRunnable = new LobbyRefresh();
        executor.execute(refreshRunnable);
        super.show();
    }

    @Override
    public void hide() {
        this.screenHelper.hide();
        refreshRunnable.disable();
        Gdx.input.setInputProcessor(null);
        super.hide();
    }


    @Override
    public void render(float deltaInSeconds) {
        try {
            try {
                final Lobby lobby = connector.getCurrentLobby();
                if(lobby.getCurrentPhase().equals(LobbyPhase.INGAME)) {
                    try {
                        game.setScreen(new MultiplayerScreen(game, this, connector, connector.getGameInformation(),
                                assignedFaction));
                    } catch(Exception e) {
                        LOG.error("Unable to switch to multiplayer screen", e);
                    }
                } else if(lobby.getCurrentPhase().equals(LobbyPhase.REJECTED) && !rejectedWindowShownBefore) {
                    rejectedWindowShownBefore = true;
                    final Dialog dialog = new Dialog("Challenge rejected", skin);
                    dialog.text(
                            new Label(
                                    lobby.getPlayer2().orElse("The challenged player") + " has rejected the challenge.",
                                    skin));
                    final TextButton ok = new TextButton("Ok", skin);
                    ok.addListener(new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            game.switchToTitleScreen();
                            super.clicked(event, x, y);
                        }
                    });
                    dialog.button(ok);
                    dialog.show(stage);
                }
            } catch(InterruptedException e) {
                LOG.error("Lobby state unavailable", e);
            }

            stage.act(deltaInSeconds);
            synchronized(this) {
                stage.draw();
            }
            screenHelper.render(deltaInSeconds);
        } catch(Exception e) {
            LOG.fatal("Uncaught exception during rendering", e);
            synchronized(this) {
                refreshRunnable.disable();
            }
            Gdx.app.exit();
            throw e;
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
        super.dispose();
    }

    private void buildUI(SettingsPersistenceHelper sph) throws InterruptedException {
        Image header = new Image(new Texture(Gdx.files.internal("We-Are-Prophet1.png")));
        final Table rootTable = new Table();
        rootTable.top();
        rootTable.setFillParent(true);

        player2ready = new CheckBox(" " + sph.getLocalizedProperty(LobbyScreenText.OPPONENT_READY), skin);
        player2ready.setDisabled(false);
        player2ready.setTouchable(Touchable.disabled);

        rootTable.add(header).colspan(3).padTop(DEFAULT_PADDING);
        rootTable.row();

        final Label titleLabel = new Label(sph.getLocalizedProperty(LobbyScreenText.MULTIPLAYER_LOBBY), skin, "title");
        rootTable.add(titleLabel).colspan(3).center().top().padTop(DEFAULT_PADDING);
        rootTable.row().padTop(DEFAULT_PADDING);

        addPlayerNames(sph, rootTable);
        rootTable.row().padTop(DEFAULT_PADDING);

        addScoreLabels(rootTable);
        rootTable.row().padTop(5 * DEFAULT_PADDING);

        addObserversAndSettings(sph, rootTable);

        rootTable.row().padTop(DEFAULT_PADDING);
        rootTable.add().colspan(3).expand().row();

        addFooterButtons(sph, rootTable);
        stage.addActor(rootTable);
    }

    private void addPlayerNames(SettingsPersistenceHelper sph, Table rootTable) {
        final Window player1area = new Window(sph.getLocalizedProperty(LobbyScreenText.PROPHET_ORANGE), skin);
        player1area.setColor(WapGame.FACTION_1_OVERLAY_COLOR);
        player1area.setMovable(false);
        player1name = new Label("", skin, "large");
        player1area.add(player1name);
        rootTable.add(player1area).width(480).right();

        final Label vs = new Label("vs", skin, "title-plain");
        rootTable.add(vs).width(20).center();

        final Window player2area = new Window(sph.getLocalizedProperty(LobbyScreenText.PROPHET_BLUE), skin);
        player2area.setColor(WapGame.FACTION_2_OVERLAY_COLOR);
        player2area.setMovable(false);
        player2name = new Label("", skin, "large");
        player2area.add(player2name);
        rootTable.add(player2area).width(480).left();
    }

    private void addScoreLabels(Table rootTable) {
        score1 = new Label("0", skin, "lobby-score1");
        score1.setAlignment(Align.center);
        score2 = new Label("0", skin, "lobby-score2");
        score2.setAlignment(Align.center);
        rootTable.add(score1).width(80);
        rootTable.add(new Label(":", skin, "large"));
        rootTable.add(score2).width(80);
    }

    private void addObserversAndSettings(SettingsPersistenceHelper sph, Table rootTable) throws InterruptedException {
        final Label observersTitle = new Label(sph.getLocalizedProperty(LobbyScreenText.OBSERVERS), skin, "title-plain");
        observersTitle.setAlignment(Align.center);
        final Label settingsTitle = new Label(sph.getLocalizedProperty(LobbyScreenText.MATCH_SETTINGS), skin, "title-plain");
        settingsTitle.setAlignment(Align.center);

        rootTable.add(settingsTitle);
        rootTable.add();
        rootTable.add(observersTitle);
        rootTable.row().padTop(DEFAULT_PADDING);

        final VerticalGroup settingsGroup = new VerticalGroup();
        settingsGroup.space(DEFAULT_PADDING);
        settingsGroup.wrapSpace(DEFAULT_PADDING);

        tournamentMode = new CheckBox(" " + sph.getLocalizedProperty(LobbyScreenText.TOURNAMENT_MODE), skin, "switch");
        final boolean settingsCanBeChanged = Faction.PLAYER1.equals(assignedFaction)
                && connector.getCurrentLobby().getCurrentPhase().equals(LobbyPhase.SETUP);

        tournamentMode.setTouchable(settingsCanBeChanged ? Touchable.enabled : Touchable.disabled);
        tournamentMode.addListener(new TextTooltip(sph.getLocalizedProperty(LobbyScreenText.TOURNAMENT_MODE_EXPLANATION), skin));
        final ChangeListener changeSettingsListener = new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                connector.lobbyChangeSettings(
                        new LobbySettings(matchMode.getSelected().matchMode, tournamentMode.isChecked()));
            }
        };
        if(settingsCanBeChanged) {
            tournamentMode.addListener(changeSettingsListener);
        }
        settingsGroup.addActor(tournamentMode);

        matchMode = new SelectBox<MatchModeSelectionItem>(skin);
        final MatchMode[] matchModes = MatchMode.values();
        final MatchModeSelectionItem[] matchModeSelectionItems = new MatchModeSelectionItem[matchModes.length];
        for(int i = 0; i < matchModes.length; i++) {
            matchModeSelectionItems[i] = new MatchModeSelectionItem(matchModes[i], sph);
        }
        matchMode.setItems(matchModeSelectionItems);
        matchMode.setTouchable(settingsCanBeChanged ? Touchable.enabled : Touchable.disabled);
        matchModeTooltip = new TextTooltip(matchMode.getSelected().description, skin);
        matchMode.addListener(matchModeTooltip);
        if(settingsCanBeChanged) {
            matchMode.addListener(changeSettingsListener);
        }
        settingsGroup.addActor(matchMode);

        rootTable.add(settingsGroup).top().pad(DEFAULT_PADDING);
        rootTable.add();
        observerList = new List<String>(skin, "dimmed");
        observerList.setItems();
        final ScrollPane observerPane = new ScrollPane(observerList, skin);
        observerPane.setFadeScrollBars(false);
        rootTable.add(observerPane).width(200).height(120);
    }

    private void addFooterButtons(SettingsPersistenceHelper sph, Table rootTable) {
        final TextButton leaveButton = new TextButton(sph.getLocalizedProperty(LobbyScreenText.LEAVE_LOBBY), skin, "round");
        leaveButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.switchToTitleScreen();
                connector.leaveLobby();
                super.clicked(event, x, y);
            }
        });

        rootTable.add(leaveButton).left().bottom().padLeft(20).padBottom(20);
        rootTable.add();
        bottomRight = new VerticalGroup();
        if(Faction.PLAYER1.equals(assignedFaction)) {
            bottomRight.addActor(player2ready);
            final TextButton startButton = new TextButton(sph.getLocalizedProperty(LobbyScreenText.START_GAME), skin, "round");
            startButton.addListener(
                    new ClickListener() {
                        @Override
                        public void clicked(InputEvent event, float x, float y) {
                            connector.lobbyStartGame();
                            super.clicked(event, x, y);
                        }
                    }
            );
            bottomRight.addActor(startButton);

        } else if(Faction.PLAYER2.equals(assignedFaction)) {
            readyButton = new TextButton(sph.getLocalizedProperty(LobbyScreenText.READY), skin, "toggle");
            readyButton.addListener(
                    new ChangeListener() {
                        @Override
                        public void changed(ChangeEvent event, Actor actor) {
                            connector.lobbyChangeReadyState(readyButton.isChecked());
                        }
                    }
            );
            final Table table = new Table();
            table.add(readyButton).width(120).height(30);
            bottomRight.addActor(table);
        }
        rootTable.add(bottomRight).right().bottom().padRight(20).padBottom(20);
    }

    private final class MatchModeSelectionItem {
        private final MatchMode matchMode;
        private final String label;

        private final String description;

        MatchModeSelectionItem(MatchMode matchMode, SettingsPersistenceHelper settingsPersistenceHelper) {
            this.matchMode = matchMode;
            this.label = settingsPersistenceHelper.getLocalizedProperty(matchMode.getLabelProperty());
            this.description = settingsPersistenceHelper.getLocalizedProperty(matchMode.getDescriptionProperty());
        }


        @Override
        public String toString() {
            return label;
        }
    }

    private final class LobbyRefresh implements Runnable {
        private int recentFailures = 0;
        private boolean disabled = false;

        @Override
        public void run() {
            LOG.debug("Lobby refresh");

            if(disabled) {
                return;
            }
            try {
                synchronized(LobbyScreen.this) {
                    if(!connector.isConnected()) {
                        connector.connect(game.getSettingsPersistenceHelper().getSettings().getPlayerName());
                    }
                    final Lobby lobby = connector.getCurrentLobby();
                    player1name.setText(lobby.getPlayer1().orElse(""));
                    player2name.setText(lobby.getPlayer2().orElse(""));
                    final SortedSet<String> observers = lobby.getObservers();
                    observerList.setItems(observers == null ? new String[0] : observers.toArray(new String[0]));
                    for(MatchModeSelectionItem i : matchMode.getItems()) {
                        if(i.matchMode.equals(lobby.getSettings().matchMode())) {
                            matchMode.setSelected(i);
                            matchModeTooltip.getActor().setText(i.description);
                            break;
                        }
                    }
                    tournamentMode.setChecked(lobby.getSettings().tournamentModeOn());
                    player2ready.setChecked(lobby.isReady());
                    if(readyButton != null) {
                        readyButton.setChecked(lobby.isReady());
                    }
                    score1.setText(lobby.getScoreOfPlayer1());
                    score2.setText(lobby.getScoreOfPlayer2());
                    if(assignedFaction.equals(Faction.PLAYER1) && !lobby.getCurrentPhase().equals(LobbyPhase.SETUP)) {
                        // We are the host, but the setup phase is over
                        tournamentMode.setTouchable(Touchable.disabled);
                        matchMode.setTouchable(Touchable.disabled);
                    }
                    if(LobbyPhase.FINISHED.equals(lobby.getCurrentPhase())) {
                        bottomRight.remove();
                    }
                }
            } catch(Exception e) {
                recentFailures++;
                LOG.warn("Connection failed.", e);
            } finally {
                if(!disabled) {
                    final int nextRefresh = Math.min(10_000, REFRESH_INTERVAL_MILLIS << recentFailures);
                    LOG.debug("Scheduling next refresh in: " + nextRefresh + "s");
                    executor.schedule(this, nextRefresh, TimeUnit.MILLISECONDS);
                }
            }
        }

        public void disable() {
            this.disabled = true;
        }
    }

}
