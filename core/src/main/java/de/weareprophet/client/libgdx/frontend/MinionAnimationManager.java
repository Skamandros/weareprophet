/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.frontend;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.carrotsearch.hppc.IntObjectHashMap;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.GameStateProvider;
import de.ggj14.wap.common.datamodel.MinionType;
import de.ggj14.wap.common.datamodel.conversation.Conversation;
import de.ggj14.wap.common.gamefield.objects.Minion;

public class MinionAnimationManager {

    public static final int SELECTION_IMAGE_OFFSET = 2;
    private final GameStateProvider gameStateProvider;
    private final Interaction interactionManager;
    private final boolean debugMode;
    private final BitmapFont font;
    private final ShaderProgram fontShader;
    private float stateTime = 0f;

    private static class MinionGraphic {

        /**
         * holds all animations [faction][MinionType]
         */
        private Animation<TextureRegion>[][] animations;

        private Texture[] selectionTextures;

        /**
         * loads the images and so on
         */
        public void loadData() {

            animations = new Animation[Faction.values().length][MinionType.values().length];
            selectionTextures = new Texture[MinionType.values().length];

            for(MinionType miniType : MinionType.values()) {
                final Animation<TextureRegion> greyAnimation = getAnimation(miniType, miniType.getGreyFile());
                final Animation<TextureRegion> orangeAnimation = getAnimation(miniType, miniType.getOrangeFile());
                final Animation<TextureRegion> blueAnimation = getAnimation(miniType, miniType.getBlueFile());

                // player 1 is orange
                animations[Faction.NEUTRAL.ordinal()][miniType.ordinal()] = greyAnimation;
                animations[Faction.PLAYER1.ordinal()][miniType.ordinal()] = orangeAnimation;
                animations[Faction.PLAYER2.ordinal()][miniType.ordinal()] = blueAnimation;

                selectionTextures[miniType.ordinal()] = new Texture(Gdx.files.internal(miniType.getSelectedFile()));
            }
        }

        private Animation<TextureRegion> getAnimation(final MinionType minionType, final String minionAnimationFile) {
            final Texture greySpriteSheet = new Texture(Gdx.files.internal(minionAnimationFile));
            TextureRegion[][] frameSplit = TextureRegion.split(greySpriteSheet, minionType.getWidth(),
                    minionType.getHeight());
            TextureRegion[] animationFrames = new TextureRegion[2];
            animationFrames[0] = frameSplit[0][0];
            animationFrames[1] = frameSplit[0][1];
            return new Animation<TextureRegion>(0.35f, animationFrames);
        }

        /**
         * draws the specified minion
         */
        public void drawMinion(final SpriteBatch spriteBatch, final float elapsedTime, final Minion m) {
            // we offset the animation times for each minion to increase diversity
            final float timeWithOffset = elapsedTime + m.getId() * 32;
            final TextureRegion currentFrame = animations[m.getFaction().ordinal()][m.getMinionType().ordinal()].getKeyFrame(
                    timeWithOffset, true);
            spriteBatch.draw(currentFrame, m.getX() - m.getMinionType().getWidth() / 2f,
                    FrontendUtils.flipY(m.getY()) - m.getMinionType().getHeight() / 2f);
        }

        Texture getSelectionTexture(MinionType minionType) {
            return selectionTextures[minionType.ordinal()];
        }

    }

    /**
     * miniongraphics
     **/
    private final MinionGraphic mg;

    /**
     * emiter map
     **/
    private final IntObjectHashMap<Aura> auraMap;

    /**
     *
     **/
    private final Color selectColor;
    private final Color auraColor1;
    private final Color auraColor2;

    private final Texture[] bubbles;

    public MinionAnimationManager(final GameStateProvider gsp, final Interaction interactionManager, final boolean debugMode, final BitmapFont font, final ShaderProgram fontShader) {
        this.debugMode = debugMode;
        this.font = font;
        this.fontShader = fontShader;
        // selectColor = new Color(66 / 255f, 255 / 255f, 122 / 255f, 150 / 255f);
        selectColor = new Color(255 / 255f, 255 / 255f, 255 / 255f, 1f);
        auraColor1 = new Color(255 / 255f, 141 / 255f, 12 / 255f, 255 / 255f); // rgba(255,141,12, 188)
        auraColor2 = new Color(0 / 255f, 114 / 255f, 243 / 255f, 255 / 255f); // Farbe Team 2: rgba(12,126,255,188)

        bubbles = new Texture[10];
        bubbles[0] = new Texture(Gdx.files.internal("bubble/bubble_alert.png"));
        bubbles[1] = new Texture(Gdx.files.internal("bubble/bubble_burger.png"));
        bubbles[2] = new Texture(Gdx.files.internal("bubble/bubble_happy.png"));
        bubbles[3] = new Texture(Gdx.files.internal("bubble/bubble_love.png"));
        bubbles[4] = new Texture(Gdx.files.internal("bubble/bubble_positive.png"));
        bubbles[5] = new Texture(Gdx.files.internal("bubble/bubble_sad.png"));
        bubbles[6] = new Texture(Gdx.files.internal("bubble/bubble_scream.png"));
        bubbles[7] = new Texture(Gdx.files.internal("bubble/bubble_silent.png"));
        bubbles[8] = new Texture(Gdx.files.internal("bubble/bubble_sing.png"));
        bubbles[9] = new Texture(Gdx.files.internal("bubble/bubble_sword.png"));

        auraMap = new IntObjectHashMap<Aura>();
        for(Minion mini : gsp.getGameState().getMinionMap().values()) {
            addMinion(mini);
        }
        gameStateProvider = gsp;
        this.interactionManager = interactionManager;
        mg = new MinionGraphic();
        mg.loadData();
        stateTime = 0f;
    }

    /**
     * add a minion to the aura map
     *
     * @param m The minion to add.
     */
    public void addMinion(Minion m) {
        auraMap.put(m.getId(), new Aura());
    }

    /**
     * update all animation data
     */
    public void update(int deltaInMillis, GameState gs) {
        stateTime += deltaInMillis / 1_000f;
        for(Minion mini : gs.getMinionMap().values()) {
            if(!auraMap.containsKey(mini.getId())) {
                addMinion(mini);
            }
            Aura aura = auraMap.get(mini.getId());
            aura.update(deltaInMillis, mini.getX(), mini.getY());
            if(aura.maySpawn()) {
                aura.resetSpawnCd();
                if(mini.getMinionType() == MinionType.UBER_PROPHET) {
                    aura.addParticle((int) mini.getX(), (int) mini.getY() + 5,
                            mini.getFaction() == Faction.PLAYER1 ? auraColor1 : auraColor2);
                } else if(mini.isInvulnerable(gs.getTime())) {
                    if(mini.getFaction().equals(Faction.PLAYER1)) {
                        aura.addParticle((int) mini.getX(), (int) mini.getY() + 5, auraColor1);
                    } else if(mini.getFaction().equals(Faction.PLAYER2)) {
                        aura.addParticle((int) mini.getX(), (int) mini.getY() + 5, auraColor2);
                    }
                }
            }
        }
    }

    public void render(final SpriteBatch spriteBatch, final ShapeRenderer shapeRenderer) {

        final GameState gs = gameStateProvider.getGameState();


        // render the minions which are not selected first...
        for(Minion mini : gs.getMinionMap().values().stream().filter(
                m -> !interactionManager.getSelectedMinionIds().contains(m.getId())).toList()) {
            renderMinion(spriteBatch, shapeRenderer, gs, mini);
        }
        // ...and then the selected minions
        for(Minion mini : gs.getMinionMap().values().stream().filter(
                m -> interactionManager.getSelectedMinionIds().contains(m.getId())).toList()) {
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            /* new variant of selection drawing: */
            spriteBatch.begin();
            spriteBatch.setColor(selectColor);


            spriteBatch.draw(mg.getSelectionTexture(mini.getMinionType()),
                    mini.getX() - mini.getMinionType().getWidth() + SELECTION_IMAGE_OFFSET,
                    FrontendUtils.flipY(mini.getY()) - mini.getMinionType().getHeight() / 2f - SELECTION_IMAGE_OFFSET);
            spriteBatch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            spriteBatch.end();
            Gdx.gl.glDisable(GL20.GL_BLEND);
            renderMinion(spriteBatch, shapeRenderer, gs, mini);
        }
    }

    private void renderMinion(SpriteBatch spriteBatch, ShapeRenderer shapeRenderer, GameState gs, Minion mini) {
        if(!auraMap.containsKey(mini.getId())) {
            addMinion(mini);
        }
        auraMap.get(mini.getId()).render(shapeRenderer);
        spriteBatch.begin();
        mg.drawMinion(spriteBatch, stateTime, mini);
        if(gs.getMinionIDConversationMap().containsKey(mini.getId())) {
            Conversation conv = gs.getMinionIDConversationMap().get(mini.getId());
            spriteBatch.draw(bubbles[conv.getId() % 10], mini.getX() + 4,
                    FrontendUtils.flipY(mini.getY()) + mini.getMinionType().getHeight() / 3f);
        }
        if(debugMode) {
            spriteBatch.setShader(fontShader);
            font.setColor(Color.RED);
            font.draw(spriteBatch, "" + mini.getId(), mini.getX() - font.getSpaceXadvance(),
                    FrontendUtils.flipY(mini.getY() + mini.getMinionType().getHeight() / 2f));
            spriteBatch.setShader(null);
        }
        spriteBatch.end();
        if(debugMode) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(Color.CYAN);
            final int offset = 5;
            shapeRenderer.line(mini.getX() - offset, FrontendUtils.flipY(mini.getY()) - offset, mini.getX() + offset, FrontendUtils.flipY(mini.getY()) + offset);
            shapeRenderer.line(mini.getX() - offset, FrontendUtils.flipY(mini.getY()) + offset, mini.getX() + offset, FrontendUtils.flipY(mini.getY()) - offset);
            shapeRenderer.end();
        }
    }
}
