/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.ProphetProperties;
import de.ggj14.wap.common.communication.dto.ProgramVersion;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.lobby.Lobby;
import de.ggj14.wap.common.server.ConnectionFailedException;
import de.ggj14.wap.common.server.JoinFailedException;
import de.ggj14.wap.common.server.LobbyInvitationListener;
import de.ggj14.wap.common.server.ProphetConnector;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.connectivity.Server;
import de.weareprophet.client.libgdx.settings.ClientSettings;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TitleScreen extends ScreenAdapter {
    public static final int SUB_SECTION_PADDING = 20;
    public static final int LINE_PADDING = 5;
    public static final int SUB_SECTION_HEADING_WIDTH = 150;
    private static final Logger LOG = LogManager.getLogger(TitleScreen.class);
    private final WapGame game;
    private final ClientSettings clientSettings;
    private final ScheduledExecutorService executor;
    private final SettingsPersistenceHelper settingsPersistenceHelper;
    private Stage stage;

    private final List<Lobby> lobbies;
    private final List<String> playersOnServer;
    private final Skin skin;
    private final ProgramVersion version;
    private final SelectBox<Server> server;
    private TextTooltip serverTooltip;
    private Server currentServer = Server.offline();
    private GameListRefresh refreshRunnable;

    private Table offlineOverlay;
    private Label connectionError;
    private TextButton toggleFullscreen;
    private TextField playerNameInput;

    public TitleScreen(WapGame game, ClientSettings settings, ProgramVersion version, ScheduledExecutorService executor) {
        this.game = game;
        clientSettings = settings;
        settingsPersistenceHelper = game.getSettingsPersistenceHelper();
        this.skin = game.getSkin();
        this.version = version;
        lobbies = new List<>(skin, "dimmed");
        playersOnServer = new List<>(skin, "dimmed");
        this.executor = executor;
        server = new SelectBox<>(skin);
        final LobbyInvitationListener lobbyInvitationListener = new LobbyInvitationListenerImpl();
        try {
            server.setItems(Server.offline(),
                    Server.kryonet(
                            "EU Central",
                            "weareprophet.de",
                            8091, 8092,
                            executor, version,
                            clientSettings.getKeys(),
                            lobbyInvitationListener),
                    Server.kryonet(
                            "EU Central (Beta)",
                            "weareprophet.de",
                            8093, 8094,
                            executor,
                            version,
                            clientSettings.getKeys(),
                            lobbyInvitationListener),
                    Server.kryonet(
                            "Local Test",
                            "localhost",
                            8091, 8092,
                            executor,
                            version,
                            clientSettings.getKeys(),
                            lobbyInvitationListener));
        } catch(NoSuchAlgorithmException | InvalidKeySpecException e) {
            LOG.error("Unable to load client keys", e);
            throw new IllegalStateException("Invalid client state", e);
        }
        server.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                connectionError.setText("");
                synchronized(refreshRunnable) {
                    refreshRunnable.disable();
                }
                currentServer.getConnector().disconnect();
                currentServer = server.getSelected();
                boolean enableRefresh = false;
                try {
                    currentServer.getConnector().connect(settings.getPlayerName());
                    server.setColor(Color.LIGHT_GRAY);
                    enableRefresh = true;
                } catch(ConnectionFailedException e) {
                    LOG.error("Initial server connection has failed", e);
                    if(e.isRetryMakesSense()) {
                        enableRefresh = true;
                    }
                    clearListsAndDisplayConnectionError(e.getMessage());
                } finally {
                    if(enableRefresh) {
                        refreshRunnable = new GameListRefresh();
                        executor.submit(refreshRunnable);
                    }
                }
            }
        });
    }


    @Override
    public void show() {
        LOG.debug("Switched to title screen");
        stage = new Stage(game.getViewport());
        stage.setDebugTableUnderMouse(clientSettings.isDebugMode());
        final InputMultiplexer inputProcessor = new InputMultiplexer();
        inputProcessor.addProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keyCode) {
                if(keyCode == Input.Keys.ESCAPE) {
                    currentServer.getConnector().disconnect();
                    Gdx.app.exit();
                }
                return true;
            }
        });
        inputProcessor.addProcessor(stage);
        Gdx.input.setInputProcessor(inputProcessor);
        final Table rootTable = new Table();
        rootTable.setFillParent(true);
        stage.addActor(rootTable);
        addMenuItems(rootTable);
        refreshRunnable = new GameListRefresh();
        executor.submit(refreshRunnable);
        if(ClientSettings.DEFAULT_PLAYER_NAME.equals(clientSettings.getPlayerName())) {
            new RequestNicknameDialog(skin, settingsPersistenceHelper, playerNameInput).show(stage);
        }
    }

    @Override
    public void hide() {
        LOG.debug("Leaving title screen");
        refreshRunnable.disable();
        Gdx.input.setInputProcessor(null);
        stage.dispose();
    }

    @Override
    public void render(float delta) {
        try {
            if(Gdx.graphics.isFullscreen() != clientSettings.isFullScreenMode()) {
                if(clientSettings.isFullScreenMode()) {
                    LOG.info("Switching to full screen mode");
                    Graphics.Monitor currMonitor = Gdx.graphics.getMonitor();
                    Graphics.DisplayMode displayMode = Gdx.graphics.getDisplayMode(currMonitor);
                    if(!Gdx.graphics.setFullscreenMode(displayMode)) {
                        LOG.error("Full screen switch has failed");
                        clientSettings.setFullScreenMode(false);
                        settingsPersistenceHelper.saveSettings(clientSettings);
                        toggleFullscreen.setChecked(false);
                    }
                } else {
                    LOG.info("Switching to window mode");
                    Gdx.graphics.setWindowedMode(WapGame.DEFAULT_VIEWPORT_WIDTH, WapGame.DEFAULT_VIEWPORT_HEIGHT);
                }
            }
            offlineOverlay.setVisible(
                    server.getSelectedIndex() == 0 || !server.getSelected().getConnector().isConnected());
            stage.act(delta);
            synchronized(this) {
                stage.draw();
            }
        } catch(Exception e) {
            LOG.fatal("Uncaught exception during rendering", e);
            synchronized(refreshRunnable) {
                refreshRunnable.disable();
            }
            Gdx.app.exit();
            throw e;
        }
    }

    private void addMenuItems(final Table rootTable) {
        Image header = new Image(new Texture(Gdx.files.internal("We-Are-Prophet1.png")));
        Label version;
        try {
            version = new Label(new ProphetProperties().getVersion().toString(), skin);
        } catch(IOException e) {
            LOG.warn("Unable to load version from property file", e);
            version = new Label("", skin);
        }
        version.setX(WapGame.WORLD_WIDTH - 60);
        version.setY(WapGame.WORLD_HEIGHT - 30);
        stage.addActor(version);

        rootTable.add(header).colspan(2);
        rootTable.row();
        final Window singleplayer = buildSingleplayerWindow();
        rootTable.add(singleplayer).width(945).height(110).colspan(2);
        rootTable.row();
        final Window options = buildOptionsWindow();
        rootTable.add(options).width(250).height(480);

        final Stack mpStack = new Stack();
        final Window multiplayer = buildMultiplayerWindow();
        mpStack.add(multiplayer);
        offlineOverlay = new Table();
        final Label offlineModeLabel = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.OFFLINE_MODE), skin, "overlay");
        offlineModeLabel.setAlignment(Align.center);
        offlineOverlay.add(offlineModeLabel).width(662).height(340).padTop(85);
        offlineOverlay.setVisible(server.getSelectedIndex() == 0);
        mpStack.add(offlineOverlay);
        final Table connectionErrorTable = new Table();
        connectionError = new Label("", skin);
        connectionError.setColor(Color.RED);
        connectionErrorTable.add(connectionError).center().padLeft(45);
        mpStack.add(connectionErrorTable);
        rootTable.add(mpStack).width(690).height(480);
    }

    private Window buildMultiplayerWindow() {
        Window multiplayer = new Window(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.MULTIPLAYER), skin);
        multiplayer.setMovable(false);
        multiplayer.clearListeners();

        final Label networkSettings = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.SERVER_SELECTION), skin, "subtitle");
        multiplayer.add(networkSettings).colspan(2);
        multiplayer.row().padTop(LINE_PADDING);

        networkSettings.setAlignment(Align.center);
        multiplayer.add(server).width(200).colspan(2);
        multiplayer.row().padTop(SUB_SECTION_PADDING);

        final Label playersLabel = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.OTHER_PLAYERS), skin, "subtitle");
        multiplayer.add(playersLabel);
        final Label lobbiesLabel = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.LOBBIES), skin, "subtitle");
        multiplayer.add(lobbiesLabel);
        multiplayer.row();

        final ScrollPane playersInLobby = new ScrollPane(playersOnServer, skin);
        playersInLobby.setFadeScrollBars(false);
        multiplayer.add(playersInLobby).width(200).height(245).pad(5);

        final ScrollPane runningGamesContainer = new ScrollPane(lobbies, skin);
        runningGamesContainer.setFadeScrollBars(false);
        multiplayer.add(runningGamesContainer).width(380).height(245).pad(5);
        multiplayer.row().padTop(5);

        final TextButton startGameButton = new TextButton(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.CHALLENGE_PLAYER), skin, "round");
        startGameButton.addListener(startMultiplayerGameListener());
        multiplayer.add(startGameButton);
        final TextButton watchGameButton = new TextButton(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.ENTER_LOBBY), skin, "round");
        watchGameButton.addListener(enterMultiplayerLobbyListener());
        multiplayer.add(watchGameButton);

        return multiplayer;
    }

    private Window buildOptionsWindow() {
        final Window options = new Window(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.OPTIONS), skin);
        options.top();
        options.setMovable(false);
        options.row().padTop(SUB_SECTION_PADDING);

        final Table gameSettingsTable = buildGameSettingsTable();
        final Table soundTable = buildSoundTable();
        final Table mediaTable = buildMediaTable();

        options.add(gameSettingsTable);
        options.row().padTop(SUB_SECTION_PADDING);
        options.add(soundTable);
        options.row().padTop(SUB_SECTION_PADDING);
        options.add(mediaTable);
        return options;
    }

    private Table buildMediaTable() {
        final Table mediaTable = new Table();
        final Label links = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.EXTERNAL_LINKS), skin, "subtitle");
        links.setAlignment(Align.center);
        mediaTable.add(links).width(SUB_SECTION_HEADING_WIDTH);
        final Texture yt = new Texture(Gdx.files.internal("browser24.png"));
        final Button youtubeVideo = new Button(skin);
        youtubeVideo.add(new Image(yt)).width(24).height(24).padRight(10);
        youtubeVideo.add(new Label("weareprophet.de", skin)).width(110);
        youtubeVideo.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.net.openURI("https://weareprophet.de/");
                super.clicked(event, x, y);
            }
        });
        mediaTable.row().padTop(LINE_PADDING);
        mediaTable.add(youtubeVideo).width(170).height(31);

        final Texture discordIcon = new Texture(Gdx.files.internal("discord24.png"));
        final Button discordButton = new Button(skin);
        discordButton.add(new Image(discordIcon)).width(24).height(24).padRight(10);
        discordButton.add(new Label("Discord Server", skin)).width(110);
        discordButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.net.openURI("https://discord.gg/KZVB5hGx5j");
                super.clicked(event, x, y);
            }
        });
        mediaTable.row().padTop(LINE_PADDING);
        mediaTable.add(discordButton).width(170).height(31);
        return mediaTable;
    }

    private Table buildSoundTable() {
        final Table soundTable = new Table();
        final Label soundSettings = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.SOUND_SETTINGS), skin, "subtitle");
        soundSettings.setAlignment(Align.center);
        soundTable.add(soundSettings).colspan(4).width(SUB_SECTION_HEADING_WIDTH);
        soundTable.row().padTop(LINE_PADDING);
        final Button musicSwitch = new Button(skin, "music");
        musicSwitch.setChecked(clientSettings.isMusicOn());
        musicSwitch.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                final boolean musicOn = !musicSwitch.isChecked();
                clientSettings.setMusicOn(musicOn);
                settingsPersistenceHelper.saveSettings(clientSettings);
                game.getSoundManager().setMusicEnabled(musicOn);
                return true;
            }
        });

        soundTable.add(musicSwitch).colspan(2);
        final Button soundSwitch = new Button(skin, "sound");
        soundSwitch.setChecked(clientSettings.isSoundOn());
        soundSwitch.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                final boolean soundOn = !soundSwitch.isChecked();
                clientSettings.setSoundOn(soundOn);
                settingsPersistenceHelper.saveSettings(clientSettings);
                return true;
            }
        });
        soundTable.add(soundSwitch).colspan(2).row();

        final Slider volumeSlider = getVolumeSlider();
        soundTable.add(volumeSlider).colspan(4).padTop(LINE_PADDING).row();
        return soundTable;
    }

    private Slider getVolumeSlider() {
        final Slider volumeSlider = new Slider(0f, 1f, 0.01f, false, skin);
        volumeSlider.setValue(clientSettings.getVolume());
        volumeSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                clientSettings.setVolume(volumeSlider.getValue());
                settingsPersistenceHelper.saveSettings(clientSettings);
                game.getSoundManager().setVolume(volumeSlider.getValue());
            }
        });
        return volumeSlider;
    }

    private Table buildGameSettingsTable() {
        final Table gameSettingsTable = new Table();
//        final Label betaSettings = new Label("Beta Settings", skin, "subtitle");
//        betaSettings.setAlignment(Align.center);
        toggleFullscreen = new TextButton(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.FULL_SCREEN), skin, "toggle");
        toggleFullscreen.setChecked(clientSettings.isFullScreenMode());
        toggleFullscreen.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(toggleFullscreen.isChecked()) {
                    clientSettings.setFullScreenMode(true);
                    settingsPersistenceHelper.saveSettings(clientSettings);
                } else {
                    clientSettings.setFullScreenMode(false);
                    settingsPersistenceHelper.saveSettings(clientSettings);
                }
            }
        });
        gameSettingsTable.add(toggleFullscreen).width(140);
        gameSettingsTable.row().padTop(SUB_SECTION_PADDING);

        Label nameLabel = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.NICKNAME), skin, "subtitle");
        nameLabel.setAlignment(Align.center);
        playerNameInput = new TextField(clientSettings.getPlayerName(), skin);
        playerNameInput.setMaxLength(CommonValues.MAX_NICKNAME_LENGTH);
        playerNameInput.setAlignment(Align.center);
        playerNameInput.setOnlyFontChars(true);
        playerNameInput.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                final String setName = playerNameInput.getText();
                // wait for the name to be stable for some time
                executor.schedule(() -> {
                    if(playerNameInput.getText().equals(setName)) {
                        if(!CommonValues.ALLOWED_NICKNAMES.matcher(setName).matches()) {
                            playerNameInput.setColor(Color.RED);
                        } else {
                            playerNameInput.setColor(skin.getColor("white"));
                            clientSettings.setPlayerName(setName);
                            settingsPersistenceHelper.saveSettings(clientSettings);
                            final ProphetConnector connector = server.getSelected().getConnector();
                            connector.disconnect();
                        }
                    }
                }, 500, TimeUnit.MILLISECONDS);
            }
        });
        if(!CommonValues.ALLOWED_NICKNAMES.matcher(playerNameInput.getText()).matches()) {
            playerNameInput.setColor(Color.RED);
        }
        gameSettingsTable.add(nameLabel).width(SUB_SECTION_HEADING_WIDTH);
        gameSettingsTable.row().padTop(LINE_PADDING);
        gameSettingsTable.add(playerNameInput);
        gameSettingsTable.row().padTop(SUB_SECTION_PADDING);

        final Label langLabel = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.LANGUAGE), skin, "subtitle");
        langLabel.setAlignment(Align.center);
        gameSettingsTable.add(langLabel).width(SUB_SECTION_HEADING_WIDTH);
        gameSettingsTable.row().padTop(LINE_PADDING);
        final HorizontalGroup languageButtons = getLanguageButtons();
        gameSettingsTable.add(languageButtons);
        gameSettingsTable.row().padTop(SUB_SECTION_PADDING);

//        gameSettingsTable.add(betaSettings).width(SUB_SECTION_HEADING_WIDTH);
//        gameSettingsTable.row().padTop(LINE_PADDING);
//        final TextButton toggleGreyProphetMode = getGreyProphetModeButton();
//        gameSettingsTable.add(toggleGreyProphetMode).width(140);
//        gameSettingsTable.row().padTop(SUB_SECTION_PADDING);
        return gameSettingsTable;
    }

    private HorizontalGroup getLanguageButtons() {
        final ButtonGroup<TextButton> languageSelect = new ButtonGroup<>();
        final TextButton english = new TextButton("English", skin, "radio");
        english.addListener(changeLanguageListener(english, Locale.US));
        final TextButton german = new TextButton("Deutsch", skin, "radio");
        german.addListener(changeLanguageListener(german, Locale.GERMANY));
        languageSelect.add(english, german);
        if(Locale.GERMAN.getLanguage().equals(clientSettings.getLocale().getLanguage())) {
            german.setChecked(true);
        }
        final HorizontalGroup languageButtons = new HorizontalGroup();
        languageButtons.addActor(english);
        languageButtons.addActor(german);
        languageButtons.space(SUB_SECTION_PADDING);
        return languageButtons;
    }

//    private TextButton getGreyProphetModeButton() {
//        final TextButton toggleGreyProphetMode = new TextButton("Grey Prophet", skin, "toggle");
//        toggleGreyProphetMode.setChecked(clientSettings.isGreyProphetMode());
//        toggleGreyProphetMode.addListener(
//                new ChangeListener() {
//                    @Override
//                    public void changed(ChangeEvent event, Actor actor) {
//                        if (toggleGreyProphetMode.isChecked()) {
//                            LOG.info("Activating the grey prophet! Prepare yourselfs faithfuls");
//                            clientSettings.setGreyProphetMode(true);
//                        } else {
//                            LOG.info("Deactivating the grey prophet! hahahaaaa pussy...");
//                            clientSettings.setGreyProphetMode(false);
//                        }
//                        game.getSettingsPersistenceHelper().saveSettings(clientSettings);
//                    }
//                });
//        return toggleGreyProphetMode;
//    }

    private TextTooltip tooltip(String text) {
        return new TextTooltip(text, skin);
    }

    private Window buildSingleplayerWindow() {
        final Texture star = new Texture(Gdx.files.internal("star.png"));
        final Texture starWhite = new Texture(Gdx.files.internal("star-white.png"));

        final Window singleplayer = new Window(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.SINGLEPLAYER), skin);
        singleplayer.setMovable(false);
        singleplayer.setTouchable(Touchable.childrenOnly);

        for(SingleplayerMode m : SingleplayerMode.values()) {
            singleplayer.add(m.getScreenElement(game, skin, star, starWhite))
                    .width(SingleplayerMode.ELEMENT_WIDTH)
                    .padLeft(SUB_SECTION_PADDING / 2f)
                    .padRight(SUB_SECTION_PADDING / 2f);
        }
        return singleplayer;
    }


    private ClickListener changeLanguageListener(TextButton button, Locale localeToSet) {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(button.isChecked()) {
                    clientSettings.setLocale(localeToSet);
                    settingsPersistenceHelper.saveSettings(clientSettings);
                    game.getSoundManager().rebuildSoundCache();
                    game.setScreen(new TitleScreen(game, clientSettings, version, executor));
                }
            }
        };
    }

    private ClickListener startMultiplayerGameListener() {
        return new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                synchronized(TitleScreen.this) {
                    if(currentServer.getConnector().isConnected()) {
                        try {
                            final String selectedPlayer = playersOnServer.getSelected();
                            if(selectedPlayer == null) {
                                return true;
                            }
                            currentServer.getConnector().createLobby(selectedPlayer);
                            final LobbyScreen lobbyScreen = new LobbyScreen(game, currentServer.getConnector(), skin,
                                    executor, Faction.PLAYER1);
                            game.setScreen(lobbyScreen);
                        } catch(JoinFailedException | InterruptedException e) {
                            LOG.error("Failed to open a new lobby", e);
                            showErrorDialog(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.CHALLENGE_FAILED));
                        }
                    } else {
                        showErrorDialog(
                                settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.CONNECTION_FAILED));
                    }
                    return true;
                }
            }
        };
    }

    private void showErrorDialog(String text) {
        final Dialog dialog = new Dialog(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.GAME_START_FAILED), skin);
        final Label errorLabel = new Label(text, skin, "error");
        dialog.text(errorLabel);
        dialog.button("Ok");
        dialog.show(stage);
    }

    private ClickListener enterMultiplayerLobbyListener() {
        return new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                synchronized(TitleScreen.this) {
                    final Lobby lobbyToEnter = lobbies.getSelected();
                    if(lobbyToEnter != null) {
                        if(lobbyToEnter.isProphet(clientSettings.getPlayerName())) {
                            // join lobby as active player, because we are one of the prophets
                            try {
                                Faction faction = currentServer.getConnector().joinLobby(lobbyToEnter.getId());
                                final LobbyScreen lobbyScreen = new LobbyScreen(game, currentServer.getConnector(),
                                        skin,
                                        executor, faction);
                                game.setScreen(lobbyScreen);
                            } catch(JoinFailedException | InterruptedException e) {
                                LOG.error("Unable to join lobby", e);
                                showErrorDialog(e.getMessage());
                            }
                        } else {
                            // observe lobby
                            try {
                                currentServer.getConnector().observeLobby(lobbyToEnter.getId());
                                final LobbyScreen lobbyScreen = new LobbyScreen(game, currentServer.getConnector(),
                                        skin,
                                        executor, Faction.NEUTRAL);
                                game.setScreen(lobbyScreen);
                            } catch(JoinFailedException | InterruptedException e) {
                                LOG.error("Unable to observe lobby", e);
                            }
                        }
                    }
                }
                return true;
            }
        };
    }

    private void clearListsAndDisplayConnectionError(String errorMessage) {
        synchronized(TitleScreen.this) {
            TitleScreen.this.lobbies.clearItems();
            server.setColor(Color.RED);
            if(serverTooltip == null) {
                serverTooltip = tooltip(errorMessage);
                server.addListener(serverTooltip);
            } else if(!serverTooltip.getActor().getText().toString().equals(errorMessage)) {
                serverTooltip.hide();
                server.removeListener(serverTooltip);
                serverTooltip = tooltip(errorMessage);
                server.addListener(serverTooltip);
            }
            connectionError.setText(errorMessage);
        }
    }

    private final class GameListRefresh implements Runnable {
        private int recentFailures = 0;
        private boolean disabled = false;

        @Override
        public synchronized void run() {
            LOG.debug("Server refresh");
            if(disabled) {
                return;
            }
            try {
                final ProphetConnector connector = currentServer.getConnector();
                if(!connector.isConnected()) {
                    connector.connect(clientSettings.getPlayerName());
                }

                synchronized(TitleScreen.this) {
                    playersOnServer.setItems(connector.getPlayersOnServer().toArray(new String[0]));
                }
                final SortedSet<Lobby> updatedLobbyList = new TreeSet<>();
                updatedLobbyList.addAll(connector.listMyLobbies());
                updatedLobbyList.addAll(connector.listOtherLobbies());
                updateGameList(TitleScreen.this.lobbies, updatedLobbyList.toArray(new Lobby[0]));
                final int ping = connector.getPing();
                LOG.debug("Current ping is {}", ping);
                if(ping <= 100) {
                    server.setColor(Color.GREEN);
                } else {
                    server.setColor(Color.YELLOW);
                }
                recentFailures = 0;
                if(serverTooltip != null) {
                    serverTooltip.hide();
                    server.removeListener(serverTooltip);
                }
                synchronized(TitleScreen.this) {
                    connectionError.setText("");
                }
            } catch(IOException e) {
                recentFailures++;
                clearListsAndDisplayConnectionError(e.getMessage());
                LOG.warn("IOException with the server", e);
            } catch(ConnectionFailedException e) {
                recentFailures++;
                clearListsAndDisplayConnectionError(e.getMessage());
                LOG.warn("Connection failed.", e);
                if(!e.isRetryMakesSense()) {
                    disabled = true;
                }
            } finally {
                if(!disabled) {
                    final int nextRefresh = Math.min(60, CommonValues.REFRESH_COOLDOWN << recentFailures);
                    LOG.debug("Scheduling next refresh in: " + nextRefresh + "s");
                    executor.schedule(this, nextRefresh, TimeUnit.SECONDS);
                }
            }
        }

        private void updateGameList(List<Lobby> uiList, Lobby[] serverList) {
            Lobby selectedLobby = null;
            for(Lobby lobby : serverList) {
                final Lobby prevSelectedGame = uiList.getSelected();
                if(prevSelectedGame != null && lobby.getId() == prevSelectedGame.getId()) {
                    selectedLobby = lobby;
                    break;
                }
            }
            synchronized(TitleScreen.this) {
                uiList.setItems(serverList);
                uiList.setSelected(selectedLobby);
            }
        }

        public void disable() {
            this.disabled = true;
        }
    }

    private final class LobbyInvitationListenerImpl implements LobbyInvitationListener {
        @Override
        public void challengeReceived(String otherPlayer, int lobbyId) {
            ChallengeDialog challengeDialog =
                    new ChallengeDialog(skin, settingsPersistenceHelper, otherPlayer, new ClickListener() {
                        @Override
                        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                            try {
                                Faction faction = currentServer.getConnector().joinLobby(lobbyId);
                                final LobbyScreen lobbyScreen = new LobbyScreen(game, currentServer.getConnector(),
                                        skin, executor, faction);
                                game.setScreen(lobbyScreen);
                            } catch(JoinFailedException | InterruptedException e) {
                                LOG.error("Unable to join lobby", e);
                                showErrorDialog(e.getMessage());
                            }
                            return super.touchDown(event, x, y, pointer, button);
                        }
                    }, new ClickListener() {
                        @Override
                        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                            currentServer.getConnector().declineInvitation(lobbyId);
                            return super.touchDown(event, x, y, pointer, button);
                        }
                    }
                    );
            challengeDialog.show(stage);
        }
    }
}
