/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.screen;

import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.I18NProperty;
import de.ggj14.wap.common.ai.AI;
import de.ggj14.wap.common.ai.EasyAI;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.MinionType;
import de.ggj14.wap.common.datamodel.conversation.Conversation;
import de.ggj14.wap.common.gamefield.objects.Minion;
import de.weareprophet.client.LocalOnlyGameState;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.frontend.WapFrontend;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;
import de.weareprophet.client.libgdx.sound.SoundManager;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Represents a part of the tutorial. The parts are linear and ordered as in this enum.
 */
enum TutorialSection implements I18NProperty {
    /**
     * Introduction before we have selected the prophet.
     */
    SELECT_PROPHET {
        private Minion myProphet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            myProphet = gameState.getMinionMap().get(0);
            screenUpdate.disableFrontendUpdates();
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(frontend.getInteractionManager().getSelectedMinionIds().contains(0));
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(myProphet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myProphet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_SELECT_PROPHET);
        }
    },
    /**
     * We have selected the prophet and have to move it a bit.
     */
    MOVE_PROPHET {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
            screenUpdate.enableFrontendUpdates();
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            final Minion myProphet = gameState.getMinionMap().get(0);
            return nextSection(myProphet.distanceToPoint(75, 75) > 100);
        }

        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(WapGame.WORLD_WIDTH - TutorialOverlay.TEXT_BOX_WIDTH - 50, 50, settingsPersistenceHelper.getLocalizedProperty(this));
        }
    },
    /**
     * A young child is spawned and explained.
     */
    EXPLAIN_CHILD {
        private Minion child;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            child = gameState.addMinion(MinionType.CHILD, Faction.NEUTRAL, new Point2D.Float(CHILD_X, CHILD_Y));
            child.dontMove();
            frontend.addMinion(child);
            gameState.pause();
            screenUpdate.disableFrontendUpdates();
            clickExpected.set(true);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(new Point2D.Float(CHILD_X, CHILD_Y));
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(child, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_EXPLAIN_CHILD);
        }
    },
    /**
     * The prophet has to move to the young child to start a conversation.
     */
    MEET_CHILD {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
            screenUpdate.enableFrontendUpdates();
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(gameState.getMinionIDConversationMap().get(0) != null);
        }

        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(WapGame.WORLD_WIDTH - TutorialOverlay.TEXT_BOX_WIDTH - 50, 50, settingsPersistenceHelper.getLocalizedProperty(this));
        }
    },
    /**
     * The prophet has started a conversation with the child, which is now explained.
     */
    EXPLAIN_CONVERSATION {
        private Minion myProphet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            myProphet = gameState.getMinionMap().get(0);
            gameState.pause();
            screenUpdate.disableFrontendUpdates();
            clickExpected.set(true);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(
                    new Point2D.Float(CHILD_X, CHILD_Y),
                    myProphet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myProphet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_EXPLAIN_CONVERSATION);
        }
    },
    /**
     * After a click, the conversation progresses until it's finished.
     */
    CONVINCE_CHILD {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
            screenUpdate.enableFrontendUpdates();
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(gameState.getMinionMap().get(1).getFaction().equals(Faction.PLAYER1));
        }
    },
    /**
     * Explain and perform a multiselect.
     */
    MULTISELECT {
        private Minion myProphet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            myProphet = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myProphet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(frontend.getInteractionManager().getSelectedMinionIds().size() == 2);
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_MULTISELECT);
        }
    },
    MULTISELECT_DONE {
        private Minion myProphet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            clickExpected.set(true);
            myProphet = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(myProphet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myProphet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_MULTISELECT_DONE);
        }
    },
    /**
     * Explain the tab key.
     */
    TAB_KEY {
        private Minion myProphet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            myProphet = gameState.getMinionMap().get(0);
            tabExpected.set(3);
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myProphet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(tabReceived);
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_TAB_KEY);
        }
    },
    TAB_KEY_DONE {
        private Minion myProphet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            clickExpected.set(true);
            myProphet = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(myProphet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myProphet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_TAB_KEY_DONE);
        }
    },
    /**
     * Explain that different minion types have different speeds.
     */
    SPEED_DEMO_EXPLANATION {

        private Point2D.Float[] minionPositions;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.initSpeedDemo();
            clickExpected.set(true);
            final Collection<Minion> minions = gameState.getMinionMap().values();
            minionPositions = new Point2D.Float[minions.size()];
            int i = 0;
            for(Minion m : minions) {
                minionPositions[i++] = m.getPosition();
            }
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(minionPositions);
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(
                    (WapGame.WORLD_WIDTH - TutorialOverlay.TEXT_BOX_WIDTH) / 2f,
                    (WapGame.WORLD_HEIGHT - TutorialOverlay.TEXT_BOX_LINE_HEIGHT * 4) / 2f, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_SPEED_DEMO);
        }
    },
    /**
     * Show the different speeds of different minion types.
     */
    SPEED_DEMO {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(gameState.getMinionMap().get(0).getX() > 400);
        }
    },
    /**
     * Explain that different minion types have different persuasiveness.
     */
    PERSUASION_DEMO_EXPLANATION {

        private Minion myMinion;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.initPersuasionDemo();
            clickExpected.set(true);
            myMinion = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(myMinion.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myMinion, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_PERSUASION_DEMO_EXPLANATION);
        }
    },
    /**
     * Let the player experience the differences in persuasiveness.
     */
    PERSUASION_DEMO {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(gameState.getMinionMap().get(1).getFaction().equals(Faction.PLAYER1));
        }

        @Override
        Optional<TutorialSection> checkFailed(LocalOnlyGameState gameState, WapFrontend frontend) {
            Conversation convo = gameState.getMinionIDConversationMap().get(0);
            if(convo != null && convo.hasProphet() && convo.getConversationOverview().getPlayer2Percentage() > 0.5) {
                return Optional.of(TutorialSection.PERSUASION_DEMO_FAILED);
            } else {
                return Optional.empty();
            }
        }

        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(WapGame.WORLD_WIDTH - TutorialOverlay.TEXT_BOX_WIDTH - 50, 80, settingsPersistenceHelper.getLocalizedProperty(this));
        }
    },
    /**
     * Success message for the persuasion demo.
     */
    PERSUASION_DEMO_SUCCESS {

        private Minion myMinion;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.pause();
            clickExpected.set(true);
            myMinion = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(myMinion.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myMinion, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_PERSUASION_DEMO_SUCCESS);
        }
    },
    /**
     * Introduce what the power demo will demonstrate.
     */
    POWER_DEMO_INTRO {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.initPowerDemo();
            clickExpected.set(true);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight();
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(
                    (WapGame.WORLD_WIDTH - TutorialOverlay.TEXT_BOX_WIDTH) / 2f,
                    (WapGame.WORLD_HEIGHT - TutorialOverlay.TEXT_BOX_LINE_HEIGHT * 4) / 2f, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_POWER_DEMO_INTRO);
        }
    },
    /**
     * Show the player how the conversation bars progress.
     */
    POWER_DEMO {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(gameState.getIdConversationMap().get(0).getConversationOverview().getPlayer1Percentage() > 0.4);
        }
    },
    /**
     * Explain how different minion types influence the way that conversation bars move (persuasion & willpower).
     */
    POWER_DEMO_EXPLANATION_1 {
        private Point2D.Float highlight;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.pause();
            clickExpected.set(true);
            highlight = new Point2D.Float(125, gameState.getMinionIDConversationMap().get(0).getConvoCenterY() - 60);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(highlight);
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(highlight.x + 40, highlight.y + 40, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_POWER_DEMO_EXPLANATION_1);
        }
    },
    POWER_DEMO_EXPLANATION_2 {
        private Point2D.Float highlight;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.pause();
            clickExpected.set(true);
            highlight = new Point2D.Float(125, gameState.getMinionIDConversationMap().get(1).getConvoCenterY() - 60);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(highlight);
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(highlight.x + 40, highlight.y + 40, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_POWER_DEMO_EXPLANATION_2);
        }
    },
    POWER_DEMO_EXPLANATION_3 {
        private Point2D.Float highlight;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.pause();
            clickExpected.set(true);
            highlight = new Point2D.Float(125, gameState.getMinionIDConversationMap().get(2).getConvoCenterY() - 60);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(highlight);
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(highlight.x + 40, highlight.y + 40, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_POWER_DEMO_EXPLANATION_3);
        }
    },
    POWER_DEMO_EXPLANATION_4 {
        private Minion minion;
        private Point2D.Float highlight;


        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
            minion = gameState.getMinionMap().get(6);
            minion.setTargetPosX(75);
            minion.setTargetPosY(540);
            highlight = new Point2D.Float(125, gameState.getMinionIDConversationMap().get(2).getConvoCenterY() - 60);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(highlight, minion.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(minion, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            final Map<Integer, Minion> minionMap = gameState.getMinionMap();
            for(int i = 0; i < 6; i++) {
                minionMap.get(i).dontMove();
            }
            return nextSection(gameState.getIdConversationMap().isEmpty());
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_POWER_DEMO_EXPLANATION_4);
        }
    },
    /**
     * Pauses the tutorial after the power demo is done.
     */
    POWER_DEMO_DONE {
        private Minion minion;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            clickExpected.set(true);
            minion = gameState.getMinionMap().get(6);
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(minion, settingsPersistenceHelper.getLocalizedProperty(POWER_DEMO_EXPLANATION_4));
        }

    },
    /**
     * Explain that even a prophet can't stop an enemy prophet once in a conversation.
     */
    INVULNERABILITY_DEMO_PROPHET_EXPLANATION {

        private Minion prophet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.initInvulDemo();
            clickExpected.set(true);
            prophet = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(prophet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(prophet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_INVULNERABILITY_DEMO_PROPHET_EXPLANATION);
        }
    },
    /**
     * Demonstrate that minions can be used to slow down conversations of the enemy prophet.
     */
    INVULNERABILITY_DEMO_DELAY_CONVERSATION {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
            screenUpdate.enableFrontendUpdates();
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return nextSection(gameState.getIdConversationMap().get(0).getParticipants().size() == 3);
        }
    },
    /**
     * Explain that the enemy prophet can try to recapture minions after a conversation has finished.
     */
    INVULNERABILITY_DEMO_RECAPTURE_EXPLANATION {

        private Minion prophet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.pause();
            prophet = gameState.getMinionMap().get(0);
            clickExpected.set(true);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(prophet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(prophet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_INVULNERABILITY_DEMO_RECAPTURE_EXPLANATION);
        }
    },
    /**
     * Let the player try to save a minion that has just been freshly convinced.
     */
    INVULNERABILITY_DEMO {
        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(50, 50, settingsPersistenceHelper.getLocalizedProperty(INVULNERABILITY_DEMO_RECAPTURE_EXPLANATION));
        }

        @Override
        Optional<TutorialSection> checkFailed(LocalOnlyGameState gameState, WapFrontend frontend) {
            final Minion minion1 = gameState.getMinionMap().get(1);
            final Minion minion2 = gameState.getMinionMap().get(2);
            if(minion1.getFaction().equals(Faction.PLAYER1) && !minion1.canMove()
                    && minion2.getFaction().equals(Faction.PLAYER1) && !minion2.canMove()) {
                return Optional.of(INVULNERABILITY_DEMO_FAILED);
            } else {
                return Optional.empty();
            }
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            final Minion minion1 = gameState.getMinionMap().get(1);
            final Minion minion2 = gameState.getMinionMap().get(2);
            return nextSection(
                    (minion1.getFaction().equals(Faction.PLAYER1) && !minion1.isInvulnerable(gameState.getTime() - 100) && minion1.canMove())
                            || (minion2.getFaction().equals(Faction.PLAYER1) && !minion2.isInvulnerable(gameState.getTime() - 100) && minion2.canMove())
            );
        }
    },
    /**
     * The player has managed to escape with a minion.
     */
    INVULNERABILITY_DEMO_COMPLETE {

        private Minion prophet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.pause();
            prophet = gameState.getMinionMap().get(0);
            clickExpected.set(true);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(prophet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(prophet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_INVULNERABILITY_DEMO_COMPLETE);
        }
    },
    /**
     * Explain the win condition of the game.
     */
    MAP_DOMINATION_EXPLANATION {

        private Minion prophet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, 6, true, false);
            gameState.setStartCountdown(0);
            gameState.pause();
            frontend.reset();
            prophet = gameState.getMinionMap().get(0);
            clickExpected.set(true);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(prophet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(prophet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_MAP_DOMINATION_EXPLANATION);
        }
    },
    /**
     * Let the player try to capture enough map tiles.
     */
    MAP_DOMINATION_DEMO {
        private final AI ai = new EasyAI();

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            gameState.resume();
            gameState.resumeGameFieldUpdates();
            ai.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, Faction.PLAYER2,
                    m -> gameState.setMinionTargetPosition(m.objectID(), m.targetX(), m.targetY()));
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            ai.update(gameState);
            return nextSection(Faction.PLAYER1.equals(gameState.checkWinningCondition()));
        }

        @Override
        Optional<TutorialSection> checkFailed(LocalOnlyGameState gameState, WapFrontend frontend) {
            if(Faction.PLAYER2.equals(gameState.checkWinningCondition())) {
                return Optional.of(TutorialSection.MAP_DOMINATION_DEMO_FAILED);
            } else {
                return Optional.empty();
            }
        }
    },
    /**
     * The tutorial is over and a success message is shown.
     */
    SUCCESS_MESSAGE {
        private Minion myProphet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            myProphet = gameState.getMinionMap().get(0);
            gameState.pause();
            clickExpected.set(true);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(new Point2D.Float(myProphet.getX(), myProphet.getY()));
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myProphet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_SUCCESS_MESSAGE);
        }
    },
    FINISHED {
        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            return Optional.empty();
        }
    },
    PERSUASION_DEMO_FAILED {
        private Minion myMinion;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            clickExpected.set(true);
            myMinion = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(myMinion.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myMinion, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            if(clickReceived) {
                return Optional.of(PERSUASION_DEMO_EXPLANATION);
            } else {
                return Optional.empty();
            }
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_PERSUASION_DEMO_FAILED);
        }
    },
    INVULNERABILITY_DEMO_FAILED {
        private Minion prophet;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            clickExpected.set(true);
            prophet = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(prophet.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(prophet, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            if(clickReceived) {
                return Optional.of(INVULNERABILITY_DEMO_PROPHET_EXPLANATION);
            } else {
                return Optional.empty();
            }
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_INVULNERABILITY_DEMO_FAILED);
        }
    },
    MAP_DOMINATION_DEMO_FAILED {
        private Minion myMinion;

        @Override
        void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
            clickExpected.set(true);
            myMinion = gameState.getMinionMap().get(0);
        }

        @Override
        void renderToMask(TutorialOverlay tutorialOverlay) {
            tutorialOverlay.renderHighlight(myMinion.getPosition());
        }

        @Override
        void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
            tutorialOverlay.renderMessageBox(myMinion, settingsPersistenceHelper.getLocalizedProperty(this));
        }

        @Override
        Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
            if(clickReceived) {
                return Optional.of(MAP_DOMINATION_EXPLANATION);
            } else {
                return Optional.empty();
            }
        }

        @Override
        Optional<SoundManager.Sound> getSound() {
            return Optional.of(SoundManager.Sound.TUTORIAL_MAP_DOMINATION_DEMO_FAILED);
        }
    };
    private static final int CHILD_X = 300;
    private static final int CHILD_Y = 300;

    void setup(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update screenUpdate, AtomicBoolean clickExpected, AtomicInteger tabExpected) {
    }

    void renderToMask(TutorialOverlay tutorialOverlay) {
    }

    void renderToOverlay(TutorialOverlay tutorialOverlay, SettingsPersistenceHelper settingsPersistenceHelper) {
    }


    /**
     * Checks whether this section is finished and, if so, returns the tutorial section with which
     * to continue.
     *
     * @param gameState     The current game state.
     * @param frontend      The frontend in use, e.g. to determine selection.
     * @param clickReceived Whether a click has been received since the last time one was expected.
     * @param tabReceived   Whether the tab key has been pressed since the last time one was expected.
     * @return True if this section is finished and false if it is still continuing.
     */
    Optional<TutorialSection> checkFinished(LocalOnlyGameState gameState, WapFrontend frontend, boolean clickReceived, boolean tabReceived) {
        return nextSection(clickReceived);
    }

    /**
     * Checks whether this tutorial section can no longer be finished and, if so, returns the tutorial
     * section to which to go back.
     *
     * @param gameState The current game state.
     * @param frontend  The frontend in use, e.g. to determine selection.
     * @return Returns a non-empty optional of the section to go to if this one has failed.
     */
    Optional<TutorialSection> checkFailed(LocalOnlyGameState gameState, WapFrontend frontend) {
        return Optional.empty();
    }

    Optional<TutorialSection> nextSection(final boolean sectionFinished) {
        if(!sectionFinished) {
            return Optional.empty();
        } else {
            TutorialSection[] sections = TutorialSection.values();
            for(int i = 0; i < sections.length - 1; i++) {
                if(sections[i].equals(this)) {
                    return Optional.of(sections[i + 1]);
                }
            }
            throw new NoSuchElementException();
        }
    }

    Optional<SoundManager.Sound> getSound() {
        return Optional.empty();
    }

    @Override
    public String getKey() {
        return TutorialSection.class.getSimpleName() + "." + name();
    }
}
