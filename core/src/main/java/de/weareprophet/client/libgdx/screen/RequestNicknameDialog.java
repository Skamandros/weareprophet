/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.ggj14.wap.common.CommonValues;
import de.weareprophet.client.libgdx.settings.ClientSettings;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;

public class RequestNicknameDialog extends Dialog {
    public RequestNicknameDialog(Skin skin, SettingsPersistenceHelper settingsPersistenceHelper, TextField titleScreenPlayerNameInput) {
        super(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.REQUEST_NICKNAME_TITLE), skin);
        final HorizontalGroup group = new HorizontalGroup();
        final Label textLabel = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.REQUEST_NICKNAME_LABEL), skin);
        this.text(textLabel);
        group.addActor(textLabel);
        final TextField nameInput = new TextField("", skin);
        nameInput.setMaxLength(CommonValues.MAX_NICKNAME_LENGTH);
        nameInput.setOnlyFontChars(true);
        nameInput.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(Color.RED.equals(nameInput.getColor())) {
                    nameInput.setColor(Color.WHITE);
                }
            }
        });
        group.addActor(nameInput);
        group.space(10);
        this.getContentTable().add(group);


        TextButton ok = new TextButton("Ok", skin, "round");
        ok.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                final String nameToSet = nameInput.getText();
                if(!CommonValues.ALLOWED_NICKNAMES.matcher(nameToSet).matches()) {
                    nameInput.setColor(Color.RED);
                    RequestNicknameDialog.this.cancel();
                    return true;
                } else {
                    ClientSettings settings = settingsPersistenceHelper.getSettings();
                    settings.setPlayerName(nameToSet);
                    settingsPersistenceHelper.saveSettings(settings);
                    titleScreenPlayerNameInput.setText(nameToSet);
                    return super.touchDown(event, x, y, pointer, button);
                }
            }
        });
        this.button(ok);
    }
}
