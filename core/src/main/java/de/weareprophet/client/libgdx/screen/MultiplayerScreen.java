/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import de.ggj14.wap.common.communication.ServerCommunicationHelper;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.gamefield.MoveActionHandler;
import de.ggj14.wap.common.server.ProphetConnector;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.frontend.WapFrontend;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MultiplayerScreen extends ScreenAdapter {

    private static final Logger LOG = LogManager.getLogger(MultiplayerScreen.class);

    private final LobbyScreen lobby;
    private final ProphetConnector psc;
    private ServerCommunicationHelper communicationHelper;

    private final WapGame game;
    private final ScreenHelper screenHelper;
    private final Faction myFaction;
    private WapFrontend frontend;
    private GameInformation gameInformation;
    private boolean namesSynchronized = false;
    private SpriteBatch spriteBatch;
    private ShapeRenderer shapeRenderer;
    private ScheduledExecutorService updateExecutor;

    public MultiplayerScreen(final WapGame game, final LobbyScreen lobby, final ProphetConnector psc, final GameInformation gameInformation, final Faction assignedFaction) {
        if(gameInformation.getGameId() == GameInformation.NO_GAME_ID) {
            throw new IllegalArgumentException("No valid game information provided");
        }
        this.game = game;
        this.lobby = lobby;
        this.psc = psc;
        this.gameInformation = gameInformation;
        this.screenHelper = new ScreenHelper(game, psc);
        this.myFaction = assignedFaction;
    }

    @Override
    public void show() {
        LOG.info("Entering Multiplayer-Mode");
        namesSynchronized = false;
        frontend = new WapFrontend(game.getFont12(), game.getFont48(), game.getFontShader(),
                game.getSettingsPersistenceHelper());
        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(game.getViewport().getCamera().combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(game.getViewport().getCamera().combined);
        this.screenHelper.show();

        try {
            if(gameInformation == null) {
                throw new IllegalStateException("GameInformation is null");
            }

            //
            this.communicationHelper = new ServerCommunicationHelper(psc);
            frontend.init(this.communicationHelper, myFaction, gameInformation, game.getSoundManager(),
                    game.getViewport(), new MoveActionHandler() {
                        @Override
                        public void handleMoveAction(MoveAction moveAction) {
                            if(!myFaction.equals(Faction.NEUTRAL)) {
                                if(communicationHelper.getGameState().getMinionMap().get(
                                        moveAction.objectID()).getFaction().equals(myFaction)) {
                                    communicationHelper.sendMoveAction(moveAction);
                                } else {
                                    frontend.deselectMinion();
                                }
                            }
                        }
                    });


            final InputMultiplexer inputProcessor = new InputMultiplexer();
            inputProcessor.addProcessor(screenHelper.getIngameInputHandler());
            inputProcessor.addProcessor(frontend.getInputProcessor());
            Gdx.input.setInputProcessor(inputProcessor);

            updateExecutor = Executors.newScheduledThreadPool(1);
            updateExecutor.scheduleAtFixedRate(new Update(), 0, 10, TimeUnit.MILLISECONDS);
        } catch(Exception e) {
            LOG.error("Creating or joining a game has failed!", e);
            game.switchToTitleScreen();
        }
    }

    @Override
    public void hide() {
        LOG.info("Leaving Multiplayer-Mode");
        this.screenHelper.hide();
        spriteBatch.dispose();
        shapeRenderer.dispose();
        if(communicationHelper != null) {
            this.communicationHelper.close();
        }
        if(updateExecutor != null) {
            updateExecutor.shutdownNow();
            try {
                if(!updateExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
                    LOG.error("Update executor didn't shut down in time");
                }
            } catch(InterruptedException e) {
                LOG.warn("Update executor shutdown interrupted");
            }
        }
    }


    @Override
    public synchronized void render(float deltaInSeconds) {
        try {
            frontend.render(spriteBatch, shapeRenderer);
            screenHelper.render(deltaInSeconds);
            if(frontend.getLeaveGameCounter() > 2_000) {
                game.setScreen(lobby);
            }
        } catch(Exception e) {
            LOG.fatal("Uncaught exception during rendering", e);
            throw e;
        }
    }

    private final class Update implements Runnable {
        private long lastUpdate = System.nanoTime();


        @Override
        public void run() {
            final long currentTime = System.nanoTime();
            final int deltaInMillis = (int) (currentTime - lastUpdate) / 1_000_000;

            final GameState gameState = communicationHelper.getGameState();
            synchronized(MultiplayerScreen.this) {
                // check for other player entering the game
                if(gameState.getStartCountdown() == GameState.COUNTDOWN_MAX - 1 && !namesSynchronized) {
                    namesSynchronized = true;
                    try {
                        gameInformation = psc.getGameInformation();
                        LOG.info("Joined a multiplayer game {} as faction {}", gameInformation, myFaction);
                        frontend.setGameInformation(gameInformation);
                    } catch(IOException e) {
                        LOG.error("Unable to get game information", e);
                    }
                }
                frontend.update(deltaInMillis, gameState);
            }

            lastUpdate = currentTime;
            LOG.trace("Update run execution time: {} ns", System.nanoTime() - currentTime);
        }
    }

}
