/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.settings;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Locale;

/**
 * Allows access to user-specific settings.
 *
 * @author SharkofMetal
 */
public interface ClientSettings {
    /**
     * The player name that is used if the player has none yet.
     */
    String DEFAULT_PLAYER_NAME = "Default";

    /**
     * Gets the player name.
     *
     * @return The current player's name
     */
    String getPlayerName();

    /**
     * Sets a new player name.
     *
     * @param playerName The new player name to set
     */
    void setPlayerName(String playerName);

    /**
     * Returns the information whether the game music should be active.
     *
     * @return Whether the music is on
     */
    boolean isMusicOn();

    /**
     * Stores a new state whether the game music should be active.
     *
     * @param musicOn Whether the music is on
     */
    void setMusicOn(boolean musicOn);

    /**
     * Returns the information whether the game sound should be active.
     *
     * @return Whether the sound is on
     */
    boolean isSoundOn();

    /**
     * Stores a new state whether the game sound should be active.
     *
     * @param soundOn Whether the sound is on
     */
    void setSoundOn(boolean soundOn);

    /**
     * Returns the desired sound volume.
     *
     * @return The sound volume from 0 to 1.
     */
    float getVolume();

    /**
     * Stores a new configured sound volume.
     *
     * @param volume The sound volume to use, from 0 to 1.
     */
    void setVolume(float volume);

    /**
     * Returns whether the game shall run in full screen mode.
     *
     * @return True if the game shall run in full screen mode and false is window mode is desired.
     */
    boolean isFullScreenMode();

    /**
     * Changes the full screen mode configuration setting.
     *
     * @param fullScreenMode The new config value to set - true for full screen mode and false for window mode.
     */
    void setFullScreenMode(boolean fullScreenMode);

    /**
     * @return The locale which determines in which language the game runs.
     */
    Locale getLocale();

    /**
     * Changes the saved game locale.
     *
     * @param toSet Which locale shall be used, i.e. in which language the game shall run.
     */
    void setLocale(Locale toSet);

    /**
     * Returns whether debug UI features shall be enabled, e.g. movement lines. Does not affect logging.
     *
     * @return True iff debug the game shall show debug UI output.
     */
    boolean isDebugMode();

    /**
     * Changes the UI debug mode setting.
     *
     * @param debugMode The new config value to set - true for debug UI output and false otherwise.
     */
    void setDebugMode(boolean debugMode);

    /**
     * @return The key pair of this client.
     */
    KeyPair getKeys() throws NoSuchAlgorithmException, InvalidKeySpecException;
}