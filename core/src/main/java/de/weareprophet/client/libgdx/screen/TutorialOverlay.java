/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import de.ggj14.wap.common.I18NProperty;
import de.ggj14.wap.common.gamefield.objects.Minion;
import de.weareprophet.client.LocalOnlyGameState;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.frontend.WapFrontend;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.geom.Point2D;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Draws tutorial-related content on top of the game elements, e.g. highlights and text boxes.
 */
class TutorialOverlay {
    private static final Logger LOGGER = LogManager.getLogger(TutorialOverlay.class);

    enum Property implements I18NProperty {
        CLICK_TO_CONTINUE;

        @Override
        public String getKey() {
            return TutorialOverlay.class.getSimpleName() + "." + name();
        }
    }

    static final int TEXT_BOX_WIDTH = 200;
    static final int TEXT_BOX_LINE_HEIGHT = 17;
    private static final int TEXT_BOX_X_OFFSET = 25;
    private static final int TEXT_BOX_Y_OFFSET = -8;
    private static final int TEXT_PADDING = 10;
    public static final int TIME_IN_SECTION_UNTIL_CLICK_ALLOWED_MILLIS = 1_200;
    private final SettingsPersistenceHelper settingsPersistenceHelper;
    private final BitmapFont font;

    private final BitmapFont fontBold;
    private final ShaderProgram fontShader;
    private final GlyphLayout glyphLayout;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private FrameBuffer frameBuffer;
    private LocalOnlyGameState gameState;
    private WapFrontend frontend;
    private TutorialScreen.Update screenUpdate;
    private TutorialSection currentSection;
    private final AtomicInteger timeInSectionMillis = new AtomicInteger();
    private final AtomicBoolean clickExpected = new AtomicBoolean(false);
    private final AtomicInteger tabExpected = new AtomicInteger(0);

    TutorialOverlay(SettingsPersistenceHelper settingsPersistenceHelper, BitmapFont font, BitmapFont fontBold, ShaderProgram fontShader) {
        this.settingsPersistenceHelper = settingsPersistenceHelper;
        this.font = font;
        this.fontBold = fontBold;
        this.fontShader = fontShader;
        glyphLayout = new GlyphLayout();
    }

    public void show(LocalOnlyGameState gameState, WapFrontend frontend, TutorialScreen.Update updateCommand) {
        this.gameState = gameState;
        this.frontend = frontend;
        this.screenUpdate = updateCommand;
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);
        spriteBatch = new SpriteBatch();
        frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, WapGame.WORLD_WIDTH, WapGame.WORLD_HEIGHT, false);
        final StretchViewport viewport = new StretchViewport(WapGame.WORLD_WIDTH, WapGame.WORLD_HEIGHT);
        // I don't know who thought this would be a good idea, but the viewport camera isn't centered right away
        viewport.update(WapGame.WORLD_WIDTH, WapGame.WORLD_HEIGHT, true);
        // The shapeRenderer and spriteBatch need the correct projection matrix - since we render to a frame buffer,
        // the "screen size" will always be the frame buffer size
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        spriteBatch.setProjectionMatrix(viewport.getCamera().combined);
        gameState.pause();
        currentSection = TutorialSection.SELECT_PROPHET;
        switchSection(TutorialSection.SELECT_PROPHET);
    }

    public void hide() {
        currentSection.getSound().ifPresent(frontend.getSoundManager()::stopSound);
        shapeRenderer.dispose();
        spriteBatch.dispose();
        frameBuffer.dispose();
    }

    public Texture renderToMask() {
        frameBuffer.begin();
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        currentSection.renderToMask(this);
        frameBuffer.end();
        return frameBuffer.getColorBufferTexture();
    }

    public Texture renderToOverlay() {
        frameBuffer.begin();
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        currentSection.renderToOverlay(this, settingsPersistenceHelper);
        if(clickExpected.get() && timeInSectionMillis.get() >= TIME_IN_SECTION_UNTIL_CLICK_ALLOWED_MILLIS) {
            renderMessageBox((WapGame.WORLD_WIDTH - TEXT_BOX_WIDTH) / 2f,
                    (WapGame.WORLD_HEIGHT + TEXT_BOX_LINE_HEIGHT) / 2f,
                    fontBold, settingsPersistenceHelper.getLocalizedProperty(Property.CLICK_TO_CONTINUE));
        }
        frameBuffer.end();
        return frameBuffer.getColorBufferTexture();
    }

    /**
     * Updates the current tutorial section, if the player has accomplished the set task.
     *
     * @param deltaInMillis The time since the last update call, in millis.
     */
    public void update(int deltaInMillis) {
        timeInSectionMillis.addAndGet(deltaInMillis);
        Optional<TutorialSection> sectionFinished = currentSection.checkFinished(gameState, frontend,
                !clickExpected.get(), tabExpected.get() == 0);
        Optional<TutorialSection> sectionFailed = currentSection.checkFailed(gameState, frontend);
        if(sectionFinished.isPresent()) {
            switchSection(sectionFinished.get());
        } else if(sectionFailed.isPresent()) {
            switchSection(sectionFailed.get());
        }
    }

    private void switchSection(TutorialSection nextSection) {
        LOGGER.debug("[Tutorial] Switch from section {} to section {}", currentSection, nextSection);
        currentSection.getSound().ifPresent(frontend.getSoundManager()::stopSound);
        nextSection.getSound().ifPresent(frontend.getSoundManager()::playSound);
        currentSection = nextSection;
        currentSection.setup(gameState, frontend, screenUpdate, clickExpected, tabExpected);
        timeInSectionMillis.set(0);
    }

    /**
     * @return Whether the entire tutorial is finished.
     */
    public boolean isFinished() {
        return currentSection.equals(TutorialSection.FINISHED);
    }

    public InputProcessor getInputProcessor() {
        return new InputAdapter() {
            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                LOGGER.debug("Click received; expected: {}", clickExpected);
                if(timeInSectionMillis.get() < TIME_IN_SECTION_UNTIL_CLICK_ALLOWED_MILLIS) {
                    return false;
                } else {
                    return button == Input.Buttons.LEFT && clickExpected.compareAndSet(true, false);
                }
            }

            @Override
            public boolean keyDown(int keycode) {
                if(keycode == Input.Keys.TAB) {
                    for(int prev = tabExpected.get(); prev > 0; prev = tabExpected.get()) {
                        if(tabExpected.compareAndSet(prev, prev - 1)) {
                            break;
                        }
                    }
                }
                return false;
            }
        };
    }

    /**
     * Darkens the screen and highlights an area.
     *
     * @param highlights The highlight areas' center coordinates.
     */
    void renderHighlight(Point2D.Float... highlights) {
        Gdx.gl.glColorMask(false, false, false, true);
        Gdx.gl.glBlendFunc(GL20.GL_ONE, GL20.GL_ZERO);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(1, 1, 1, 0.7f);
        shapeRenderer.rect(0, 0, WapGame.WORLD_WIDTH, WapGame.WORLD_HEIGHT);
        shapeRenderer.flush();
        shapeRenderer.setColor(1, 1, 1, 0);
        for(Point2D.Float p : highlights) {
            shapeRenderer.circle(p.x, p.y, 50);
        }
        shapeRenderer.flush();
        shapeRenderer.end();
        Gdx.gl.glBlendFunc(GL20.GL_DST_ALPHA, GL20.GL_ONE_MINUS_DST_ALPHA);
        Gdx.gl.glColorMask(true, true, true, true);
    }

    void renderMessageBox(Minion minion, String message) {
        renderMessageBox(minion.getX(), minion.getY(), message);
    }

    void renderMessageBox(final float x, final float y, final String message) {
        renderMessageBox(x, y, font, message);
    }

    private void renderMessageBox(final float x, final float y, final BitmapFont font, final String message) {
        glyphLayout.setText(font, message, Color.BLACK, TEXT_BOX_WIDTH - 2 * TEXT_PADDING, Align.left, true);
        final float boxHeight = glyphLayout.height + 2f * TEXT_PADDING;
        final float boxX, boxY;
        if(x < WapGame.WORLD_WIDTH - TEXT_BOX_WIDTH - TEXT_BOX_X_OFFSET) {
            boxX = x + TEXT_BOX_X_OFFSET;
        } else {
            boxX = x - TEXT_BOX_WIDTH;
        }
        if(y - boxHeight + TEXT_BOX_Y_OFFSET > 0) {
            boxY = y - boxHeight + TEXT_BOX_Y_OFFSET;
        } else {
            boxY = y + TEXT_BOX_Y_OFFSET;
        }
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.WHITE);
        roundedRect(shapeRenderer, boxX - TEXT_PADDING, boxY - TEXT_PADDING,
                boxHeight);
        shapeRenderer.end();
        spriteBatch.begin();
        spriteBatch.setShader(fontShader);
        font.setColor(Color.BLACK);
        font.draw(spriteBatch, message, boxX, boxY, TEXT_BOX_WIDTH - 2 * TEXT_PADDING, Align.left, true);
        spriteBatch.setShader(null);
        spriteBatch.end();
    }


    /**
     * Draws a rectangle with rounded corners of the given radius.
     */
    private static void roundedRect(ShapeRenderer renderer, float x, float y, float height) {
        final float radius = 10;
        // Central rectangle
        renderer.rect(x + radius, y + radius, TutorialOverlay.TEXT_BOX_WIDTH - 2 * radius, height - 2 * radius);

        // Four side rectangles, in clockwise order
        renderer.rect(x + radius, y, TutorialOverlay.TEXT_BOX_WIDTH - 2 * radius, radius);
        renderer.rect(x + TutorialOverlay.TEXT_BOX_WIDTH - radius, y + radius, radius, height - 2 * radius);
        renderer.rect(x + radius, y + height - radius, TutorialOverlay.TEXT_BOX_WIDTH - 2 * radius, radius);
        renderer.rect(x, y + radius, radius, height - 2 * radius);

        // Four arches, clockwise too
        renderer.arc(x + radius, y + radius, radius, 180f, 90f);
        renderer.arc(x + TutorialOverlay.TEXT_BOX_WIDTH - radius, y + radius, radius, 270f, 90f);
        renderer.arc(x + TutorialOverlay.TEXT_BOX_WIDTH - radius, y + height - radius, radius, 0f, 90f);
        renderer.arc(x + radius, y + height - radius, radius, 90f, 90f);
    }
}
