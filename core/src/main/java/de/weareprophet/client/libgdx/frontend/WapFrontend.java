/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.frontend;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.Viewport;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.GameStateProvider;
import de.ggj14.wap.common.datamodel.conversation.Conversation;
import de.ggj14.wap.common.gamefield.MoveActionHandler;
import de.ggj14.wap.common.gamefield.objects.Minion;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;
import de.weareprophet.client.libgdx.sound.SoundManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class WapFrontend {

    private static final Logger LOG = LogManager.getLogger(WapFrontend.class);
    private static final int LEAVE_GAME_COOLDOWN_MILLIS = 3_000;
    private final BitmapFont fontSmall;
    private final BitmapFont fontLarge;
    private final ShaderProgram fontShader;
    private final boolean debugMode;

    /**
     * the minions (graphical representation)
     **/
    private MinionAnimationManager minions;
    /**
     * the map
     **/
    private MapDrawer mapDrawer;
    private float actualRenderWidth1 = 0f;
    private float actualRenderWidth2 = 0f;

    private final float alphaValue = 0.7f;
    private final static float BAR_CHANGE_PER_MILLISECOND = 0.0002f;
    /**
     * input shit
     **/
    private Interaction interactionManager;

    /**
     * color of the conversation frame
     **/
    private Color conversationFrameColor;
    private Color auraColor1;
    private Color auraColor2;
    private Color auraColor3;
    private Color nameBackgroundColor;

    private Color blackColor;
    private Color hotZone;

    private Texture frame;
    private Texture[][] waitingScreen;

    private GameState gameState;
    private Faction myFaction;
    private SoundManager soundManager;

    GameInformation gameInformation;
    private int leaveGameCounter = 0;
    private Faction gameOver = Faction.NEUTRAL;

    private final Set<Integer> countdownSoundPlayed = new HashSet<>();

    private boolean moveLines = false;
    private final SettingsPersistenceHelper settingsPersistenceHelper;
    private final GlyphLayout glyphLayout = new GlyphLayout();

    public WapFrontend(BitmapFont fontSmall, BitmapFont fontLarge, ShaderProgram fontShader, SettingsPersistenceHelper settingsPersistenceHelper) {
        this.fontSmall = fontSmall;
        this.fontLarge = fontLarge;
        this.fontShader = fontShader;
        this.debugMode = settingsPersistenceHelper.getSettings().isDebugMode();
        this.moveLines = debugMode;
        this.settingsPersistenceHelper = settingsPersistenceHelper;
    }

    /**
     * build all necessary objects
     */
    public void init(GameStateProvider gsp,
                     Faction faction,
                     GameInformation gameInformation,
                     SoundManager soundManager,
                     Viewport viewport,
                     MoveActionHandler moveActionHandler) {
        final GameState gs = gsp.getGameState();
        LOG.debug("Frontend init game state: {}", gs);
        this.gameState = gs;
        myFaction = faction;
        this.soundManager = soundManager;
        interactionManager = new Interaction(myFaction, gsp, soundManager, viewport, moveActionHandler);
        this.gameInformation = gameInformation;
        minions = new MinionAnimationManager(gsp, interactionManager, debugMode, fontSmall, fontShader);
        countdownSoundPlayed.clear();
        countdownSoundPlayed.add(gameState.getStartCountdown());
        leaveGameCounter = 0;
        gameOver = Faction.NEUTRAL;

        frame = new Texture(Gdx.files.internal("outer_frame.png"));
        mapDrawer = new MapDrawer();
        mapDrawer.init();

        auraColor1 = new Color(255 / 255f, 141 / 255f, 12 / 255f, 255 / 255f); // rgba(255,141,12, 188)
        auraColor2 = new Color(0 / 255f, 114 / 255f, 243 / 255f, 255 / 255f); // Farbe Team 2:
        auraColor3 = new Color(90 / 255f, 90 / 255f, 90 / 255f, 255 / 255f); // Farbe Dark Grey:
        conversationFrameColor = new Color(66 / 255f, 255 / 255f, 122 / 255f, 200 / 255f);
        nameBackgroundColor = new Color(50 / 255f, 50 / 255f, 50 / 255f, 200 / 255f);
        hotZone = new Color(150 / 255f, 150 / 255f, 150 / 255f, 200 / 255f);
        blackColor = new Color(0 / 255f, 0 / 255f, 0 / 255f, 255 / 255f);

        waitingScreen = new Texture[2][6];
        waitingScreen[0][0] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_ORANGE_1)));
        waitingScreen[0][1] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_ORANGE_1)));
        waitingScreen[0][2] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_ORANGE_2)));
        waitingScreen[0][3] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_ORANGE_3)));
        waitingScreen[0][4] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_ORANGE_4)));
        waitingScreen[0][5] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_ORANGE_5)));

        waitingScreen[1][0] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_BLUE_1)));
        waitingScreen[1][1] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_BLUE_1)));
        waitingScreen[1][2] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_BLUE_2)));
        waitingScreen[1][3] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_BLUE_3)));
        waitingScreen[1][4] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_BLUE_4)));
        waitingScreen[1][5] = new Texture(Gdx.files.internal(this.settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.COUNTDOWN_BLUE_5)));
    }

    public void addMinion(Minion m) {
        minions.addMinion(m);
    }

    public void update(final int deltaInMillis, final GameState gameState) {
        minions.update(deltaInMillis, gameState);
        mapDrawer.update(deltaInMillis, gameState);
        interactionManager.deselectLostMinions();

        this.gameState = gameState;
        float winningCondition = (float) CommonValues.getNumberOfFieldsToWin(
                gameState.getGameField().getNumberOfLockedGameFieldElements()); // replace
        // this
        float player1Points = gameState.getGameField().getGameFieldFactionCountMap().get(Faction.PLAYER1);
        float player2Points = gameState.getGameField().getGameFieldFactionCountMap().get(Faction.PLAYER2);
        if(actualRenderWidth1 - (player1Points / winningCondition) < (-0.0005f * deltaInMillis)) {
            actualRenderWidth1 = actualRenderWidth1 + BAR_CHANGE_PER_MILLISECOND * deltaInMillis;
        } else if(actualRenderWidth1 - (player1Points / winningCondition) >= (0.0005f * deltaInMillis)) {
            actualRenderWidth1 = actualRenderWidth1 - BAR_CHANGE_PER_MILLISECOND * deltaInMillis;
        }
        if(actualRenderWidth2 - (player2Points / winningCondition) < (-0.0005f * deltaInMillis)) {
            actualRenderWidth2 = actualRenderWidth2 + BAR_CHANGE_PER_MILLISECOND * deltaInMillis;
        } else if(actualRenderWidth2 - (player2Points / winningCondition) >= (0.0005f * deltaInMillis)) {
            actualRenderWidth2 = actualRenderWidth2 - BAR_CHANGE_PER_MILLISECOND * deltaInMillis;
        }
        if(gameOver == Faction.NEUTRAL) {
            if(gameState.getStartCountdown() == 0) {
                gameState.update(deltaInMillis);
                gameOver = gameState.checkWinningCondition();
            }
        } else {
            if(leaveGameCounter <= LEAVE_GAME_COOLDOWN_MILLIS) {
                leaveGameCounter += deltaInMillis;
            }
            for(Minion mini : gameState.getMinionMap().values()) {
                mini.dontMove();
            }
        }
    }

    public void render(final SpriteBatch spriteBatch, final ShapeRenderer shapeRenderer) {
        mapDrawer.render(gameState, spriteBatch, shapeRenderer);
        spriteBatch.begin();
        spriteBatch.draw(frame, 0, 0);
        spriteBatch.end();
        renderConversationBackground(gameState, shapeRenderer);
        minions.render(spriteBatch, shapeRenderer);
        renderSelectionFrame(shapeRenderer);

        if(this.moveLines) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            for(final Minion m : gameState.getMinionMap().values()) {
                shapeRenderer.line(m.getX(), FrontendUtils.flipY(m.getY()), m.getTargetPosX(),
                        FrontendUtils.flipY(m.getTargetPosY()));
            }
            shapeRenderer.end();
        }
        // WAITING FOR GAME:
        if(gameState.getStartCountdown() > 0) {
            if(!Faction.NEUTRAL.equals(myFaction)) {
                spriteBatch.begin();
                spriteBatch.draw(waitingScreen[myFaction.ordinal() - 1][gameState.getStartCountdown()], 0, 0);
                spriteBatch.end();
            }

            if(!countdownSoundPlayed.contains(gameState.getStartCountdown())) {
                countdownSoundPlayed.add(gameState.getStartCountdown());
                soundManager.playSound(SoundManager.Sound.GAME_COUNTDOWN_4_TO_1);
            }
        }
        // GAME IS RUNNING:
        else if(gameState.getStartCountdown() == 0) {
            renderConversations(gameState, spriteBatch, shapeRenderer);
            renderNamesAndWinningBar(spriteBatch, shapeRenderer);

            if(!countdownSoundPlayed.contains(gameState.getStartCountdown())) {
                countdownSoundPlayed.add(gameState.getStartCountdown());
                soundManager.playSound(SoundManager.Sound.GAME_COUNTDOWN_0);
            }
            if(gameOver != Faction.NEUTRAL) {
                spriteBatch.begin();
//                font.getData().setScale(3f);
                fontLarge.setColor(Color.BLACK);
                final String colorString = settingsPersistenceHelper.getLocalizedProperty(gameOver);
                fontLarge.draw(spriteBatch, settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.VICTORY, colorString), 323, FrontendUtils.flipY(323));
                fontLarge.setColor(Color.WHITE);
                fontLarge.draw(spriteBatch, settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.VICTORY, colorString), 320, FrontendUtils.flipY(320));

                if(leaveGameCounter >= LEAVE_GAME_COOLDOWN_MILLIS) {
                    fontLarge.setColor(Color.BLACK);
                    fontLarge.draw(spriteBatch, settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.ESC_TO_CONTINUE), 323, FrontendUtils.flipY(403));
                    fontLarge.setColor(Color.WHITE);
                    fontLarge.draw(spriteBatch, settingsPersistenceHelper.getLocalizedProperty(WapFrontendText.ESC_TO_CONTINUE), 320, FrontendUtils.flipY(400));
                }
                spriteBatch.end();
            }
        }
    }

    public int getLeaveGameCounter() {
        return leaveGameCounter;
    }

    private void renderNamesAndWinningBar(final SpriteBatch spriteBatch, final ShapeRenderer shapeRenderer) {
        int barOffset = 230;
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(nameBackgroundColor);
        //FIXME: MagicNumberPattern
        shapeRenderer.rect(16, FrontendUtils.flipY(16), CommonValues.NUMBER_OF_COLUMNS
                * CommonValues.WIDTH_OF_MAP_ELEMENT - 32, 16);

        int maxWidth = CommonValues.NUMBER_OF_COLUMNS * CommonValues.WIDTH_OF_MAP_ELEMENT / 2;

        // draw hot zone
        shapeRenderer.setColor(hotZone);
        //FIXME: MagicNumberPattern
        shapeRenderer.rect(barOffset, FrontendUtils.flipY(12), maxWidth * 2 - barOffset * 2, 5);
        shapeRenderer.setColor(conversationFrameColor);
        //FIXME: MagicNumberPattern
        shapeRenderer.rect(maxWidth - 2, FrontendUtils.flipY(14), 4, 9);

        // draw winning bar 1
        float ta = auraColor1.a;
        //FIXME: MagicNumberPattern
        auraColor1.a = alphaValue;
        shapeRenderer.setColor(auraColor1);
        float barWidth1 = actualRenderWidth1 * (maxWidth - barOffset);
        //FIXME: MagicNumberPattern
        shapeRenderer.rect(barOffset, FrontendUtils.flipY(14), barWidth1, 9);
        auraColor1.a = ta;

        // draw winning bar 2
        ta = auraColor2.a;
        //FIXME: MagicNumberPattern
        auraColor2.a = alphaValue;
        shapeRenderer.setColor(auraColor2);
        float barWidth2 = actualRenderWidth2 * (maxWidth - barOffset);
        //FIXME: MagicNumberPattern
        shapeRenderer.rect((2.0f * maxWidth - barWidth2) - barOffset, FrontendUtils.flipY(14), barWidth2, 9);
        auraColor2.a = ta;

        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
        spriteBatch.begin();
        spriteBatch.setShader(fontShader);
        // name 1
        glyphLayout.setText(fontSmall, gameInformation.getPlayerNames().get(Faction.PLAYER1));
        float nameWidth = glyphLayout.width;
        fontSmall.setColor(blackColor);
        //FIXME: MagicNumberPattern
        fontSmall.draw(spriteBatch, gameInformation.getPlayerNames().get(Faction.PLAYER1), barOffset
                - nameWidth - 10, FrontendUtils.flipY(4));
        fontSmall.setColor(auraColor1);
        //FIXME: MagicNumberPattern
        fontSmall.draw(spriteBatch, gameInformation.getPlayerNames().get(Faction.PLAYER1), barOffset
                - nameWidth - 9, FrontendUtils.flipY(3));

        // name 2
        fontSmall.setColor(blackColor);
        //FIXME: MagicNumberPattern
        fontSmall.draw(spriteBatch, gameInformation.getPlayerNames().get(Faction.PLAYER2), maxWidth * 2
                - barOffset + 10, FrontendUtils.flipY(4));
        fontSmall.setColor(auraColor2);
        //FIXME: MagicNumberPattern
        fontSmall.draw(spriteBatch, gameInformation.getPlayerNames().get(Faction.PLAYER2), maxWidth * 2
                - barOffset + 11, FrontendUtils.flipY(3));
        spriteBatch.setShader(null);
        spriteBatch.end();
    }


    private void renderSelectionFrame(final ShapeRenderer shapeRenderer) {
        // mouseInteraction.
        if(interactionManager.currentMouseState == Interaction.SelectionState.SELECTING) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(conversationFrameColor);
            shapeRenderer.rect(
                    interactionManager.getSelectionStart().x,
                    FrontendUtils.flipY(interactionManager.getSelectionStart().y),
                    interactionManager.getSelectionEnd().x - interactionManager.getSelectionStart().x,
                    // since the Y axis is flipped, we need to draw in the opposite direction
                    interactionManager.getSelectionStart().y - interactionManager.getSelectionEnd().y);
            shapeRenderer.end();
        }
    }

    private void renderConversationBackground(final GameState gs, final ShapeRenderer shapeRenderer) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for(Conversation conversation : gs.getIdConversationMap().values()) {
            // render conversation circle
            shapeRenderer.setColor(this.conversationFrameColor);
            shapeRenderer.circle(conversation.getConvoCenterX(), FrontendUtils.flipY(conversation.getConvoCenterY()),
                    60.0f);
        }
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    private void renderConversations(final GameState gs, SpriteBatch spriteBatch, final ShapeRenderer shapeRenderer) {
        float height = 20;
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        if(!debugMode) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        }
        for(Conversation conversation : gs.getIdConversationMap().values()) {
            float minX = Float.MAX_VALUE;
            float maxX = Float.MIN_VALUE;
            float minY = Float.MAX_VALUE;
            float maxY = Float.MIN_VALUE;
            // determine the position of the conversation bar
            for(Minion minion : conversation.getParticipants()) {
                if(minion.getX() < minX) {
                    minX = minion.getX();
                }
                if(minion.getX() > maxX) {
                    maxX = minion.getX();
                }
                if(minion.getY() - height - minion.getMinionType().getHeight() / 2f < minY) {
                    minY = minion.getY() - height - minion.getMinionType().getHeight() / 2f;
                }
                if(minion.getY() + minion.getMinionType().getHeight() > maxY) {
                    maxY = minion.getY() + minion.getMinionType().getHeight();
                }
            }

            float width = Math.max(100, maxX - minX);
            final float barX = (minX + maxX) / 2.0f - width / 2.0f;
            final float barY = minY < height + 16 ? maxY : minY; // 16 is the height of the name bar at the top

            if(debugMode) {
                spriteBatch.begin();
                spriteBatch.setShader(fontShader);
                fontSmall.setColor(Color.PURPLE);
                fontSmall.draw(spriteBatch, "" + conversation.getId(), barX, FrontendUtils.flipY(barY));
                spriteBatch.setShader(null);
                spriteBatch.end();
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            }
            // the outer Box which contains the progress bars.
            shapeRenderer.setColor(conversationFrameColor);
            //FIXME: MagicNumberPattern
            shapeRenderer.rect(barX, FrontendUtils.flipY(barY), width + 4, height);


            // if a prophet is part of the convo or only one attacking faction is active, only one bar should be shown
            if(conversation.hasProphet()) {
                if(conversation.getConversationOverview().getPlayer1Percentage() != 0.0f) {
                    shapeRenderer.setColor(this.auraColor1);
                    shapeRenderer.rect(barX, FrontendUtils.flipY(barY), width
                            * conversation.getConversationOverview().getPlayer1Percentage(), height);
                } else if(conversation.getConversationOverview().getPlayer2Percentage() != 0.0f) {
                    shapeRenderer.setColor(this.auraColor2);
                    shapeRenderer.rect(barX, FrontendUtils.flipY(barY), width
                            * conversation.getConversationOverview().getPlayer2Percentage(), height);
                } else if(conversation.getConversationOverview().getGreyPercentage() != 0.0f) {
                    shapeRenderer.setColor(this.auraColor3);
                    shapeRenderer.rect(barX, FrontendUtils.flipY(barY), width
                            * conversation.getConversationOverview().getGreyPercentage(), height);
                }
            }
            // if multiple factions convo for victory show status of both
            else {
                shapeRenderer.setColor(this.auraColor1);
                shapeRenderer.rect(barX, FrontendUtils.flipY(barY), width
                        * conversation.getConversationOverview().getPlayer1Percentage(), height / 2);
                shapeRenderer.setColor(this.auraColor2);
                shapeRenderer.rect(barX, FrontendUtils.flipY(barY) + height / 2, width
                        * conversation.getConversationOverview().getPlayer2Percentage(), height / 2);
            }
            if(debugMode) {
                shapeRenderer.end();
            }
        }
        if(!debugMode) {
            shapeRenderer.end();
        }
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void deselectMinion() {
        this.interactionManager.deselectMinion();
    }

    public void toggleMoveLines() {
        this.moveLines = !this.moveLines;
    }

    public InputProcessor getInputProcessor() {
        return interactionManager;
    }

    public void setGameInformation(GameInformation gameInformation) {
        this.gameInformation = gameInformation;
    }

    public Interaction getInteractionManager() {
        return interactionManager;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    /**
     * Resets the frontend state to the beginning of the game.
     */
    public void reset() {
        interactionManager.deselectMinion();
        gameOver = Faction.NEUTRAL;
        countdownSoundPlayed.clear();
        countdownSoundPlayed.add(gameState.getStartCountdown());
    }
}
