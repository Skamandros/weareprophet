/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.frontend;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * shows particles to define the minion faction
 *
 * @author grimmer
 */
public class Aura {

    /**
     * @author grimmer
     */
    private class AuraParticle {
        float x; // current x
        float y; // current y
        float sx; // start x
        float sy; // start y
        float lifeTime; // range: 0 to 1
        float alpha;
        Color color;

        public AuraParticle(float sx, float sy, float tx, float ty, Color c,
                            float alpha) {
            this.x = sx;
            this.y = sy;
            this.sx = sx;
            this.sy = sy;
            this.lifeTime = 0;
            this.color = c;
            this.alpha = alpha;
        }
    }

    Random rnd;
    List<AuraParticle> particles; // holds the particles
    int currentSpawnCd; // cool down for spawning
    final int spawnCd = 40;
    final float startRadius = 20.0f;
    final float targetRadius = 30.0f;
    final float absLifeTime = 1000.0f;
    float tx; // target x
    float ty; // target y

    public Aura() {
        particles = new LinkedList<AuraParticle>();
        rnd = new Random();
    }

    public void addParticle(int x, int y, Color c) {
        float alpha = rnd.nextFloat() * 6.283f;
        float startX = x + (float) (Math.cos(alpha) * startRadius);
        float startY = y + (float) (Math.sin(alpha) * startRadius);
        AuraParticle p = new AuraParticle(startX, startY, x, y, c, alpha);
        particles.add(p);
    }

    public boolean maySpawn() {
        return currentSpawnCd < 0;
    }

    public void resetSpawnCd() {
        currentSpawnCd = spawnCd;
    }

    public void update(int delta, float tx, float ty) {
        if(currentSpawnCd >= 0) {
            currentSpawnCd -= delta;
        }

        this.tx = tx;
        this.ty = ty;

        Iterator<AuraParticle> iter = particles.iterator();
        while(iter.hasNext()) {
            AuraParticle ap = iter.next();
            ap.lifeTime += ((float) delta / absLifeTime);
            ap.sx = tx + (float) (Math.cos(ap.alpha) * startRadius);
            ap.sy = ty + (float) (Math.sin(ap.alpha) * startRadius);
            float ttx = tx + (float) (Math.cos(ap.alpha) * targetRadius);
            float tty = ty + (float) (Math.sin(ap.alpha) * targetRadius);
            ap.x = ap.sx * (1.0f - ap.lifeTime) + ttx * (ap.lifeTime);
            ap.y = ap.sy * (1.0f - ap.lifeTime) + tty * (ap.lifeTime);
            if(ap.lifeTime > 1.0f) {
                iter.remove();
            }
        }
    }

    public void render(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for(AuraParticle ap : particles) {
            shapeRenderer.setColor(ap.color);
            shapeRenderer.rect(ap.x, FrontendUtils.flipY(ap.y), 5.0f, 5.0f);

        }
        shapeRenderer.end();
    }
}
