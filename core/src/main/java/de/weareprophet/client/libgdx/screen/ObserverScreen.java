/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import de.ggj14.wap.common.communication.ServerCommunicationHelper;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.server.ProphetConnector;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.frontend.WapFrontend;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ObserverScreen extends ScreenAdapter {

    private static final Logger LOG = LogManager.getLogger(ObserverScreen.class);

    private final ProphetConnector psc;
    private ServerCommunicationHelper communicationHelper;

    private final WapGame game;
    private WapFrontend frontend;

    private GameInformation gameInformation;

    private SpriteBatch spriteBatch;
    private ShapeRenderer shapeRenderer;
    private final ScreenHelper screenHelper;

    private ScheduledExecutorService updateExecutor;


    public ObserverScreen(final WapGame game, final ProphetConnector psc, final GameInformation gameInformation) {
        super();
        this.game = game;
        this.psc = psc;
        this.gameInformation = gameInformation;
        this.screenHelper = new ScreenHelper(game, psc);
    }

    @Override
    public void show() {
        LOG.info("Entering Observer-Mode");
        frontend = new WapFrontend(game.getFont12(), game.getFont48(), game.getFontShader(),
                game.getSettingsPersistenceHelper());
        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(game.getViewport().getCamera().combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(game.getViewport().getCamera().combined);
        screenHelper.show();

        try {
            gameInformation = psc.getGameInformation();
            LOG.info("Joined a multiplayer game {} as observer", gameInformation);
            this.communicationHelper = new ServerCommunicationHelper(psc);
            frontend.init(this.communicationHelper, Faction.NEUTRAL, gameInformation, game.getSoundManager(),
                    game.getViewport(),
                    m -> {
                    });

            final InputMultiplexer inputProcessor = new InputMultiplexer();
            inputProcessor.addProcessor(screenHelper.getIngameInputHandler());
            inputProcessor.addProcessor(frontend.getInputProcessor());
            Gdx.input.setInputProcessor(inputProcessor);

            updateExecutor = Executors.newScheduledThreadPool(1);
            updateExecutor.scheduleAtFixedRate(new Update(), 0, 10, TimeUnit.MILLISECONDS);
        } catch(Exception e) {
            LOG.error("Unable to retrieve game state", e);
            game.switchToTitleScreen();
        }
    }

    @Override
    public void hide() {
        LOG.info("Leaving Observer-Mode");
        screenHelper.hide();
        spriteBatch.dispose();
        shapeRenderer.dispose();
        if(communicationHelper != null) {
            this.communicationHelper.close();
        }
        if(updateExecutor != null) {
            updateExecutor.shutdownNow();
            try {
                if(!updateExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
                    LOG.error("Update executor didn't shut down in time");
                }
            } catch(InterruptedException e) {
                LOG.warn("Update executor shutdown interrupted");
            }
        }
    }

    @Override
    public synchronized void render(float deltaInSeconds) {
        final GameState gameState = this.communicationHelper.getGameState();
        if(gameState.getStartCountdown() == 0) {
            frontend.render(spriteBatch, shapeRenderer);
        }
        this.screenHelper.render(deltaInSeconds);
    }

    private final class Update implements Runnable {
        private long lastUpdate = System.nanoTime();


        @Override
        public void run() {
            final long currentTime = System.nanoTime();
            final int deltaInMillis = (int) (currentTime - lastUpdate) / 1_000_000;

            final GameState gameState = communicationHelper.getGameState();
            synchronized(ObserverScreen.this) {
                frontend.update(deltaInMillis, gameState);
            }

            lastUpdate = currentTime;
            LOG.trace("Update run execution time: {} ns", System.nanoTime() - currentTime);
        }
    }

}
