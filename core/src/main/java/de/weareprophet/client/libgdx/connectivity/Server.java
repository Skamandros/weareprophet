/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.connectivity;

import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.communication.dto.LobbySettings;
import de.ggj14.wap.common.communication.dto.ProgramVersion;
import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.lobby.Lobby;
import de.ggj14.wap.common.server.JoinFailedException;
import de.ggj14.wap.common.server.KryonetServerConnector;
import de.ggj14.wap.common.server.LobbyInvitationListener;
import de.ggj14.wap.common.server.ProphetConnector;

import java.security.KeyPair;
import java.util.Collections;
import java.util.SortedSet;
import java.util.concurrent.ExecutorService;

public final class Server {

    private static final Server OFFLINE_INSTANCE = new Server("Offline", new ProphetConnector() {

        @Override
        public void connect(String playerName) {
        }

        @Override
        public void disconnect() {

        }

        @Override
        public void leaveLobby() {

        }

        @Override
        public void createLobby(String otherPlayer) throws JoinFailedException {
            throw new JoinFailedException("Game creation is not possible in offline mode");
        }

        @Override
        public Faction joinLobby(int lobbyId) throws JoinFailedException {
            throw new JoinFailedException("Game joining is not possible in offline mode");
        }

        @Override
        public void lobbyChangeSettings(LobbySettings toSet) {

        }

        @Override
        public void observeLobby(int lobbyId) throws JoinFailedException {
            throw new JoinFailedException("Game joining is not possible in offline mode");
        }

        @Override
        public void lobbyChangeReadyState(boolean ready) {

        }

        @Override
        public void lobbyStartGame() {

        }

        @Override
        public void declineInvitation(int lobbyId) {
        }

        @Override
        public Lobby getCurrentLobby() {
            return null;
        }

        @Override
        public GameState getCurrentGameState() throws IllegalStateException {
            throw new IllegalStateException("No game in progress");
        }

        @Override
        public GameInformation getGameInformation() {
            return null;
        }

        @Override
        public SortedSet<Lobby> listMyLobbies() {
            return Collections.emptySortedSet();
        }

        @Override
        public SortedSet<Lobby> listOtherLobbies() {
            return Collections.emptySortedSet();
        }

        @Override
        public SortedSet<String> getPlayersOnServer() {
            return Collections.emptySortedSet();
        }

        @Override
        public boolean isConnected() {
            return false;
        }

        @Override
        public void addListener(GameStateUpdateListener toAdd) {

        }

        @Override
        public void removeListener(GameStateUpdateListener toRemove) {

        }

        @Override
        public void addListener(ConnectionStateListener toAdd) {

        }

        @Override
        public void removeListener(ConnectionStateListener toRemove) {

        }

        @Override
        public int getPing() {
            return 0;
        }

        @Override
        public void handleMoveAction(MoveAction moveAction) {
        }
    });
    private final ProphetConnector connector;

    private final String name;

    public static Server offline() {
        return OFFLINE_INSTANCE;
    }

    public static Server kryonet(final String serverName, final String server, final int tcpPort, final int udpPort, final ExecutorService threadPool, ProgramVersion version, KeyPair clientKeys, final LobbyInvitationListener lobbyInvitationListener) {
        final ProphetConnector connector = new KryonetServerConnector(server, tcpPort, udpPort, threadPool, version, clientKeys, lobbyInvitationListener);
        return new Server(serverName, connector);
    }

    private Server(String name, ProphetConnector connector) {
        this.connector = connector;
        this.name = name;
    }

    public ProphetConnector getConnector() {
        return connector;
    }

    @Override
    public String toString() {
        return name;
    }
}
