/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.settings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.ggj14.wap.common.CommonValues;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Locale;

class ClientSettingsImpl implements ClientSettings {

    private String playerName = DEFAULT_PLAYER_NAME;

    private boolean musicOn = true;

    private boolean soundOn = true;

    private float volume = 1f;

    private boolean fullScreenMode = false;

    private Locale locale = Locale.US;

    private boolean debugMode = false;

    private String privateKeyEncodedBase64;

    private String publicKeyEncodedBase64;

    public String getPrivateKeyEncodedBase64() {
        return privateKeyEncodedBase64;
    }

    public void setPrivateKeyEncodedBase64(String privateKeyEncodedBase64) {
        this.privateKeyEncodedBase64 = privateKeyEncodedBase64;
    }

    public String getPublicKeyEncodedBase64() {
        return publicKeyEncodedBase64;
    }

    public void setPublicKeyEncodedBase64(String publicKeyEncodedBase64) {
        this.publicKeyEncodedBase64 = publicKeyEncodedBase64;
    }

    @Override
    public String getPlayerName() {
        return playerName;
    }

    @Override
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    @Override
    public boolean isMusicOn() {
        return musicOn;
    }

    @Override
    public void setMusicOn(boolean musicOn) {
        this.musicOn = musicOn;
    }

    @Override
    public boolean isSoundOn() {
        return soundOn;
    }

    @Override
    public void setSoundOn(boolean soundOn) {
        this.soundOn = soundOn;
    }

    @Override
    public float getVolume() {
        return volume;
    }

    @Override
    public void setVolume(float volume) {
        this.volume = volume;
    }

    @Override
    public boolean isFullScreenMode() {
        return fullScreenMode;
    }

    @Override
    public void setFullScreenMode(boolean fullScreenMode) {
        this.fullScreenMode = fullScreenMode;
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public boolean isDebugMode() {
        return debugMode;
    }

    @Override
    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    @JsonIgnore
    public KeyPair getKeys() throws NoSuchAlgorithmException, InvalidKeySpecException {
        if(this.privateKeyEncodedBase64 == null || this.publicKeyEncodedBase64 == null) {
            return null;
        }
        final KeyFactory kf = KeyFactory.getInstance(CommonValues.CLIENT_KEY_ALGORITHM);
        PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(this.privateKeyEncodedBase64));
        final PrivateKey privateKey = kf.generatePrivate(privateSpec);
        X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.getDecoder().decode(this.publicKeyEncodedBase64));
        final PublicKey publicKey = kf.generatePublic(publicSpec);
        return new KeyPair(publicKey, privateKey);
    }

    @JsonIgnore
    void setKeys(KeyPair keys) {
        this.privateKeyEncodedBase64 = Base64.getEncoder().encodeToString(keys.getPrivate().getEncoded());
        this.publicKeyEncodedBase64 = Base64.getEncoder().encodeToString(keys.getPublic().getEncoded());
    }
}
