/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.settings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.I18NBundle;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.I18NProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Locale;

public class SettingsPersistenceHelper implements AutoCloseable {
    static final String I18N_BUNDLE_NAME = "i18n/wap-localization";
    private static final Logger LOG = LogManager.getLogger(SettingsPersistenceHelper.class);
    private static final String APP_DATA_DIRECTORY_NAME = "WeAreProphet";
    private final ObjectMapper mapper = new ObjectMapper();
    private final File configFile;
    private volatile I18NBundle i18NBundle;
    private volatile ClientSettings settings;

    public SettingsPersistenceHelper(final String configFileName) {
        this.getGameDataDirectory().mkdirs();
        configFile = new File(getGameDataDirectory(), configFileName);
    }

    private void loadSettings() {
        try {
            ClientSettingsImpl result;
            if(configFile.exists()) {
                try {
                    result = mapper.readValue(configFile, ClientSettingsImpl.class);
                } catch(final IOException e) {
                    LOG.warn("Unable to load settings, falling back to defaults", e);
                    result = new ClientSettingsImpl();
                }
                if(result.getKeys() == null) {
                    result.setKeys(CommonValues.generateClientKeys());
                    saveSettings(result);
                }
            } else {
                result = new ClientSettingsImpl();
                result.setKeys(CommonValues.generateClientKeys());
            }
            this.settings = result;
            reloadLocalization(result.getLocale());
        } catch(NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalStateException("Unable to build valid settings", e);
        }
    }

    public synchronized ClientSettings getSettings() {
        if(settings == null) {
            this.loadSettings();
        }
        return settings;
    }

    public synchronized void saveSettings(ClientSettings toSave) {
        try {
            mapper.writeValue(configFile, toSave);
            this.settings = toSave;
            reloadLocalization(toSave.getLocale());
        } catch(IOException e) {
            LOG.error("Unable to save config file!", e);
        }
    }

    private void reloadLocalization(Locale locale) {
        LOG.info("Reloading localization bundle for locale {}", locale);
        this.i18NBundle = I18NBundle.createBundle(Gdx.files.internal(I18N_BUNDLE_NAME), locale);
    }

    public synchronized String getLocalizedProperty(I18NProperty propertyToGet, String... args) {
        return this.i18NBundle.format(propertyToGet.getKey(), (Object[]) args);
    }

    @Override
    public void close() throws Exception {
    }

    /**
     * Returns the directory where the game stores data, e.g. the settings.
     * Please not that this <strong>is not</strong> the game directory itself.
     *
     * @return The directory where game data (not the game itself) is stored
     */
    public File getGameDataDirectory() {
        return new File(System.getProperty("user.home"), APP_DATA_DIRECTORY_NAME);
    }
}
