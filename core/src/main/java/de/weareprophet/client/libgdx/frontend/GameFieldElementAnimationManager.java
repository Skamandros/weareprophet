/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.frontend;

import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import de.ggj14.wap.common.datamodel.GameFieldElementPosition;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.gamefield.GameFieldElement;

import java.util.HashMap;
import java.util.Map;

public class GameFieldElementAnimationManager {
    private final Map<GameFieldElementPosition, GameFieldElementVisualizationState> elementVisualizationState;

    public GameFieldElementAnimationManager() {
        elementVisualizationState = new HashMap<>();
    }

    public void update(int deltaInMillis, GameState gs) {
        for(ObjectObjectCursor<GameFieldElementPosition, GameFieldElement> gf : gs.getGameField().getGameFieldMap()) {
            if(!elementVisualizationState.containsKey(gf.key))
                elementVisualizationState.put(gf.key, new GameFieldElementVisualizationState());

            elementVisualizationState.get(gf.key).update(deltaInMillis, gs, gf.value.getFieldFaction());
        }
    }

    public float getBrightnessOfField(GameFieldElementPosition key) {
        if(elementVisualizationState.get(key) == null)
            return 0.15f;
        return elementVisualizationState.get(key).getBrightness();
    }
}
