/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.ScreenUtils;
import de.ggj14.wap.common.server.ProphetConnector;
import de.weareprophet.client.libgdx.WapGame;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class ScreenHelper implements ProphetConnector.ConnectionStateListener {
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss");

    private final WapGame game;
    private final ProphetConnector psc;
    private final Dialog dcWindow;
    private final File screenshotDirectory;
    private final Label screenshotNotification;
    private Stage overlayStage;

    ScreenHelper(WapGame game, ProphetConnector psc) {
        this.game = game;
        this.psc = psc;
        this.dcWindow = new Dialog("Disconnect", game.getSkin());
        dcWindow.text("You have been disconnected from the server, attempting reconnect...");
        screenshotDirectory = game.getSettingsPersistenceHelper().getGameDataDirectory();
        this.screenshotNotification = new Label("Screenshot saved to " + screenshotDirectory.getAbsolutePath(), game.getSkin(), "message");
        this.screenshotNotification.setVisible(false);
    }

    void show() {
        this.overlayStage = new Stage(game.getViewport());
        Container<Label> container = new Container<>(screenshotNotification);
        container.setFillParent(true);
        container.align(Align.bottom);
        container.padBottom(20);
        overlayStage.addActor(container);
        this.psc.addListener(this);
    }

    void hide() {
        this.psc.removeListener(this);
        this.overlayStage.dispose();
    }

    void render(final float deltaInSeconds) {
        overlayStage.act(deltaInSeconds);
        overlayStage.draw();
    }

    /**
     * @return An input processor for in-game screens, i.e. {@link SingleplayerScreen} and {@link MultiplayerScreen}.
     */
    InputProcessor getIngameInputHandler() {
        return new InputMultiplexer(
                new InputAdapter() {
                    @Override
                    public boolean keyDown(int keycode) {
                        if(keycode == Input.Keys.ESCAPE) {
                            leaveGame();
                            return true;
                        } else if(keycode == Input.Keys.F12) {
                            screenshot(screenshotDirectory);
                            return true;
                        }
                        return false;
                    }
                }, overlayStage);
    }

    /**
     * @return An input handler for {@link LobbyScreen}.
     */
    InputProcessor getLobbyInputHandler() {
        return new InputMultiplexer(
                new InputAdapter() {
                    @Override
                    public boolean keyDown(int keycode) {
                        if(keycode == Input.Keys.ESCAPE) {
                            psc.leaveLobby();
                            game.switchToTitleScreen();
                            return true;
                        } else if(keycode == Input.Keys.F12) {
                            screenshot(screenshotDirectory);
                            return true;
                        }
                        return false;
                    }
                }, overlayStage);
    }

    private void screenshot(File outputDirectory) {
        byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), true);

        // This loop makes sure the whole screenshot is opaque and looks exactly like what the user is seeing
        for(int i = 4; i < pixels.length; i += 4) {
            pixels[i - 1] = (byte) 255;
        }

        Pixmap pixmap = new Pixmap(Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), Pixmap.Format.RGBA8888);
        BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
        PixmapIO.writePNG(Gdx.files.absolute(new File(outputDirectory, "screenshots/" + LocalDateTime.now().format(DATE_FORMAT) + ".png").getAbsolutePath()), pixmap);
        pixmap.dispose();
        screenshotNotification.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.fadeIn(.5f),
                Actions.delay(2),
                Actions.fadeOut(.5f)));
        screenshotNotification.setVisible(true);
    }

    private void leaveGame() {
        psc.leaveLobby();
        game.switchToTitleScreen();
    }

    @Override
    public void disconnected() {
        dcWindow.show(overlayStage);
    }

    @Override
    public void connected() {
        dcWindow.hide();
    }
}
