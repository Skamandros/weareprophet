/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.frontend;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameFieldElementPosition;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.gamefield.GameFieldElement;
import de.weareprophet.client.libgdx.WapGame;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MapDrawer {

    private Map<Faction, Color> fractionColors;

    private GameFieldElementAnimationManager gfeAnimationManager;

    private Texture background;

    public void init() {
        fractionColors = new HashMap<>();
        this.gfeAnimationManager = new GameFieldElementAnimationManager();
        initColors();
        Random rnd = new Random();
        int l = rnd.nextInt(3) + 1;
        String filename = "map/level" + l + ".png";
        background = new Texture(Gdx.files.internal(filename));
    }

    private void initColors() {
        for(Faction f : Faction.values()) {
            switch(f) {
                case PLAYER1:// red
                    fractionColors.put(Faction.PLAYER1, WapGame.FACTION_1_OVERLAY_COLOR);
                    break;
                case PLAYER2:// green
                    fractionColors.put(Faction.PLAYER2, WapGame.FACTION_2_OVERLAY_COLOR);
                    break;
                case NEUTRAL:
                default:
                    fractionColors.put(Faction.NEUTRAL, new Color(0 / 255f, 0 / 255f, 0 / 255f, 0 / 255f));
                    break;
            }
        }
    }

    public void update(int deltaInMillis, GameState gs) {
        gfeAnimationManager.update(deltaInMillis, gs);
    }

    public void render(GameState gs, SpriteBatch batch, ShapeRenderer shapeRenderer) {
        // render the background
        batch.begin();
        batch.draw(background, 0, 0);
        batch.end();

        // render the fences
        for(ObjectObjectCursor<GameFieldElementPosition, GameFieldElement> gf : gs
                .getGameField().getGameFieldMap()) {
            float domState = gf.value.getDominationState();

            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

            if(gs.isGreyProphetModeActive()
                    && !gf.value.isGameFieldLocked()
                    && gf.value.isWasCapturedAtLeastOnce()
                    && gf.value.getFieldFaction().equals(Faction.NEUTRAL)) {
                //&& gf.value.getFactionCountMap().containsKey(Faction.GREY)) {
                batch.begin();
                float absDomState = Math.abs(domState);

                if(absDomState <= 5.0f && absDomState > 4.0f) {
                    batch.draw(Roots.ROOT_1.getTexture(), gf.value.getField().x, FrontendUtils.flipY(gf.value.getField().y) - CommonValues.HEIGHT_OF_MAP_ELEMENT);
                } else if(absDomState <= 4.0f && absDomState > 3.0f) {
                    batch.draw(Roots.ROOT_2.getTexture(), gf.value.getField().x, FrontendUtils.flipY(gf.value.getField().y) - CommonValues.HEIGHT_OF_MAP_ELEMENT);
                } else if(absDomState <= 3.0f && absDomState > 2.0f) {
                    batch.draw(Roots.ROOT_3.getTexture(), gf.value.getField().x, FrontendUtils.flipY(gf.value.getField().y) - CommonValues.HEIGHT_OF_MAP_ELEMENT);
                } else if(absDomState <= 2.0f && absDomState > 1.0f) {
                    batch.draw(Roots.ROOT_4.getTexture(), gf.value.getField().x, FrontendUtils.flipY(gf.value.getField().y) - CommonValues.HEIGHT_OF_MAP_ELEMENT);
                } else if(absDomState <= 1.0f && absDomState >= 0.0f) {
                    batch.draw(Roots.ROOT_5.getTexture(), gf.value.getField().x, FrontendUtils.flipY(gf.value.getField().y) - CommonValues.HEIGHT_OF_MAP_ELEMENT);
                }
                batch.end();
            } else if(gf.value.isGameFieldLocked()) {
                batch.begin();
                batch.draw(Roots.ROOT_6.getTexture(), gf.value.getField().x, FrontendUtils.flipY(gf.value.getField().y) - CommonValues.HEIGHT_OF_MAP_ELEMENT);
                batch.end();
            }
            // the standard case, where a element is either orange, blue or grey without prophet interaction
            else {
                Gdx.gl.glEnable(GL20.GL_BLEND);
                Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

                Rectangle rectangle = gf.value.getField();
                Color color = fractionColors.get(gf.value.getFieldFaction());

                float tw = Math.abs(domState) / 2f;
                float fenceAlpha = Math.abs(domState / 40.0f);

                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(color.r, color.g, color.b, fenceAlpha);

                // top fence
                shapeRenderer.rect(rectangle.x, FrontendUtils.flipY(rectangle.y) - CommonValues.HEIGHT_OF_MAP_ELEMENT, rectangle.width, tw);
                // right fence
                shapeRenderer.rect(rectangle.x + CommonValues.WIDTH_OF_MAP_ELEMENT - tw, FrontendUtils.flipY(rectangle.y) - CommonValues.HEIGHT_OF_MAP_ELEMENT, tw, rectangle.height);
                // bottom fence
                shapeRenderer.rect(rectangle.x, FrontendUtils.flipY(rectangle.y) - tw, rectangle.width, tw);
                // left fence
                shapeRenderer.rect(rectangle.x, FrontendUtils.flipY(rectangle.y) - CommonValues.HEIGHT_OF_MAP_ELEMENT, tw, rectangle.height);

                // ground layer
                if(gf.value.getFieldFaction() != Faction.NEUTRAL) {
                    shapeRenderer.setColor(color.r, color.g, color.b, gfeAnimationManager.getBrightnessOfField(gf.key));
                    shapeRenderer.rect(rectangle.x + tw, FrontendUtils.flipY(rectangle.y) + tw - CommonValues.HEIGHT_OF_MAP_ELEMENT, CommonValues.WIDTH_OF_MAP_ELEMENT - 2 * tw, CommonValues.WIDTH_OF_MAP_ELEMENT - 2 * tw);
                }

                shapeRenderer.end();
            }
        }

        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

}
