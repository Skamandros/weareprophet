/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.ai.AI;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.MinionType;
import de.ggj14.wap.common.gamefield.objects.Minion;
import de.weareprophet.client.LocalOnlyGameState;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.connectivity.Server;
import de.weareprophet.client.libgdx.frontend.FrontendUtils;
import de.weareprophet.client.libgdx.frontend.WapFrontend;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SingleplayerScreen extends ScreenAdapter {
    private static final Logger LOG = LogManager.getLogger(SingleplayerScreen.class);

    private final WapGame game;
    private final ScreenHelper screenHelper;
    private LocalOnlyGameState gameState;
    private WapFrontend frontend;

    private final Faction myFaction = Faction.PLAYER1;
    private final AI ai;
    private final GameInformation gameInformation;
    private int timeSinceStart;

    private SpriteBatch spriteBatch;
    private ShapeRenderer shapeRenderer;
    private ScheduledExecutorService updateExecutor;

    /**
     * If this is 0, the cheat mode is active
     */
    private int cheatMode = 5;

    public SingleplayerScreen(final WapGame game, final AI aiToUse, final GameInformation gameInformation) {
        super();
        this.game = game;
        this.ai = aiToUse;
        this.gameInformation = gameInformation;
        this.screenHelper = new ScreenHelper(game, Server.offline().getConnector());
    }

    @Override
    public void show() {
        LOG.info("Entering Singleplayer-Mode");

        screenHelper.show();
        frontend = new WapFrontend(game.getFont12(), game.getFont48(), game.getFontShader(),
                game.getSettingsPersistenceHelper());
        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(game.getViewport().getCamera().combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(game.getViewport().getCamera().combined);

        final int columns = CommonValues.NUMBER_OF_COLUMNS;
        final int rows = CommonValues.NUMBER_OF_ROWS;
        timeSinceStart = 0;

        gameState = new LocalOnlyGameState();
        gameState.init(rows, columns, CommonValues.MINIONS_AT_MAP, true, false, gameInformation.isGreyProphetMode());
        gameState.setStartCountdown(4);
        ai.init(rows, columns, Faction.PLAYER2,
                m -> gameState.setMinionTargetPosition(m.objectID(), m.targetX(), m.targetY()));

        frontend.init(() -> gameState, myFaction, gameInformation, game.getSoundManager(), game.getViewport(),
                m -> gameState.doMoveAction(m));

        final InputMultiplexer inputProcessor = new InputMultiplexer();
        inputProcessor.addProcessor(screenHelper.getIngameInputHandler());
        inputProcessor.addProcessor(frontend.getInputProcessor());
        inputProcessor.addProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if(cheatMode > 0) {
                    if(keycode == Input.Keys.F4) {
                        cheatMode--;
                    }
                } else {
                    /* Cheat 'P': Show all move orders */
                    if(keycode == Input.Keys.P) {
                        frontend.toggleMoveLines();
                    }
                    /* Cheat '#': Uber prophet */
                    if(keycode == 76) {
                        gameState.getMinionMap().get(0).setMinionType(MinionType.UBER_PROPHET);
                    }
                    /* Cheat 'l': Gain a little child for free */
                    if(keycode == Input.Keys.L) {
                        addMinion(MinionType.CHILD, Faction.PLAYER1);
                    }
                    /* Cheat 'o': Gain an old men for free */
                    if(keycode == Input.Keys.O) {
                        addMinion(MinionType.OLD_MAN, Faction.PLAYER1);
                    }
                    /* Cheat 'y': Gain a young men for free */
                    if(keycode == Input.Keys.Y) {
                        addMinion(MinionType.YOUNG_MAN, Faction.PLAYER1);
                    }
                    /* Cheat 'g': Gain a gospel woman for free */
                    if(keycode == Input.Keys.G) {
                        addMinion(MinionType.GOSPEL_WOMAN, Faction.PLAYER1);
                    }
                }
                return true;
            }
        });
        Gdx.input.setInputProcessor(inputProcessor);

        updateExecutor = Executors.newScheduledThreadPool(1);
        updateExecutor.scheduleAtFixedRate(new Update(), 0, 10, TimeUnit.MILLISECONDS);
    }

    @Override
    public void hide() {
        LOG.info("Leaving Singleplayer-Mode");
        spriteBatch.dispose();
        shapeRenderer.dispose();
        screenHelper.hide();
        if(updateExecutor != null) {
            updateExecutor.shutdownNow();
            try {
                if(!updateExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
                    LOG.error("Update executor didn't shut down in time");
                }
            } catch(InterruptedException e) {
                LOG.warn("Update executor shutdown interrupted");
            }
        }
    }

    @Override
    public synchronized void render(float deltaInSeconds) {
        try {
            frontend.render(spriteBatch, shapeRenderer);
            screenHelper.render(deltaInSeconds);
        } catch(Exception e) {
            LOG.fatal("Uncaught exception during rendering", e);
            throw e;
        }
    }

    private void addMinion(MinionType type, Faction fact) {
        Minion newMinion = this.gameState.addMinion(type, fact, FrontendUtils.calculateStartingPoint(fact));
        this.frontend.addMinion(newMinion);
    }

    private final class Update implements Runnable {
        private long lastUpdate = System.nanoTime();


        @Override
        public void run() {
            final long currentTime = System.nanoTime();
            final int deltaInMillis = (int) (currentTime - lastUpdate) / 1_000_000;

            if(gameState.getStartCountdown() == 0) {
                ai.update(gameState);
            }

            timeSinceStart += deltaInMillis;
            synchronized(SingleplayerScreen.this) {
                // reduce count down for playing
                if(gameState.getStartCountdown() != 0 && (4 - (timeSinceStart / 1000)) < gameState.getStartCountdown()) {
                    gameState.setStartCountdown(4 - (timeSinceStart / 1000));
                    LOG.debug("Countdown is {}", gameState.getStartCountdown());
                    return;
                }
                frontend.update(deltaInMillis, gameState);
            }

            lastUpdate = currentTime;
        }
    }

}
