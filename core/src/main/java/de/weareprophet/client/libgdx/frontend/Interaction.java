/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.weareprophet.client.libgdx.frontend;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;
import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.GameStateProvider;
import de.ggj14.wap.common.gamefield.MoveActionHandler;
import de.ggj14.wap.common.gamefield.objects.Minion;
import de.weareprophet.client.libgdx.sound.SoundManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Interaction extends InputAdapter {

    public final static float MAX_DISTANCE = 40.0f;
    public final static int NOTHING_SELECTED = -1;
    private final Faction myFaction;
    private final GameStateProvider gameStateProvider;
    private final SoundManager sound;
    private final Viewport viewport;
    private final MoveActionHandler moveActionHandler;

    public static enum SelectionState {
        NORMAL, SELECTING;
    }

    public SelectionState currentMouseState = SelectionState.NORMAL;

    private final List<Integer> selectedMinions;

    private final Point start = new Point();
    private final Point end = new Point();

    public List<Integer> getSelectedMinionIds() {
        return selectedMinions;
    }

    public Interaction(Faction faction, GameStateProvider gameStateProvider, SoundManager sound, Viewport viewport, MoveActionHandler moveActionHandler) {
        myFaction = faction;
        this.gameStateProvider = gameStateProvider;
        this.sound = sound;
        this.viewport = viewport;
        this.moveActionHandler = moveActionHandler;
        selectedMinions = Collections.synchronizedList(new ArrayList<>());
    }

    public void handleTab(final GameState game) {
        sound.playSound(SoundManager.Sound.SELECT_MINION);
        // switch to next minion
        // case 1: there is no minion selected -> select the first
        if(selectedMinions.isEmpty()) {
            for(Minion minion : game.getMinionMap().values()) {
                if(minion.getFaction().equals(myFaction)) {
                    selectedMinions.add(minion.getId());
                    return;
                }
            }
        }
        // case 2: there is already one minion selected -> select the next
        // in the list, or the first if the last was already selected
        else {
            int currentSelected = selectedMinions.get(0);
            int first = currentSelected;
            // find the smallest minionID greater than currentSelected
            int smallest = Integer.MAX_VALUE;
            for(Minion minion : game.getMinionMap().values()) {
                if(!minion.getFaction().equals(myFaction)) {
                    continue;
                }
                if(minion.getId() < first) {
                    first = minion.getId();
                }
                if(minion.getId() > currentSelected && minion.getId() < smallest) {
                    smallest = minion.getId();
                }
            }
            this.deselectMinion();
            if(smallest != Integer.MAX_VALUE) {
                selectedMinions.add(smallest);
            } else {
                selectedMinions.add(first);
            }
        }
    }

    public void evaluateSelection(final GameState game) {
        if(Math.abs(start.x - end.x) <= 10 && Math.abs(start.y - end.y) <= 10) {
            // normal ONE click selection
            int selectedMinionId = NOTHING_SELECTED;
            // select an minion with left click:

            selectedMinionId = checkSelectMinion(game, start.x, start.y);
            // a minion was selected
            if(selectedMinionId != NOTHING_SELECTED) {
                this.deselectMinion();
                this.selectedMinions.add(selectedMinionId);
                sound.playSound(SoundManager.Sound.SELECT_MINION);
            }
            // no minion was selected
            else {
                sound.playSound(SoundManager.Sound.DESELECT_MINION);
                this.deselectMinion();

            }
            return;
        }

        // selection box mode
        this.deselectMinion();
        float top = Math.min(start.y, end.y);
        float bottom = Math.max(start.y, end.y);
        float left = Math.min(start.x, end.x);
        float right = Math.max(start.x, end.x);

        for(Minion minion : game.getMinionMap().values()) {
            if(minion.getFaction().equals(myFaction) && minion.getX() > left && minion.getX() < right && minion.getY() > top && minion.getY() < bottom) {
                selectedMinions.add(minion.getId());
            }
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        final Vector2 unprojected = viewport.unproject(new Vector2(screenX, screenY));
        if(button == Input.Buttons.LEFT) {
            currentMouseState = SelectionState.SELECTING;
            start.x = Math.round(unprojected.x);
            start.y = Math.round(FrontendUtils.flipY(unprojected.y));
            end.x = Math.round(unprojected.x);
            end.y = Math.round(FrontendUtils.flipY(unprojected.y));
            return true;
        } else if(button == Input.Buttons.RIGHT) {
            for(Integer i : selectedMinions) {
                // get the corresponding move action
                final MoveAction ma = new MoveAction(i, Math.round(unprojected.x), Math.round(FrontendUtils.flipY(unprojected.y)));
                moveActionHandler.handleMoveAction(ma);
            }
            if(!selectedMinions.isEmpty()) {
                sound.playSound(SoundManager.Sound.MOVE_MINION);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if(currentMouseState == SelectionState.SELECTING) {
            final Vector2 unprojected = viewport.unproject(new Vector2(screenX, screenY));
            end.x = Math.round(unprojected.x);
            end.y = Math.round(FrontendUtils.flipY(unprojected.y));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(button == Input.Buttons.LEFT) {
            evaluateSelection(gameStateProvider.getGameState());
            currentMouseState = SelectionState.NORMAL;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.TAB) {
            handleTab(gameStateProvider.getGameState());
            return true;
        } else {
            return false;
        }
    }

    /**
     * returns the new minion id if a new one was clicked otherwise it returns
     * -1
     */
    int checkSelectMinion(GameState game, int mouseX, int mouseY) {
        int bestMinionId = -1;
        float bestMinionDistance = Float.MAX_VALUE;
        for(Minion minion : game.getMinionMap().values()) {
            float distance = getDistanceFromMinion(minion, mouseX, mouseY);
            if(distance < bestMinionDistance && distance < Interaction.MAX_DISTANCE && minion.getFaction().equals(myFaction)) {
                bestMinionDistance = distance;
                bestMinionId = minion.getId();
            }
        }
        return bestMinionId;
    }

    private float getDistanceFromMinion(Minion minion, int mouseX, int mouseY) {
        return (float) Math.sqrt((mouseX - minion.getX()) * (mouseX - minion.getX()) + (mouseY - minion.getY()) * (mouseY - minion.getY()));
    }

    public void deselectMinion() {
        selectedMinions.clear();
    }

    /**
     * This method checks whether the selected minions still belong to this
     * player's faction. If this is not the case, they are removed from the
     * selection.
     */
    public void deselectLostMinions() {
        Map<Integer, Minion> minions = gameStateProvider.getGameState().getMinionMap();
        this.selectedMinions.removeIf(id -> !minions.containsKey(id) || !minions.get(id).getFaction().equals(this.myFaction));
    }

    public Point getSelectionStart() {
        return start;
    }

    public Point getSelectionEnd() {
        return end;
    }
}
