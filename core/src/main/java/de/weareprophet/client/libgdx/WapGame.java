/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import de.ggj14.wap.common.ProphetProperties;
import de.ggj14.wap.common.communication.dto.ProgramVersion;
import de.weareprophet.client.libgdx.screen.TitleScreen;
import de.weareprophet.client.libgdx.settings.ClientSettings;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;
import de.weareprophet.client.libgdx.sound.SoundManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class WapGame extends Game {
    public static final int WORLD_WIDTH = 1080;
    public static final int WORLD_HEIGHT = 720;
    public static final int DEFAULT_VIEWPORT_WIDTH = WORLD_WIDTH;
    public static final int DEFAULT_VIEWPORT_HEIGHT = WORLD_HEIGHT;
    /**
     * The color for faction 1, with some alpha so it can be used for overlays.
     */
    public static final Color FACTION_1_OVERLAY_COLOR = new Color(255 / 255f, 141 / 255f, 12 / 255f, 188 / 255f);
    /**
     * The color for faction 2, with some alpha so it can be used for overlays.
     */
    public static final Color FACTION_2_OVERLAY_COLOR = new Color(12 / 255f, 126 / 255f, 255 / 255f, 188 / 255f);
    /**
     * The color for faction 1, without alpha.
     */
    public static final Color FACTION_1_SOLID_COLOR = new Color(255 / 255f, 141 / 255f, 12 / 255f, 255 / 255f);
    /**
     * The color for faction 2, without alpha.
     */
    public static final Color FACTION_2_SOLID_COLOR = new Color(0 / 255f, 114 / 255f, 243 / 255f, 255 / 255f);


    private static final Pattern CONFIG_FILE_NAME_PATTERN = Pattern.compile("[a-zA-Z0-9-]+[.]json");
    private static final String DEFAULT_CONFIG_FILE_NAME = "prophet-config.json";

    private static final Logger LOG = LogManager.getLogger(WapGame.class);
    private final String configFileName;

    private Viewport viewport;
    private SoundManager soundManager;
    private SettingsPersistenceHelper settingsPersistenceHelper;
    private TitleScreen titleScreen;
    private ScheduledExecutorService executor;
    private Skin skin;

    private BitmapFont font48;

    private BitmapFont font12;

    private BitmapFont font12boldFlipped;

    private BitmapFont font12flipped;
    private ShaderProgram fontShader;

    public WapGame(final String[] arg) {
        if(arg.length >= 1 && CONFIG_FILE_NAME_PATTERN.matcher(arg[0]).matches()) {
            this.configFileName = arg[0];
        } else {
            this.configFileName = DEFAULT_CONFIG_FILE_NAME;
        }
    }

    @Override
    public void create() {
        viewport = new FitViewport(WORLD_WIDTH, WORLD_HEIGHT);
        settingsPersistenceHelper = new SettingsPersistenceHelper(configFileName);
        final ClientSettings settings = settingsPersistenceHelper.getSettings();
        soundManager = new SoundManager(settingsPersistenceHelper);
        executor = Executors.newScheduledThreadPool(6);
        if(settings.isFullScreenMode()) {
            Graphics.Monitor currMonitor = Gdx.graphics.getMonitor();
            Graphics.DisplayMode displayMode = Gdx.graphics.getDisplayMode(currMonitor);
            if(!Gdx.graphics.setFullscreenMode(displayMode)) {
                LOG.error("Starting the game in full screen has failed, falling back to window mode");
            }
        }
        skin = new Skin(Gdx.files.internal("shade/skin/uiskin.json"));
        font12 = new BitmapFont(Gdx.files.internal("font/player-names.fnt"));
        font12.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        font12boldFlipped = new BitmapFont(Gdx.files.internal("font/roboto-black-12-df.fnt"), true);
        font12boldFlipped.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        font12flipped = new BitmapFont(Gdx.files.internal("font/player-names.fnt"), true);
        font12flipped.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        font48 = new BitmapFont(Gdx.files.internal("shade/skin/font-large.fnt"));
        font48.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        fontShader = new ShaderProgram(Gdx.files.internal("font/font.vert"), Gdx.files.internal("font/font.frag"));
        if(!fontShader.isCompiled()) {
            Gdx.app.error("fontShader", "compilation failed:\n" + fontShader.getLog());
        }


        try {
            final ProgramVersion version = new ProphetProperties().getVersion();
            titleScreen = new TitleScreen(this, settings, version, executor);
            setScreen(titleScreen);
            LOG.info("Client version {}, config file {}, startup complete", version, configFileName);
        } catch(IOException e) {
            LOG.warn("Unable to read version from properties", e);
            throw new IllegalStateException("Unable to read application properties", e);
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        try {
            super.render();
        } catch(Exception e) {
            LOG.fatal("Game rendering has failed", e);
            Gdx.app.exit();
        }
    }

    @Override
    public void dispose() {
        font48.dispose();
        font12.dispose();
        soundManager.dispose();
        super.dispose();
        executor.shutdownNow();
        try {
            if(!executor.awaitTermination(10, TimeUnit.SECONDS)) {
                LOG.warn("Unable to shutdown executor properly");
            }
        } catch(InterruptedException e) {
            LOG.warn("Unable to shutdown executor properly", e);
        }
        LOG.info("Client shutdown complete");
    }

    public Viewport getViewport() {
        return viewport;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public SettingsPersistenceHelper getSettingsPersistenceHelper() {
        return settingsPersistenceHelper;
    }

    public void switchToTitleScreen() {
        setScreen(titleScreen);
    }

    public Skin getSkin() {
        return skin;
    }

    public BitmapFont getFont12() {
        return font12;
    }

    public BitmapFont getFont12boldFlipped() {
        return font12boldFlipped;
    }

    public BitmapFont getFont12flipped() {
        return font12flipped;
    }

    public BitmapFont getFont48() {
        return font48;
    }

    public ShaderProgram getFontShader() {
        return fontShader;
    }
}
