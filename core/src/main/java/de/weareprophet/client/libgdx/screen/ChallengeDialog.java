/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.weareprophet.client.libgdx.settings.SettingsPersistenceHelper;

public class ChallengeDialog extends Dialog {
    public ChallengeDialog(Skin skin, SettingsPersistenceHelper settingsPersistenceHelper, final String challengerName,
                           final ClickListener acceptListener,
                           final ClickListener declineListener) {
        super(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.CHALLENGE_RECEIVED, challengerName), skin);
        final Label textLabel = new Label(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.CHALLENGE_QUESTION), skin);
        this.text(textLabel);
        TextButton accept = new TextButton(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.CHALLENGE_ACCEPT), skin, "round");
        accept.setColor(Color.GREEN);
        accept.addListener(acceptListener);
        TextButton decline = new TextButton(settingsPersistenceHelper.getLocalizedProperty(TitleScreenText.CHALLENGE_DECLINE), skin, "round");
        decline.setColor(Color.RED);
        decline.addListener(declineListener);
        this.button(accept);
        this.button(decline);
    }
}
