/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client.libgdx.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.ggj14.wap.common.ai.AI;
import de.ggj14.wap.common.ai.EasyAI;
import de.ggj14.wap.common.ai.HardOpBallAI;
import de.ggj14.wap.common.ai.MediumEnhancedAI;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.datamodel.Faction;
import de.weareprophet.client.libgdx.WapGame;
import de.weareprophet.client.libgdx.settings.ClientSettings;

import java.util.HashMap;

/**
 * Enumerates game modes that can be played locally.
 */
public enum SingleplayerMode {
    TUTORIAL {
        @Override
        boolean isEnabled(ClientSettings clientSettings) {
            return true;
        }

        @Override
        Button getButton(WapGame game, Skin skin, Texture star, Texture starWhite) {
            final Button tutorialButton = new TextButton("Tutorial", skin, "round");
            tutorialButton.addListener(new ClickListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    HashMap<Faction, String> nameMap = new HashMap<>();
                    final ClientSettings clientSettings = game.getSettingsPersistenceHelper().getSettings();
                    nameMap.put(Faction.PLAYER1, clientSettings.getPlayerName());
                    nameMap.put(Faction.PLAYER2, "");
                    final TutorialScreen tutorialScreen = new TutorialScreen(game, new GameInformation(
                            GameInformation.NO_GAME_ID, nameMap, false));
                    game.setScreen(tutorialScreen);
                    return true;
                }
            });
            return tutorialButton;
        }
    },
    EASY_AI {
        @Override
        boolean isEnabled(ClientSettings clientSettings) {
            return true;
        }

        @Override
        Button getButton(WapGame game, Skin skin, Texture star, Texture starWhite) {
            final Button vsEasy = new TextButton(game.getSettingsPersistenceHelper().getLocalizedProperty(TitleScreenText.EASY_AI), skin, "round");
            vsEasy.add(new Image(star)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsEasy.add(new Image(starWhite)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsEasy.add(new Image(starWhite)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsEasy.addListener(startGameVsAiListener(game, new EasyAI()));
            return vsEasy;
        }
    },
    MEDIUM_AI("To unlock Medium, you have to defeat Easy first.") {
        @Override
        boolean isEnabled(ClientSettings clientSettings) {
            return true;
        }

        @Override
        Button getButton(WapGame game, Skin skin, Texture star, Texture starWhite) {
            final Button vsMedium = new TextButton(game.getSettingsPersistenceHelper().getLocalizedProperty(TitleScreenText.MEDIUM_AI), skin, "round");
            vsMedium.add(new Image(star)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsMedium.add(new Image(star)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsMedium.add(new Image(starWhite)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsMedium.addListener(startGameVsAiListener(game, new MediumEnhancedAI()));
            return vsMedium;
        }
    },
    HARD_AI("To unlock Hard, you have to defeat Medium first.") {
        @Override
        boolean isEnabled(ClientSettings clientSettings) {
            return true;
        }

        @Override
        Button getButton(WapGame game, Skin skin, Texture star, Texture starWhite) {
            final Button vsHard = new TextButton(game.getSettingsPersistenceHelper().getLocalizedProperty(TitleScreenText.HARD_AI), skin, "round");
            vsHard.add(new Image(star)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsHard.add(new Image(star)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsHard.add(new Image(star)).width(STAR_WIDTH).height(STAR_HEIGHT);
            vsHard.addListener(startGameVsAiListener(game, new HardOpBallAI()));
            return vsHard;
        }
    };

    private static final int STAR_WIDTH = 20;
    private static final int STAR_HEIGHT = 20;

    public static final int ELEMENT_WIDTH = 200;

    private final String disabledMessage;

    SingleplayerMode() {
        this(null);
    }

    SingleplayerMode(final String disabledMessage) {
        this.disabledMessage = disabledMessage;
    }

    abstract boolean isEnabled(ClientSettings clientSettings);

    abstract Button getButton(WapGame game, Skin skin, Texture star, Texture starWhite);

    public final Actor getScreenElement(WapGame game, Skin skin, Texture star, Texture starWhite) {
        final Stack result = new Stack();
        result.add(getButton(game, skin, star, starWhite));
        if(!isEnabled(game.getSettingsPersistenceHelper().getSettings())) {
            final Image lockOverlay = new Image(new Texture(Gdx.files.internal("lock-overlay.png")));
            if(disabledMessage != null) {
                lockOverlay.addListener(new TextTooltip(disabledMessage, skin));
            }
            result.add(lockOverlay);
        }
        return result;
    }

    ClickListener startGameVsAiListener(WapGame game, AI ai) {
        return new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                HashMap<Faction, String> nameMap = new HashMap<>();
                final ClientSettings clientSettings = game.getSettingsPersistenceHelper().getSettings();
                nameMap.put(Faction.PLAYER1, clientSettings.getPlayerName());
                nameMap.put(Faction.PLAYER2, ai.getName());
                // to re-introduce grey prophet mode replace the last argument with
                // clientSettings.isGreyProphetMode()
                final SingleplayerScreen singleplayerScreen = new SingleplayerScreen(game, ai, new GameInformation(
                        GameInformation.NO_GAME_ID, nameMap, false));
                game.setScreen(singleplayerScreen);
                return true;
            }
        };
    }
}
