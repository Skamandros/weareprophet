/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.weareprophet.client;

import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.MinionType;
import de.ggj14.wap.common.gamefield.objects.Minion;
import de.weareprophet.client.libgdx.WapGame;

import java.awt.geom.Point2D;
import java.util.TreeMap;

/**
 * A game state that is for local-only purposes and contains functionality that is not supported in multiplayer games.
 */
public class LocalOnlyGameState extends GameState {

    private boolean running = true;

    private boolean updateGameField = true;

    public void initTutorial() {
        this.running = false;
        this.updateGameField = false;
        super.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, new TreeMap<>(), true, false);
        this.addMinion(MinionType.PROPHET, Faction.PLAYER1, new Point2D.Float(ORANGE_PROPHET_SPAWN_POINT_X, ORANGE_PROPHET_SPAWN_POINT_Y));
        setStartCountdown(0);
    }

    public void initSpeedDemo() {
        this.running = false;
        this.updateGameField = false;
        super.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, new TreeMap<>(), true, false);
        final Minion prophet = this.addMinion(MinionType.PROPHET, Faction.PLAYER2, new Point2D.Float(75, 75));
        prophet.setTargetPosX(1_000);
        final Minion oldMan = this.addMinion(MinionType.GOSPEL_WOMAN, Faction.PLAYER2, new Point2D.Float(75, 200));
        oldMan.setTargetPosX(1_000);
        final Minion gospelWoman = this.addMinion(MinionType.OLD_MAN, Faction.PLAYER2, new Point2D.Float(75, 325));
        gospelWoman.setTargetPosX(1_000);
        final Minion youngMan = this.addMinion(MinionType.YOUNG_MAN, Faction.PLAYER2, new Point2D.Float(75, 450));
        youngMan.setTargetPosX(1_000);
        final Minion child = this.addMinion(MinionType.CHILD, Faction.PLAYER2, new Point2D.Float(75, 575));
        child.setTargetPosX(1_000);
        setStartCountdown(0);
    }

    public void initPersuasionDemo() {
        this.running = false;
        this.updateGameField = false;
        super.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, new TreeMap<>(), true, false);
        this.addMinion(MinionType.GOSPEL_WOMAN, Faction.PLAYER1, new Point2D.Float(350, 250));
        this.addMinion(MinionType.YOUNG_MAN, Faction.PLAYER2, new Point2D.Float(BLUE_PROPHET_SPAWN_POINT_X, BLUE_PROPHET_SPAWN_POINT_Y));
        this.addMinion(MinionType.PROPHET, Faction.PLAYER2, new Point2D.Float(725, 450));
        setStartCountdown(0);
    }

    public void initPowerDemo() {
        this.running = false;
        this.updateGameField = false;
        super.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, new TreeMap<>(), true, false);
        this.addMinion(MinionType.GOSPEL_WOMAN, Faction.PLAYER2, new Point2D.Float(100, 180));
        this.addMinion(MinionType.CHILD, Faction.PLAYER2, new Point2D.Float(100, 360));
        this.addMinion(MinionType.OLD_MAN, Faction.PLAYER2, new Point2D.Float(100, 540));
        this.addMinion(MinionType.YOUNG_MAN, Faction.PLAYER1, new Point2D.Float(150, 180));
        this.addMinion(MinionType.YOUNG_MAN, Faction.PLAYER1, new Point2D.Float(150, 360));
        this.addMinion(MinionType.YOUNG_MAN, Faction.PLAYER1, new Point2D.Float(150, 540));
        this.addMinion(MinionType.GOSPEL_WOMAN, Faction.PLAYER2, new Point2D.Float(WapGame.WORLD_WIDTH - 40, 650));
        setStartCountdown(0);
    }

    public void initInvulDemo() {
        this.running = false;
        this.updateGameField = false;
        super.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, new TreeMap<>(), true, false);
        this.addMinion(MinionType.PROPHET, Faction.PLAYER1, new Point2D.Float(900, 600));
        this.addMinion(MinionType.CHILD, Faction.PLAYER2, new Point2D.Float(920, 610));
        Minion oldMan = this.addMinion(MinionType.OLD_MAN, Faction.PLAYER2, new Point2D.Float(200, 610));
        oldMan.setTargetPosX(910);
        oldMan.setTargetPosY(605);
        Minion prophet = this.addMinion(MinionType.PROPHET, Faction.PLAYER2, new Point2D.Float(50, 50));
        prophet.setTargetPosX(895);
        prophet.setTargetPosY(605);
        setStartCountdown(0);
    }

    public Minion addMinion(MinionType type, Faction faction, Point2D.Float position) {
        if(!this.isGlobalState()) {
            throw new IllegalStateException("This method is disallowed in local state!");
        }
        return super.addMinion(type, faction, position);
    }

    /**
     * Pauses all game state updates until {@link #resume()} is called.
     */
    public void pause() {
        this.running = false;
    }

    /**
     * Resumes game state updates paused with {@link #pause()} before.
     */
    public void resume() {
        this.running = true;
    }

    /**
     * Pauses updates to the game field, e.g. domination state, until {@link #resumeGameFieldUpdates()} is called.
     */
    public void pauseGameFieldUpdates() {
        this.updateGameField = false;
    }

    /**
     * Resumes updates to the game field, e.g. domination state.
     */
    public void resumeGameFieldUpdates() {
        this.updateGameField = true;
    }

    @Override
    public void update(int timeDelta) {
        if(running) {
            updateMinions(timeDelta);
            updateConversations(timeDelta);
            if(updateGameField) {
                gameField.update(timeDelta);
            }
            this.time += timeDelta;
        }
    }
}
