/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.app;

import com.esotericsoftware.kryonet.serialization.Serialization;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.ai.AI;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.lobby.LobbyPhase;
import de.ggj14.wap.common.server.ConnectionFailedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AIBasedTestClient extends TestClient {
    private static final Logger LOG = LogManager.getLogger(AIBasedTestClient.class);

    private final ScheduledExecutorService exe;
    private final AI ai;
    private final Phaser phaser;

    private final CountDownLatch gameOver = new CountDownLatch(1);

    AIBasedTestClient(ScheduledExecutorService exe, AI ai, Faction assignedFaction, String host, int tcpPort, int udpPort, Phaser phaser, Serialization serialization) throws NoSuchAlgorithmException, ConnectionFailedException, IOException {
        super(exe, host, tcpPort, udpPort, serialization);
        this.exe = exe;
        this.ai = ai;
        this.phaser = phaser;
        this.ai.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, assignedFaction, getConnector());
        phaser.register();

    }

    void play() {
        exe.scheduleWithFixedDelay(() -> {
            try {
                final LobbyPhase lobbyPhase = getConnector().getCurrentLobby().getCurrentPhase();
                try {
                    if(lobbyPhase.equals(LobbyPhase.FINISHED) && gameOver.getCount() == 1) {
                        LOG.info("Game is over");
                        gameOver.countDown();
                        super.close();
                        phaser.arriveAndDeregister();
                    } else if(gameOver.getCount() == 1) {
                        final GameState currentGameState = getConnector().getCurrentGameState();
                        ai.update(currentGameState);
                    }
                } catch(Exception e) {
                    LOG.error("Exception in AI client during lobby phase {}", lobbyPhase, e);
                }
            } catch(InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, 50, 50, TimeUnit.MILLISECONDS);
    }

    @Override
    public void close() {
        try {
            LOG.info("Waiting for game to be over...");
            gameOver.await();
        } catch(InterruptedException e) {
        }
        super.close();
    }
}
