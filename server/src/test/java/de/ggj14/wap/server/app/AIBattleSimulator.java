/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.app;

import com.esotericsoftware.kryonet.serialization.KryoSerialization;
import de.ggj14.wap.common.ai.EasyAI;
import de.ggj14.wap.common.ai.HardOpBallAI;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.server.ConnectionFailedException;
import de.ggj14.wap.common.server.JoinFailedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Executors;
import java.util.concurrent.Phaser;
import java.util.concurrent.ScheduledExecutorService;

public class AIBattleSimulator {

    private static final Logger LOG = LogManager.getLogger(AIBattleSimulator.class);

    private static ScheduledExecutorService EXE;


    public static void main(String[] args) {
        final String host;
        final int tcpPort;
        final int udpPort;
        final int clientCount;
        if(args.length < 4) {
            host = "localhost";
            tcpPort = 8091;
            udpPort = 8092;
            clientCount = 1;
        } else {
            host = args[0];
            tcpPort = Integer.parseInt(args[1]);
            udpPort = Integer.parseInt(args[2]);
            clientCount = Integer.parseInt(args[3]);
        }
        EXE = Executors.newScheduledThreadPool(clientCount * 3);

        final Phaser phaser = new Phaser();
        try {
            for(int i = 0; i < clientCount; i++) {
                EXE.submit(() -> {
                    try {
                        startGame(host, tcpPort, udpPort, phaser);
                    } catch(Exception e) {
                        LOG.error("Exception in game execution", e);
                    }
                });
            }
            phaser.awaitAdvance(0);
        } finally {
            EXE.shutdownNow();
        }
    }

    private static void startGame(String host, int tcpPort, int udpPort, Phaser phaser) throws NoSuchAlgorithmException, ConnectionFailedException, IOException, JoinFailedException, InterruptedException {
        try(final AIBasedTestClient client1 =
                    new AIBasedTestClient(EXE, new EasyAI(), Faction.PLAYER1, host, tcpPort, udpPort, phaser, new KryoSerialization());
            final AIBasedTestClient client2 =
                    new AIBasedTestClient(EXE, new HardOpBallAI(), Faction.PLAYER2, host, tcpPort, udpPort, phaser, new KryoSerialization())) {
            client1.getConnector().createLobby(client2.getName());
            Thread.sleep(500);
            client2.acceptChallenge();
            client2.getConnector().lobbyChangeReadyState(true);
            Thread.sleep(500);
            client1.getConnector().lobbyStartGame();
            Thread.sleep(5_000);
            client1.play();
            client2.play();
        }
    }

}
