/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.app;

import com.esotericsoftware.kryonet.serialization.Serialization;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.ProphetProperties;
import de.ggj14.wap.common.datamodel.lobby.Lobby;
import de.ggj14.wap.common.server.ConnectionFailedException;
import de.ggj14.wap.common.server.JoinFailedException;
import de.ggj14.wap.common.server.KryonetServerConnector;
import de.ggj14.wap.common.server.ProphetConnector;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.Closeable;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

class TestClient implements Closeable {
    private final ProphetConnector connector;

    private final String name;

    private CountDownLatch challenged = new CountDownLatch(1);

    private int challengeLobbyId = Lobby.UNSET_LOBBY_ID;


    TestClient(final ExecutorService exe, String host, final int tcpPort, final int udpPort, Serialization serialization) throws NoSuchAlgorithmException, ConnectionFailedException, IOException {
        connector = new KryonetServerConnector(host, tcpPort, udpPort, exe,
                new ProphetProperties().getVersion(),
                CommonValues.generateClientKeys(),
                (otherPlayer, lobbyId) -> {
                    challenged.countDown();
                    challengeLobbyId = lobbyId;
                },
                serialization
        );
        name = RandomStringUtils.randomAlphanumeric(5);
        connector.connect(name);
    }

    public ProphetConnector getConnector() {
        return connector;
    }

    public String getName() {
        return name;
    }

    public void acceptChallenge() throws JoinFailedException, InterruptedException {
        if(challenged.await(5, TimeUnit.SECONDS)) {
            connector.joinLobby(challengeLobbyId);
            challenged = new CountDownLatch(1);
        } else {
            throw new IllegalStateException("No active challenge");
        }
    }

    public void declineChallenge() throws InterruptedException {
        if(challenged.await(5, TimeUnit.SECONDS)) {
            connector.declineInvitation(challengeLobbyId);
            challenged = new CountDownLatch(1);
        } else {
            throw new IllegalStateException("No active challenge");
        }
    }

    @Override
    public void close() {
        connector.disconnect();
    }

    public boolean isChallenged() {
        return challenged.getCount() == 0;
    }
}
