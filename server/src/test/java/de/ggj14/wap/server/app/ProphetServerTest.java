/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.server.app;

import com.esotericsoftware.kryonet.serialization.KryoSerialization;
import com.esotericsoftware.minlog.Log;
import de.ggj14.wap.common.ProphetProperties;
import de.ggj14.wap.common.datamodel.lobby.LobbyPhase;
import de.ggj14.wap.common.server.JoinFailedException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.assertj.core.api.Assertions.assertThat;

public class ProphetServerTest {

    private static final int TEST_PORT = 8099;
    private static ProphetServerApplication SERVER;
    private static final ExecutorService EXE = Executors.newFixedThreadPool(4);
    private static TestClient CLIENT_1;
    private static TestClient CLIENT_2;

    @BeforeClass
    public static void startServer() throws Exception {
        final KryoSerialization serialization = new KryoSerialization();
        SERVER = new ProphetServerApplication(new ProphetProperties().getVersion(),
                TEST_PORT, TEST_PORT + 1, serialization);
        SERVER.getServer().start();
        CLIENT_1 = new TestClient(EXE, "localhost", TEST_PORT, TEST_PORT + 1, serialization);
        CLIENT_2 = new TestClient(EXE, "localhost", TEST_PORT, TEST_PORT + 1, serialization);
    }

    @AfterClass
    public static void endServer() {
        CLIENT_1.close();
        CLIENT_2.close();
        SERVER.getServer().stop();
        EXE.shutdownNow();
    }

    @After
    public void leaveLobby() throws InterruptedException {
        CLIENT_1.getConnector().leaveLobby();
        CLIENT_2.getConnector().leaveLobby();
        Thread.sleep(5_000);
    }

    @Test
    public void createLobby() throws JoinFailedException, InterruptedException {
        CLIENT_1.getConnector().createLobby(CLIENT_2.getName());
        assertThat(CLIENT_1.getConnector().getCurrentLobby()).isNotNull();
        Thread.sleep(5_000);
        assertThat(CLIENT_2.isChallenged()).isTrue();
        CLIENT_2.declineChallenge();
    }

    @Test
    public void joinGame() throws JoinFailedException, InterruptedException {
        CLIENT_1.getConnector().createLobby(CLIENT_2.getName());
        CLIENT_2.acceptChallenge();
        assertThat(CLIENT_2.getConnector().getCurrentLobby()).isNotNull();
    }

    @Test(expected = JoinFailedException.class)
    public void tryToChallengeMyself() throws JoinFailedException {
        CLIENT_1.getConnector().createLobby(CLIENT_1.getName());
    }


    @Test
    public void getGameState() throws JoinFailedException, InterruptedException {
        Log.set(Log.LEVEL_DEBUG);
        CLIENT_1.getConnector().createLobby(CLIENT_2.getName());
        CLIENT_2.acceptChallenge();
        CLIENT_2.getConnector().lobbyChangeReadyState(true);
        Thread.sleep(5_000);
        CLIENT_1.getConnector().lobbyStartGame();
        Thread.sleep(5_000);
        assertThat(CLIENT_1.getConnector().getCurrentLobby().getCurrentPhase()).isEqualTo(LobbyPhase.INGAME);
        assertThat(CLIENT_2.getConnector().getCurrentLobby().getCurrentPhase()).isEqualTo(LobbyPhase.INGAME);
        assertThat(CLIENT_1.getConnector().getCurrentGameState()).isNotNull();
        assertThat(CLIENT_2.getConnector().getCurrentGameState()).isNotNull();
    }

//    @Test
//    public void issueMoveOrder() throws JoinFailedException, IOException, InterruptedException {
//        connector.createLobby("Player1");
//        SortedSet<GameInformation> openGames = connector.listOpenLobbys();
//        GameInformation gameToJoin = openGames.last();
//        assertThat(gameToJoin.getGameId()).isNotNull();
//        assertThat(gameToJoin.getPlayerNames()).isNotNull().hasSize(1);
//        connector.joinLobby(gameToJoin.getGameId(), "Player2");
//        assertThat(connector.getCurrentGameState()).isNotNull();
//
//        Integer minionId = null;
//        for (Map.Entry<Integer, Minion> minion : connector.getCurrentGameState().getMinions().entrySet()) {
//            if (minion.getValue().getFaction().equals(Faction.PLAYER2)) {
//                minionId = minion.getKey();
//                connector.handleMoveAction(new MoveAction(minion.getKey(), 50, 50, null));
//                break;
//            }
//        }
//        // The server handles move orders asynchronously, i.e. the TCP request returns before the move order is reflected in the game state.
//        Thread.sleep(100);
//        assertThat(minionId).isNotNull();
//        assertThat(connector.getCurrentGameState().getMinionMap().get(minionId).getTargetPosX()).isEqualTo(50);
//    }


}
