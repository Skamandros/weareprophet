/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.server.app;

import com.carrotsearch.hppc.IntObjectHashMap;
import com.carrotsearch.hppc.cursors.IntObjectCursor;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.gamefield.objects.Minion;
import de.ggj14.wap.server.data.GameServerState;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author SharkOfMetal
 */
public class RunningGames implements Closeable {

    private static final Logger LOG = LogManager.getLogger(RunningGames.class);
    static final AtomicInteger GAME_ID = new AtomicInteger();
    private final IntObjectHashMap<GameServerState> games = new IntObjectHashMap<>();

    private final ScheduledExecutorService exe;
    private long lastUpdate = System.nanoTime();
    private final DescriptiveStatistics updateStats;

    public RunningGames() {
        this.updateStats = new DescriptiveStatistics();
        updateStats.setWindowSize(400);
        this.exe = Executors.newScheduledThreadPool(1);
        this.exe.scheduleAtFixedRate(new UpdateGameStateTask(), 1000, 25, TimeUnit.MILLISECONDS);
    }

    public int startGame(GameServerState game) {
        int gameId = GAME_ID.getAndIncrement();
        synchronized(this) {
            games.put(gameId, game);
        }
        game.start();
        LOG.debug("Size of game map is now: {}", games.size());
        return gameId;
    }

    public synchronized void issueMoveOrder(final int gameId, Faction playerFaction, final MoveAction move) {
        LOG.debug("Received a move order for game {}: {}", gameId, move);
        if(!games.containsKey(gameId)) {
            LOG.warn("Received move action for unknown game {}", gameId);
            return;
        }
        final GameServerState gameServerState = games.get(gameId);
        final Minion minion = gameServerState.getGameState().getMinionMap().get(move.objectID());
        final Faction minionFaction = minion.getFaction();
        if(!Faction.NEUTRAL.equals(minionFaction) && minionFaction.equals(playerFaction)) {
            gameServerState.getGameState().doMoveAction(move);
        } else {
            LOG.warn("Not allowed move operation in game {} on minion: {}", gameId, minion.getId());
        }
    }

    public synchronized GameInformation getGameInformation(final int gameId) {
        if(games.containsKey(gameId)) {
            final GameInformation result = new GameInformation();
            result.setGameID(gameId);
            result.setPlayerNames(games.get(gameId).getConnectedPlayers());
            return result;
        } else {
            return null;
        }
    }

    public DescriptiveStatistics getUpdateStats() {
        return updateStats;
    }

    public void close() throws IOException {
        this.exe.shutdown();
        try {
            this.exe.awaitTermination(5, TimeUnit.SECONDS);
        } catch(InterruptedException e) {
            LOG.error("The executor did not shut down in time", e);
        }
    }

    public interface GameStateUpdateListener {
        /**
         * Notifies this listener of a game state update.
         * <p>
         * This method is called synchronously by the game state updater so don't do anything expensive like networking.
         *
         * @param gameId   The game ID.
         * @param newState The new game state of the game affected.
         */
        void update(final int gameId, final GameServerState newState);

        /**
         * This method is called if the game is disposed for inactivity. After this call, no further updates will be sent.
         */
        void disposedForInactivity();
    }

    private class UpdateGameStateTask implements Runnable {

        public void run() {
            try {
                final long now = System.nanoTime();
                synchronized(RunningGames.this) {
                    for(IntObjectCursor<GameServerState> currentElement : games) {
                        final GameServerState game = currentElement.value;
                        advanceGameState(now, currentElement.key, game);
                    }
                }
                lastUpdate = now;
                final double durationMillis = (System.nanoTime() - now) / 1_000_000d;
                updateStats.addValue(durationMillis);
                final double p95 = updateStats.getPercentile(95);
                if(p95 > 10) {
                    LOG.warn("95% percentile for game state update runs is above 10 millis: {}", p95);
                }
                LOG.trace("Advanced game state in {} ms for {} games", durationMillis, games.size());
            } catch(Exception e) {
                LOG.error("Game state progression has failed", e);
            }
        }

        private void advanceGameState(final long now, int key, final GameServerState game) {
            if(game.getStartTime() != 0 && game.getGameState().checkWinningCondition() == Faction.NEUTRAL) {
                final int deltaMillis = (int) TimeUnit.NANOSECONDS.toMillis(
                        now - Math.max(lastUpdate, game.getStartTime()));
                if(deltaMillis > 0) {
                    game.getGameState().setStartCountdown(0);
                    game.getGameState().update(deltaMillis);
                } else {
                    final int secondsLeft = Math.round((float) Math.ceil(-deltaMillis / (float) 1000));
                    if(game.getGameState().getStartCountdown() != secondsLeft) {
                        game.getGameState().setStartCountdown(secondsLeft);
                        LOG.debug("Countdown for game {}: {}s", key, secondsLeft);
                    }
                }
            }
            if(!game.checkForActivity(now)) {
                LOG.info("Game {} has no activity, disposing", key);
                game.getUpdateListener().disposedForInactivity();
                games.remove(key);
            } else {
                game.getUpdateListener().update(key, game);
            }
        }
    }
}
