/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.request;

import com.esotericsoftware.kryonet.Connection;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.communication.message.ClientAuthenticationRequest;
import de.ggj14.wap.common.communication.message.ClientAuthenticationResponse;
import de.ggj14.wap.server.data.OpenChallenge;
import de.ggj14.wap.server.data.ServerPlayer;
import de.ggj14.wap.server.data.ServerState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Optional;

public class ClientAuthenticationRequestHandler implements RequestHandler<ClientAuthenticationRequest> {
    private static final Logger LOG = LogManager.getLogger(ClientAuthenticationRequestHandler.class);

    @Override
    public void handleRequest(Connection c, ClientAuthenticationRequest request, ServerState serverState) {
        Optional<OpenChallenge> challenge = serverState.getActiveChallenge(c);
        if(challenge.isEmpty()) {
            c.sendTCP(new ClientAuthenticationResponse(false));
        } else {
            serverState.removeChallenge(c);
            byte[] clientSignature = request.clientSignature();
            try {
                Signature signer = Signature.getInstance(CommonValues.SIGNATURE_ALGORITHM);
                signer.initVerify(challenge.get().getPublicKey());
                signer.update(challenge.get().getChallengeBytes());
                if(signer.verify(clientSignature)) {
                    Optional<ServerPlayer> existingPlayer = serverState.findPlayerByPublicKey(challenge.get().getPublicKey());
                    final ServerPlayer p;
                    if(existingPlayer.isPresent()) {
                        p = existingPlayer.get();
                        if(p.getNickname().equals(challenge.get().getNickname())) {
                            LOG.info("[{}] {} has been successfully re-authenticated", c, p);
                            p.setCurrentConnection(c);
                        } else {
                            LOG.info("[{}] {} has been successfully authenticated with a new nickname {}", c, p, challenge.get().getNickname());
                            // The public key is already known, but the nickname is different
                            serverState.removePlayer(p);
                            serverState.addPlayer(new ServerPlayer(challenge.get().getNickname(), challenge.get().getPublicKey(), c));
                        }
                    } else {
                        p = new ServerPlayer(challenge.get().getNickname(), challenge.get().getPublicKey(), c);
                        LOG.info("[{}] {} has been successfully authenticated for the first time", c, p);
                        serverState.addPlayer(p);
                    }
                    c.sendTCP(new ClientAuthenticationResponse(true));
                } else {
                    LOG.warn("[{}] Authentication challenge failed", c);
                    c.sendTCP(new ClientAuthenticationResponse(false));
                    c.close();
                }
            } catch(NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
                LOG.error("[{}] Unable to verify signature", c, e);
                c.sendTCP(new ClientAuthenticationResponse(false));
            }
        }
    }
}
