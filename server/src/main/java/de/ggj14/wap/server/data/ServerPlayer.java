/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.data;

import com.esotericsoftware.kryonet.Connection;

import java.security.PublicKey;
import java.util.Optional;

public class ServerPlayer {
    private final String nickname;

    private final PublicKey publicKey;

    private long lastActivity = System.nanoTime();

    private volatile ServerLobby currentLobby = null;

    private volatile Connection currentConnection;

    public ServerPlayer(String nickname, PublicKey publicKey, Connection currentConnection) {
        this.nickname = nickname;
        this.publicKey = publicKey;
        this.currentConnection = currentConnection;
    }

    public String getNickname() {
        return nickname;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }


    public long getLastActivity() {
        return lastActivity;
    }

    public synchronized Connection getCurrentConnection() {
        return currentConnection;
    }

    /**
     * Marks this player as active.
     */
    public void touch() {
        this.lastActivity = System.nanoTime();
    }


    public void setCurrentConnection(Connection currentConnection) {
        this.currentConnection = currentConnection;
    }

    public Optional<ServerLobby> getCurrentLobby() {
        return Optional.ofNullable(currentLobby);
    }

    public void setCurrentLobby(ServerLobby currentLobby) {
        this.currentLobby = currentLobby;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;

        ServerPlayer that = (ServerPlayer) o;

        return publicKey.equals(that.publicKey);
    }

    @Override
    public int hashCode() {
        return publicKey.hashCode();
    }

    @Override
    public String toString() {
        return "ServerPlayer '" + nickname + '\'';
    }
}
