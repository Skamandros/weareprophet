/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.server.app;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.kryonet.serialization.KryoSerialization;
import com.esotericsoftware.kryonet.serialization.Serialization;
import de.ggj14.wap.common.ProphetProperties;
import de.ggj14.wap.common.communication.KryoConverter;
import de.ggj14.wap.common.communication.dto.ProgramVersion;
import de.ggj14.wap.common.communication.message.*;
import de.ggj14.wap.server.data.ServerPlayer;
import de.ggj14.wap.server.data.ServerState;
import de.ggj14.wap.server.request.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The server application of Prophet.
 *
 * @author SharkOfMetal
 */
public class ProphetServerApplication implements AutoCloseable {

    private static final Logger LOG = LogManager.getLogger(ProphetServerApplication.class);

    private final ServerState state;

    private final ExecutorService serverSideThreads = Executors.newCachedThreadPool();

    private final ScheduledExecutorService cleanupExecutor = Executors.newSingleThreadScheduledExecutor();

    private final Server server;

    private final ForwardingRequestHandler requestHandler;

    ProphetServerApplication(ProgramVersion version, final int tcpPort, int udpPort, Serialization serialization) throws IOException {
        this.state = new ServerState(serverSideThreads, cleanupExecutor);
        server = new Server(1 << 17, 1 << 14, serialization);
        KryoConverter.registerKryoClasses(server.getKryo());
        requestHandler = new ForwardingRequestHandler();
        requestHandler.registerHandler(ClientChallengeRequest.class, new ClientChallengeRequestHandler());
        requestHandler.registerHandler(ClientAuthenticationRequest.class, new ClientAuthenticationRequestHandler());
        requestHandler.registerHandler(GameInformationRequest.class, new GameInformationRequestHandler());
        requestHandler.registerHandler(CreateLobbyRequest.class, new CreateLobbyRequestHandler());
        requestHandler.registerHandler(JoinRequest.class, new JoinRequestHandler());
        requestHandler.registerHandler(ChallengeDeclinedMessage.class, new LobbyInvitationDeclinedMessageHandler());
        requestHandler.registerHandler(LeaveRequest.class, new LeaveRequestHandler());
        requestHandler.registerHandler(ListLobbiesRequest.class, new ListLobbiesRequestHandler());
        requestHandler.registerHandler(ListPlayersRequest.class, new ListPlayersRequestHandler());
        requestHandler.registerHandler(MoveAction.class, new MoveActionHandler());
        requestHandler.registerHandler(ObserveRequest.class, new ObserveRequestHandler());
        requestHandler.registerHandler(LobbyChangeSettingsMessage.class, new LobbyChangeSettingsHandler());
        requestHandler.registerHandler(LobbyReadyMessage.class, new LobbyReadyMessageHandler());
        requestHandler.registerHandler(LobbyAdvancePhaseRequest.class, new LobbyAdvancePhaseRequestHandler());
        requestHandler.registerHandler(ServerVersionRequest.class, new ServerVersionRequestHandler(version));
        server.start();
        server.bind(tcpPort, udpPort);
        server.addListener(new Listener.ThreadedListener(getMessageHandler(), serverSideThreads));
    }


    private Listener getMessageHandler() {
        return new Listener() {
            @Override
            public void received(Connection connection, Object o) {
                LOG.trace("Object received: {}", o);
                try {
                    handleRequestForExistingConnection(connection, o);
                } catch(RuntimeException e) {
                    LOG.error("Unhandled runtime exception during request processing", e);
                    connection.close();
                }
            }
        };
    }


    private void handleRequestForExistingConnection(final Connection c, final Object requestObject) {
        state.findPlayerByConnection(c).ifPresent(ServerPlayer::touch);
        if(requestObject instanceof final Message message) {
            this.requestHandler.handleRequest(c, message, state);
        }
    }

    Server getServer() {
        return server;
    }

    public static void main(String[] args) throws Exception {
        final int tcpPort = Integer.parseInt(args[0]);
        final int udpPort = Integer.parseInt(args[1]);
        final ProgramVersion version = new ProphetProperties().getVersion();
        try(final ProphetServerApplication application = new ProphetServerApplication(version, tcpPort, udpPort, new KryoSerialization())) {
            LOG.info("Server version {} startup complete. Listening on TCP port {} and UDP port {}.", version, tcpPort, udpPort);
            synchronized(ProphetServerApplication.class) {
                ProphetServerApplication.class.wait();
            }
            application.getServer().stop();
        } finally {
            LOG.info("Server shutdown complete.");
        }
    }

    @Override
    public void close() throws Exception {
        state.getRunningGames().close();
        cleanupExecutor.shutdownNow();
        serverSideThreads.shutdownNow();
        if(!serverSideThreads.awaitTermination(10, TimeUnit.SECONDS)) {
            LOG.warn("Server thread pool didn't shut down in time.");
        }
    }

}
