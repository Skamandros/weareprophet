/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.data;

import com.esotericsoftware.kryonet.Connection;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.communication.dto.LobbySettings;
import de.ggj14.wap.common.communication.dto.LobbyStateTransferObject;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.lobby.LobbyPhase;
import de.ggj14.wap.server.app.RunningGames;
import de.ggj14.wap.server.app.RunningGames.GameStateUpdateListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ServerLobby implements ActivityBasedDisposable {

    private static final Logger LOG = LogManager.getLogger(ServerLobby.class);

    private static final long MAX_INACTIVITY_NANOS = TimeUnit.MINUTES.toNanos(10);

    private static final AtomicInteger LOBBY_SEQ = new AtomicInteger(0);

    private final LobbyStateTransferObject state = new LobbyStateTransferObject();

    private final Map<ServerPlayer, Faction> presentPlayers = new ConcurrentHashMap<>();
    private final RunningGames runningGames;
    private final ExecutorService serverSideThreads;

    private final ReentrantReadWriteLock stateLock = new ReentrantReadWriteLock(true);

    private volatile long lastActivity = System.nanoTime();

    public ServerLobby(ServerPlayer host, ServerPlayer otherPlayer, RunningGames runningGames, ExecutorService serverSideThreads) {
        this.runningGames = runningGames;
        this.serverSideThreads = serverSideThreads;
        state.setLobbyId(LOBBY_SEQ.getAndIncrement());
        presentPlayers.put(host, Faction.PLAYER1);
        presentPlayers.put(otherPlayer, Faction.PLAYER2);
        state.setP1Name(host.getNickname());
        state.setP2Name(otherPlayer.getNickname());
        lobbyChanged();
        host.setCurrentLobby(this);
        // the otherPlayer is not in the lobby yet, they join after they accept the challenge
    }

    public int getId() {
        stateLock.readLock().lock();
        try {
            return state.getLobbyId();
        } finally {
            stateLock.readLock().unlock();
        }
    }

    public LobbyPhase getCurrentPhase() {
        stateLock.readLock().lock();
        try {
            return state.getCurrentPhase();
        } finally {
            stateLock.readLock().unlock();
        }
    }

    public LobbyStateTransferObject toDto() {
        stateLock.readLock().lock();
        try {
            return new LobbyStateTransferObject(state);
        } finally {
            stateLock.readLock().unlock();
        }
    }

    private void lobbyChanged() {
        activity();
        stateLock.readLock().lock();
        try {
            for(ServerPlayer p : presentPlayers.keySet()) {
                p.getCurrentConnection().sendTCP(state);
            }
        } finally {
            stateLock.readLock().unlock();
        }
    }

    public void removePlayer(ServerPlayer toRemove) {
        Faction faction = presentPlayers.get(toRemove);
        if(faction != null) {
            stateLock.writeLock().lock();
            try {
                if(Faction.NEUTRAL.equals(faction)) {
                    presentPlayers.remove(toRemove);
                    state.getObservers().remove(toRemove.getNickname());
                } else if(state.getCurrentPhase().equals(LobbyPhase.SETUP)) {
                    LOG.info("A player left lobby {} during setup", getId());
                    if(faction.equals(Faction.PLAYER1)) {
                        state.setP1Name("");
                    } else if(faction.equals(Faction.PLAYER2)) {
                        state.setP2Name("");
                    }
                    state.setCurrentPhase(LobbyPhase.FINISHED);
                }
            } finally {
                stateLock.writeLock().unlock();
            }
            lobbyChanged();
            toRemove.setCurrentLobby(null);
        }
    }

    public Faction join(ServerPlayer joining) {
        final Faction faction;
        if(presentPlayers.containsKey(joining)) {
            faction = presentPlayers.get(joining);
        } else {
            presentPlayers.put(joining, Faction.NEUTRAL);
            faction = Faction.NEUTRAL;
            stateLock.writeLock().lock();
            try {
                state.getObservers().add(joining.getNickname());
            } finally {
                stateLock.writeLock().unlock();
            }
        }
        lobbyChanged();
        joining.setCurrentLobby(this);
        return faction;
    }

    public void changeSettings(ServerPlayer actor, LobbySettings newSettings) {
        if(Faction.PLAYER1.equals(presentPlayers.get(actor)) && LobbyPhase.SETUP.equals(state.getCurrentPhase())) {
            stateLock.writeLock().lock();
            try {
                state.setSettings(newSettings);
                state.setScoreP1(newSettings.matchMode().startScoreForPlayer1);
                state.setPlayer2ready(false);
            } finally {
                stateLock.writeLock().unlock();
            }
            lobbyChanged();
        } else {
            LOG.warn("Player {} tried to change settings for lobby {} but isn't allowed to", actor, this);
        }
    }

    public void setReadyState(ServerPlayer actor, boolean toSet) {
        if(Faction.PLAYER2.equals(presentPlayers.get(actor))) {
            stateLock.writeLock().lock();
            try {
                state.setPlayer2ready(toSet);
            } finally {
                stateLock.writeLock().unlock();
            }
            lobbyChanged();
        } else {
            LOG.warn("Player {} tried to change ready state for lobby {} but isn't allowed to", actor, this);
        }
    }

    public void advanceLobbyPhase(ServerPlayer actor) {
        stateLock.writeLock().lock();
        try {
            if(Faction.PLAYER1.equals(presentPlayers.get(actor)) && state.isPlayer2ready() && EnumSet.of(LobbyPhase.SETUP,
                    LobbyPhase.PAUSE).contains(state.getCurrentPhase())) {
                final GameServerState serverGame = new GameServerState(state.getSettings().tournamentModeOn(),
                        state.getP1Name(), state.getP2Name(),
                        new GameStateUpdateListenerImpl());
                int gameId = this.runningGames.startGame(serverGame);
                state.setGameInformation(this.runningGames.getGameInformation(gameId));
                state.setCurrentPhase(LobbyPhase.INGAME);
                state.setPlayer2ready(false);
                lobbyChanged();
                LOG.info("Started game {} in lobby {}", gameId, state.getLobbyId());
            } else {
                LOG.warn("Player {} tried to advance the phase of lobby {} but isn't allowed to", actor, this);
            }
        } finally {
            stateLock.writeLock().unlock();
        }
    }

    public void declineInvite(ServerPlayer p) {
        stateLock.writeLock().lock();
        try {
            if(state.getCurrentPhase().equals(LobbyPhase.SETUP) && presentPlayers.get(p).equals(Faction.PLAYER2)) {
                state.setCurrentPhase(LobbyPhase.REJECTED);
                lobbyChanged();
                LOG.info("{} has rejected the invitation to lobby {}", p, state.getLobbyId());
            }
        } finally {
            stateLock.writeLock().unlock();
        }
    }

    public GameInformation getGameInformation() {
        stateLock.readLock().lock();
        try {
            return new GameInformation(state.getGameInformation());
        } finally {
            stateLock.readLock().unlock();
        }
    }

    /**
     * Checks whether a player has one of the provided factions in this lobby.
     *
     * @param p               The player to check.
     * @param rolesToConsider The roles to consider for this check.
     * @return True iff the player has one of the provided roles.
     */
    public boolean contains(ServerPlayer p, Set<Faction> rolesToConsider) {
        stateLock.readLock().lock();
        try {
            return presentPlayers.containsKey(p) && rolesToConsider.contains(presentPlayers.get(p));
        } finally {
            stateLock.readLock().unlock();
        }
    }

    public Faction getFaction(ServerPlayer p) {
        stateLock.readLock().lock();
        try {
            return presentPlayers.get(p);
        } finally {
            stateLock.readLock().unlock();
        }
    }

    /**
     * Checks this lobby to see whether it is finished according to the configured match mode.
     *
     * @return True iff a player has a high enough score to win the match.
     */
    private boolean isMatchOver() {
        stateLock.readLock().lock();
        try {
            final int scoreRequiredToWin = state.getSettings().matchMode().scoreRequiredToWin;
            return state.getScoreP1() >= scoreRequiredToWin || state.getScoreP2() >= scoreRequiredToWin;
        } finally {
            stateLock.readLock().unlock();
        }
    }


    @Override
    public String toString() {
        return "ServerLobby{" + state.getLobbyId() + '}';
    }

    private void activity() {
        this.activity(System.nanoTime());
    }

    @Override
    public void activity(long now) {
        this.lastActivity = now;
    }

    @Override
    public boolean checkForActivity(long now) {
        stateLock.readLock().lock();
        try {
            if(EnumSet.of(LobbyPhase.FINISHED, LobbyPhase.REJECTED).contains(this.state.getCurrentPhase())
                    || presentPlayers.keySet().stream()
                    .filter(p -> EnumSet.of(Faction.PLAYER1, Faction.PLAYER2).contains(presentPlayers.get(p)))
                    .noneMatch(p -> p.getCurrentLobby().isPresent() && p.getCurrentLobby().get().equals(this))) {
                return false;
            } else {
                return now - this.lastActivity < MAX_INACTIVITY_NANOS;
            }
        } finally {
            stateLock.readLock().unlock();
        }
    }

    @Override
    public void dispose() {
        LOG.debug("{} disposed", this);
        stateLock.writeLock().lock();
        try {
            state.setCurrentPhase(LobbyPhase.FINISHED);
        } finally {
            stateLock.writeLock().unlock();
        }
        lobbyChanged();
    }

    private final class GameStateUpdateListenerImpl implements GameStateUpdateListener {
        private volatile boolean gameHasFinished = false;

        @Override
        public void update(int gameId, GameServerState game) {
            for(ServerPlayer p : presentPlayers.keySet()) {
                Connection c = p.getCurrentConnection();
                serverSideThreads.submit(() -> {
                    try {
                        if(c.isConnected() && ServerLobby.this.equals(p.getCurrentLobby().orElse(null))) {
                            LOG.trace("Sending game state for game {} on {}", gameId, c);
                            c.sendUDP(game.getGameState().getGameStateTransferObject(gameId));
                            game.activity(System.nanoTime());
                        }
                    } catch(Exception e) {
                        LOG.warn("Exception during game state transfer", e);
                    }
                });
            }
            final Faction winnerFaction = game.checkWinningCondition();
            if(!gameHasFinished && !winnerFaction.equals(Faction.NEUTRAL)) {
                stateLock.writeLock().lock();
                try {
                    switch(winnerFaction) {
                        case PLAYER1 -> state.setScoreP1(state.getScoreP1() + 1);
                        case PLAYER2 -> state.setScoreP2(state.getScoreP2() + 1);
                    }
                    if(isMatchOver()) {
                        state.setCurrentPhase(LobbyPhase.FINISHED);
                        LOG.info("Match between '{}' and '{}' finished {}:{}", state.getP1Name(), state.getP2Name(),
                                state.getScoreP1(), state.getScoreP2());
                    } else {
                        state.setCurrentPhase(LobbyPhase.PAUSE);
                    }
                } finally {
                    stateLock.writeLock().unlock();
                }
                lobbyChanged();
                gameHasFinished = true;
                stateLock.readLock().lock();
                try {
                    LOG.debug("Finished game {} in lobby {}. Score is now {}:{}", gameId, state.getLobbyId(),
                            state.getScoreP1(), state.getScoreP2());
                } finally {
                    stateLock.readLock().unlock();
                }
            }
        }

        @Override
        public void disposedForInactivity() {
            if(!gameHasFinished) {
                stateLock.writeLock().lock();
                try {
                    state.setCurrentPhase(LobbyPhase.PAUSE);
                } finally {
                    stateLock.writeLock().unlock();
                }
                lobbyChanged();
                gameHasFinished = true;
                LOG.info("Aborted game in lobby {} for inactivity of both players.", state.getLobbyId());
            }
        }
    }
}