/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.request;

import de.ggj14.wap.common.communication.message.CreateLobbyRequest;
import de.ggj14.wap.common.communication.message.CreateLobbyResponse;
import de.ggj14.wap.common.communication.message.LobbyInvitation;
import de.ggj14.wap.common.datamodel.lobby.Lobby;
import de.ggj14.wap.server.data.ServerPlayer;
import de.ggj14.wap.server.data.ServerState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class CreateLobbyRequestHandler extends AbstractAuthenticatedRequestHandler<CreateLobbyRequest> {
    private static final Logger LOG = LogManager.getLogger(CreateLobbyRequestHandler.class);

    @Override
    protected void handleAuthenticatedRequest(ServerPlayer p, CreateLobbyRequest request, ServerState serverState) {
        Optional<ServerPlayer> otherPlayer = serverState.findPlayerByName(request.otherPlayerName());
        int lobbyId = Lobby.UNSET_LOBBY_ID;
        if(otherPlayer.isPresent() && !otherPlayer.get().equals(p)) {
            lobbyId = serverState.createLobby(p, otherPlayer.get());
            otherPlayer.get().getCurrentConnection().sendTCP(new LobbyInvitation(p.getNickname(), lobbyId));
            LOG.info("[{}] {} has created new lobby {} and invited {}", p.getCurrentConnection(), p, lobbyId, otherPlayer.get());
        }
        p.getCurrentConnection().sendTCP(new CreateLobbyResponse(lobbyId));
    }
}
