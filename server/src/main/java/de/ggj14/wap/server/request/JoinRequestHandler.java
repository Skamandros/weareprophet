/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.request;

import de.ggj14.wap.common.communication.message.JoinRequest;
import de.ggj14.wap.common.communication.message.JoinResponse;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.lobby.Lobby;
import de.ggj14.wap.server.data.ServerLobby;
import de.ggj14.wap.server.data.ServerPlayer;
import de.ggj14.wap.server.data.ServerState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class JoinRequestHandler extends AbstractAuthenticatedRequestHandler<JoinRequest> {
    private static final Logger LOG = LogManager.getLogger(JoinRequestHandler.class);

    @Override
    protected void handleAuthenticatedRequest(ServerPlayer p, JoinRequest request, ServerState serverState) {
        serverState.incrementStats(RequestType.JOIN);
        int lobbyId = request.lobbyId();
        Optional<ServerLobby> lobby = serverState.findLobbyById(lobbyId);
        Faction faction = null;
        if(lobby.isPresent()) {
            faction = lobby.get().join(p);
        } else {
            lobbyId = Lobby.UNSET_LOBBY_ID;
        }
        LOG.debug("Added connection {} for lobby {} as faction {}", p.getCurrentConnection(), lobbyId, faction);
        p.getCurrentConnection().sendTCP(new JoinResponse(lobbyId, faction));

    }
}
