/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.request;

import com.esotericsoftware.kryonet.Connection;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.communication.message.ClientChallengeRequest;
import de.ggj14.wap.common.communication.message.ClientChallengeResponse;
import de.ggj14.wap.server.data.OpenChallenge;
import de.ggj14.wap.server.data.ServerPlayer;
import de.ggj14.wap.server.data.ServerState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Optional;

public class ClientChallengeRequestHandler implements RequestHandler<ClientChallengeRequest> {
    private static final Logger LOG = LogManager.getLogger(ClientChallengeRequestHandler.class);

    @Override
    public void handleRequest(Connection c, ClientChallengeRequest request, ServerState serverState) {
        try {
            PublicKey requestPublicKey = request.getPublicKey();
            final String playerName = request.getPlayerName();
            if(!CommonValues.ALLOWED_NICKNAMES.matcher(playerName).matches()) {
                LOG.warn("[{}] Player with an invalid name has tried to connect", c);
                c.sendTCP(new ClientChallengeResponse(false, null));
            }
            Optional<ServerPlayer> playerWithSameName = serverState.findPlayerByName(playerName);
            if(playerWithSameName.isPresent() && !playerWithSameName.get().getPublicKey().equals(requestPublicKey)) {
                LOG.warn("[{}] Player with name '{}' already present on server with different key", c, playerName);
                c.sendTCP(new ClientChallengeResponse(false, null));
            } else {
                OpenChallenge challenge = serverState.newChallenge(c, requestPublicKey, playerName);
                LOG.debug("[{}] Player '{}' has been challenged to prove their identity", c, playerName);
                c.sendTCP(new ClientChallengeResponse(true, challenge.getChallengeBytes()));
            }
        } catch(InvalidKeySpecException e) {
            LOG.error("[{}] Unable to decode player key", c, e);
        }
    }
}
