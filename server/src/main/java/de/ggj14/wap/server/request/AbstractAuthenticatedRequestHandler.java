/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.request;

import com.esotericsoftware.kryonet.Connection;
import de.ggj14.wap.common.communication.message.Message;
import de.ggj14.wap.server.data.ServerPlayer;
import de.ggj14.wap.server.data.ServerState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

/**
 * Only processes requests if the current connection belongs to an authenticated player.
 *
 * @param <T> The type of requests that this handler can handle.
 */
public abstract class AbstractAuthenticatedRequestHandler<T extends Message> implements RequestHandler<T> {
    private static final Logger LOG = LogManager.getLogger(AbstractAuthenticatedRequestHandler.class);

    @Override
    public final void handleRequest(Connection c, T request, ServerState serverState) {
        Optional<ServerPlayer> playerForConnection = serverState.findPlayerByConnection(c);
        if(playerForConnection.isEmpty()) {
            LOG.warn("[{}] Request type {} requires authentication, but connection isn't", c,
                    request.getClass().getSimpleName());
            return;
        }
        ServerPlayer p = playerForConnection.get();
        p.touch();
        LOG.trace("[{}] Received request {} by {}", c, request, p);
        this.handleAuthenticatedRequest(p, request, serverState);
    }

    protected abstract void handleAuthenticatedRequest(ServerPlayer p, T request, ServerState serverState);
}
