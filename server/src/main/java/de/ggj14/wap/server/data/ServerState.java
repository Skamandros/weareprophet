/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.server.data;

import com.esotericsoftware.kryonet.Connection;
import de.ggj14.wap.common.communication.message.ListLobbiesRequest;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.lobby.LobbyPhase;
import de.ggj14.wap.server.app.RunningGames;
import de.ggj14.wap.server.request.RequestType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.PublicKey;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ServerState {
    private static final long MAX_PLAYER_INACTIVITY_NANOS = TimeUnit.DAYS.toNanos(2);
    private static final Logger LOG = LogManager.getLogger(ServerState.class);
    private final Map<Connection, OpenChallenge> activeChallenges = new ConcurrentHashMap<>();
    private final Set<ServerPlayer> connectedPlayers = new HashSet<>();

    private final List<ServerLobby> lobbies = Collections.synchronizedList(new ArrayList<>());

    private final Map<RequestType, AtomicInteger> requestStatistics = new EnumMap<>(RequestType.class);

    private final RunningGames runningGames;
    private final ExecutorService serverSideThreads;

    public ServerState(ExecutorService serverSideThreads, ScheduledExecutorService cleanupExecutor) {
        this.serverSideThreads = serverSideThreads;
        for(final RequestType t : RequestType.values()) {
            requestStatistics.put(t, new AtomicInteger());
        }
        runningGames = new RunningGames();
        cleanupExecutor.scheduleAtFixedRate(() -> {
            try {
                synchronized(ServerState.this) {
                    if(activeChallenges.keySet().removeIf(c -> !c.isConnected())) {
                        LOG.debug("Obsolete challenge removed");
                    }
                    final Set<ServerLobby> inactiveLobbies = lobbies.stream().filter(
                            l -> !l.checkForActivity(System.nanoTime())).collect(Collectors.toSet());
                    for(ServerLobby l : inactiveLobbies) {
                        l.dispose();
                        lobbies.remove(l);
                        LOG.debug("Obsolete lobby {} removed", l.getId());
                    }
                    final Iterator<ServerPlayer> playerIterator = connectedPlayers.iterator();
                    while(playerIterator.hasNext()) {
                        final ServerPlayer p = playerIterator.next();
                        if(p.getLastActivity() + MAX_PLAYER_INACTIVITY_NANOS < System.nanoTime()) {
                            p.getCurrentConnection().close();
                            playerIterator.remove();
                            LOG.info("Player [{}] has been inactive for too long and has been removed",
                                    p.getNickname());
                        }
                    }
                }
                LOG.trace("Number of served requests: {}", requestStatistics);
            } catch(Exception e) {
                LOG.error("Exception during periodic cleanup", e);
            }
        }, 10, 60, TimeUnit.SECONDS);
        cleanupExecutor.scheduleAtFixedRate(() ->
                        LOG.info("Number of served requests: {}", requestStatistics), 1, 24,
                TimeUnit.HOURS);
        cleanupExecutor.scheduleAtFixedRate(() -> {
                    if(!connectedPlayers.isEmpty()) {
                        LOG.info("Number of registered players: {}", connectedPlayers.size());
                    }
                }, 30, 120,
                TimeUnit.MINUTES);

    }

    public synchronized OpenChallenge newChallenge(Connection c, PublicKey publicKey, String nickname) {
        OpenChallenge result = new OpenChallenge(publicKey, nickname);
        this.activeChallenges.put(c, result);
        return result;
    }

    public Optional<OpenChallenge> getActiveChallenge(Connection c) {
        return Optional.ofNullable(this.activeChallenges.get(c));
    }

    public RunningGames getRunningGames() {
        return runningGames;
    }

    public int createLobby(final ServerPlayer host, final ServerPlayer otherPlayer) {
        final ServerLobby lobby = new ServerLobby(host, otherPlayer, runningGames, serverSideThreads);
        this.lobbies.add(lobby);
        return lobby.getId();
    }


    public synchronized Optional<ServerPlayer> findPlayerByConnection(Connection c) {
        return this.connectedPlayers.stream().filter(p -> p.getCurrentConnection().equals(c)).findFirst();
    }

    public synchronized Optional<ServerPlayer> findPlayerByPublicKey(PublicKey requestPublicKey) {
        return this.connectedPlayers.stream().filter(p -> p.getPublicKey().equals(requestPublicKey)).findFirst();
    }

    public synchronized Optional<ServerPlayer> findPlayerByName(String nickname) {
        return this.connectedPlayers.stream().filter(p -> p.getNickname().equals(nickname)).findFirst();
    }

    public synchronized Optional<ServerLobby> findLobbyById(final int lobbyId) {
        return this.lobbies.stream().filter(l -> l.getId() == lobbyId).findFirst();
    }

    public void incrementStats(final RequestType type) {
        requestStatistics.get(type).incrementAndGet();
    }


    public synchronized void removeChallenge(Connection c) {
        this.activeChallenges.remove(c);
    }

    public Set<ServerLobby> listLobbies(ListLobbiesRequest.Type typeOfLobbies, ServerPlayer p) {
        switch(typeOfLobbies) {
            case MINE -> {
                return lobbies.stream()
                        .filter(l -> l.contains(p, EnumSet.of(Faction.PLAYER1, Faction.PLAYER2))
                                && !EnumSet.of(LobbyPhase.FINISHED, LobbyPhase.REJECTED).contains(l.getCurrentPhase()))
                        .collect(Collectors.toSet());
            }
            case OTHER -> {
                return lobbies.stream().
                        filter(l -> !l.contains(p, EnumSet.of(Faction.PLAYER1, Faction.PLAYER2))
                                && !EnumSet.of(LobbyPhase.FINISHED, LobbyPhase.REJECTED).contains(l.getCurrentPhase()))
                        .collect(Collectors.toSet());
            }
        }
        throw new IllegalArgumentException("Unknown lobby type");
    }

    public synchronized void removePlayer(ServerPlayer p) {
        connectedPlayers.remove(p);
    }

    public synchronized void addPlayer(ServerPlayer p) {
        connectedPlayers.add(p);
    }

    public synchronized SortedSet<String> listPlayerNames(Connection exceptConnection) {
        return connectedPlayers.stream().
                filter(p -> p.getCurrentConnection().isConnected() &&
                        !p.getCurrentConnection().equals(exceptConnection)).
                        <String>map(ServerPlayer::getNickname).collect(Collectors.toCollection(TreeSet::new));
    }
}
