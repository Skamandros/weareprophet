/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.server.data;

import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.server.app.RunningGames;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Encapsulates a {@link GameState} with additional server-only information.
 *
 * @author SharkOfMetal
 */
public class GameServerState implements ActivityBasedDisposable {

    private static final long MAX_INACTIVITY_NANOS = TimeUnit.SECONDS.toNanos(5);

    private final GameState gameState;

    private final Map<Faction, String> connectedPlayers = new TreeMap<>();

    private final RunningGames.GameStateUpdateListener updateListener;
    private long startTime = 0;

    private long lastContact = 0;


    public GameServerState(final boolean tournamentMode, final String player1Name, final String player2Name, RunningGames.GameStateUpdateListener updateListener) {
        this.updateListener = updateListener;
        this.gameState = new GameState();
        this.gameState.init(CommonValues.NUMBER_OF_ROWS, CommonValues.NUMBER_OF_COLUMNS, CommonValues.MINIONS_AT_MAP, true, tournamentMode);
        this.lastContact = System.nanoTime();
        this.connectedPlayers.put(Faction.PLAYER1, player1Name);
        this.connectedPlayers.put(Faction.PLAYER2, player2Name);
    }


    public GameState getGameState() {
        return gameState;
    }

    @Override
    public void activity(long now) {
        if(this.gameState.checkWinningCondition() == Faction.NEUTRAL) {
            this.lastContact = now;
        }
    }

    @Override
    public boolean checkForActivity(long now) {
        return now - this.lastContact < MAX_INACTIVITY_NANOS;
    }

    public void start() {
        this.startTime = System.nanoTime() + TimeUnit.SECONDS.toNanos(GameState.COUNTDOWN_MAX);
    }

    public long getStartTime() {
        return startTime;
    }

    public boolean isRunning() {
        return this.getStartTime() != 0;
    }

    public Faction checkWinningCondition() {
        return gameState.checkWinningCondition();
    }

    public Map<Faction, String> getConnectedPlayers() {
        return connectedPlayers;
    }

    public RunningGames.GameStateUpdateListener getUpdateListener() {
        return updateListener;
    }
}
