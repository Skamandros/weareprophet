/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.datamodel;

import com.carrotsearch.hppc.cursors.ObjectIntCursor;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.communication.dto.GameStateTransferObject;
import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.conversation.Conversation;
import de.ggj14.wap.common.datamodel.conversation.ConversationState;
import de.ggj14.wap.common.gamefield.GameField;
import de.ggj14.wap.common.gamefield.objects.Minion;

import java.awt.geom.Point2D;
import java.util.*;

public class GameState {
    /**
     * The maximum number of seconds on the countdown (it may not have started
     * in this case)
     */
    public static final int COUNTDOWN_MAX = 5;
    public static final float ORANGE_PROPHET_SPAWN_POINT_X = 75;
    public static final float ORANGE_PROPHET_SPAWN_POINT_Y = 75;
    public static final float BLUE_PROPHET_SPAWN_POINT_X = 1005;
    public static final float BLUE_PROPHET_SPAWN_POINT_Y = 645;
    private static final double CONVERSATION_RADIUS = 60.0;
    private static final double MINION_RANGE = 60.0;
    protected GameField gameField;
    private Map<Integer, Minion> minionMap;
    protected long time;
    private Map<Integer, Conversation> idConversationMap;
    private Map<Integer, Conversation> minionIDConversationMap;
    private transient int maxConversationId;
    private int startCountdown;
    private transient boolean isGlobalState;

    private transient boolean greyProphetMode;

    public GameState() {
        minionMap = new TreeMap<>();
        minionIDConversationMap = new HashMap<Integer, Conversation>();
        idConversationMap = new HashMap<Integer, Conversation>();
        gameField = new GameField();
        time = 0;
    }

    public void init(int rows, int columns, int minions, boolean isGlobalState, boolean tournamentMode) {
        this.init(rows, columns, minions, isGlobalState, tournamentMode, false);
    }

    public void init(int rows, int columns, int minions, boolean isGlobalState, boolean tournamentMode, boolean greyProphetMode) {
        Map<Integer, Minion> minionMap = new TreeMap<>();
        Minion p1Prophet = new Minion(0, ORANGE_PROPHET_SPAWN_POINT_X, ORANGE_PROPHET_SPAWN_POINT_Y, MinionType.PROPHET);
        p1Prophet.setFaction(Faction.PLAYER1);
        minionMap.put(0, p1Prophet);
        Minion p2Prophet = new Minion(1, BLUE_PROPHET_SPAWN_POINT_X, BLUE_PROPHET_SPAWN_POINT_Y, MinionType.PROPHET);
        p2Prophet.setFaction(Faction.PLAYER2);
        minionMap.put(1, p2Prophet);

        if(this.greyProphetMode) {
            this.addMinion(MinionType.PROPHET, Faction.NEUTRAL, new Point2D.Float(540, 360));
        }

        if(tournamentMode) {
            for(int i = 1; i < minions / 2; i++) {
                // start at 1 because prophets are minions as well
                final Minion m = new Minion();
                do {
                    m.fillMinionRandom(minionMap.size(), rows, columns, true);
                } while(p1Prophet.distanceToMinion(m) < 360 || p2Prophet.distanceToMinion(m) < 360);

                minionMap.put(m.getId(), m);
                final Minion mirroredMinion = Minion.mirrorMinion(m, rows, columns);
                mirroredMinion.setId(minionMap.size());
                minionMap.put(mirroredMinion.getId(), mirroredMinion);

            }
        } else {
            // start at 2 because prophets are minions as well
            for(int i = 2; i < minions; i++) {
                Minion m = new Minion();
                do {
                    m.fillMinionRandom(minionMap.size(), rows, columns, false);
                } while(p1Prophet.distanceToMinion(m) < 360 || p2Prophet.distanceToMinion(m) < 360);
                minionMap.put(m.getId(), m);
            }
        }
        this.init(rows, columns, minionMap, isGlobalState, greyProphetMode);
    }

    protected void init(int rows, int columns, Map<Integer, Minion> minionMap, boolean isGlobalState, boolean greyProphetMode) {
        this.isGlobalState = isGlobalState;
        gameField.init(rows, columns);
        this.greyProphetMode = greyProphetMode;
        this.minionMap = minionMap;
        this.startCountdown = COUNTDOWN_MAX;
        this.maxConversationId = 0;
        this.idConversationMap.clear();
        this.minionIDConversationMap.clear();
    }

    protected Minion addMinion(MinionType type, Faction faction, Point2D.Float position) {
        Minion newMinion = new Minion(getNumberOfMinions(), position.x, position.y, type);
        newMinion.setFaction(faction);
        minionMap.put(newMinion.getId(), newMinion);
        return newMinion;
    }

    public void update(int timeDelta) {
        if(Faction.NEUTRAL.equals(checkWinningCondition())) {
            updateMinions(timeDelta);
            updateConversations(timeDelta);
            gameField.update(timeDelta);
            this.time += timeDelta;
        }
    }

    protected void updateConversations(int timeDelta) {
        // update conversations
        if(isGlobalState) {
            Iterator<Conversation> it = idConversationMap.values().iterator();
            while(it.hasNext()) {
                Conversation conv = it.next();
                ConversationState cs = conv.update(timeDelta);
                if(cs != ConversationState.BATTLING) {
                    finalizeConversation(conv, cs.getCorrespondingFaction());
                    it.remove();
                }
            }
        }
    }

    protected void updateMinions(int timeDelta) {
        for(Minion minion : minionMap.values()) {
            minion.update(timeDelta, gameField.getColumns(), gameField.getRows(), isGlobalState);
            int minionRow = (int) (minion.getY() / CommonValues.HEIGHT_OF_MAP_ELEMENT);
            int minionCol = (int) (minion.getX() / CommonValues.WIDTH_OF_MAP_ELEMENT);
            gameField.getGameFieldElement(minionRow, minionCol).incrementFactionMapElement(minion);

            checkForConversationsOf(minion);
        }
    }

    private void checkForConversationsOf(Minion minion) {
        // check for colliding conversations or minions if they are not grey and not invulnerable
        // if the minion is part of a conversation or invulnerable, nothing has to be done
        if(isGlobalState && !minionIDConversationMap.containsKey(minion.getId()) && !minion.isInvulnerable(this.time)) {
            for(Conversation conv : idConversationMap.values()) {
                if(conv.getDistanceToMinion(minion) > CONVERSATION_RADIUS) continue;

                if(conv.addParticipant(minion)) {
                    minionIDConversationMap.put(minion.getId(), conv);
                    // a conversation was found so we could leave
                    return;
                }
            }

            // after outerminion was updated we want to check its position in regard to other minions
            for(Minion innerMinion : minionMap.values()) {
                // search for nearby minions to talk to
                if(minion.getId() != innerMinion.getId()
                        && !innerMinion.isInvulnerable(this.time)
                        // &&
                        // !innerMinion.getMinionType().equals(MinionType.PROPHET)
                        && minion.getFaction() != innerMinion.getFaction()
                        && minion.distanceToMinion(innerMinion) <= MINION_RANGE) {

                    // check whether the colliding minion is part of a conversation
                    if(minionIDConversationMap.containsKey(innerMinion.getId())) {
                        return;
                    }

                    // check for prophet stuff
                    // if I'm a prophet
                    if(minion.getMinionType().isProphet()
                            // and my enemy is a prophet
                            && (innerMinion.getMinionType().isProphet())) {
                        continue;
                    }

                    // no conversation yet and close enough
                    Conversation newConv = new Conversation(maxConversationId++);
                    idConversationMap.put(newConv.getId(), newConv);

                    if(newConv.addParticipant(innerMinion)) {
                        minionIDConversationMap.put(innerMinion.getId(), newConv);
                    }
                    if(newConv.addParticipant(minion)) {
                        minionIDConversationMap.put(minion.getId(), newConv);
                    }
                    // a conversation was found so we could leave
                    return;
                }
            }
        }
    }

    private void finalizeConversation(Conversation conv, Faction winningFaction) {
        final long endpointOfInvulnerability = time + CommonValues.TIME_OF_INVULNERABILITY;
        for(Minion min : conv.getParticipants()) {
            min.setInvulnerableEndpoint(endpointOfInvulnerability);
            min.moveAgain();
            minionIDConversationMap.remove(min.getId());
            if(min.getFaction() != winningFaction) {
                min.setFaction(winningFaction);
            }
        }
    }

    public Faction checkWinningCondition() {
        Faction f = Faction.NEUTRAL;
        for(ObjectIntCursor<Faction> factionCount : gameField.getGameFieldFactionCountMap()) {
            if(factionCount.value >= CommonValues.getNumberOfFieldsToWin(
                    gameField.getNumberOfLockedGameFieldElements())) {
                f = factionCount.key;
            }
        }

        return f;
    }

    public GameStateTransferObject getGameStateTransferObject(int gameId) {
        return new GameStateTransferObject(gameId, time, startCountdown, new ArrayList<Minion>(minionMap.values()),
                new ArrayList<Conversation>(
                        idConversationMap.values()), gameField.getGameFieldTransferObject());
    }

    public void doMoveAction(MoveAction ma) {
        setMinionTargetPosition(ma.objectID(), ma.targetX(), ma.targetY());
    }

    public static GameState fromGameStateTransferObject(GameStateTransferObject gsto) {
        GameState gs = new GameState();

        gs.setGameField(GameField.fromGameFieldTransferObject(gsto.gameFieldDTO()));

        // build up the minion map
        Map<Integer, Minion> minionMap = new HashMap<Integer, Minion>();
        for(Minion m : gsto.minionList()) {
            minionMap.put(m.getId(), m);
        }
        gs.setMinions(minionMap);

        // build up the conversation maps
        Map<Integer, Conversation> idConversationMap = new HashMap<Integer, Conversation>();
        Map<Integer, Conversation> minionConvesationMap = new HashMap<Integer, Conversation>();
        if(gsto.conversationList() != null) {
            for(Conversation conv : gsto.conversationList()) {
                idConversationMap.put(conv.getId(), conv);
                for(Minion min : conv.getParticipants()) {
                    minionConvesationMap.put(min.getId(), conv);
                }
            }
        }
        gs.setIdConversationMap(idConversationMap);
        gs.setMinionIDConversationMap(minionConvesationMap);

        gs.setStartCountdown(gsto.startCountdown());
        gs.setTime(gsto.time());

        // rebuild the map state
        gs.update(0);

        return gs;
    }

    public long getTime() {
        return time;
    }

    private void setTime(long time) {
        this.time = time;
    }

    private void setMinions(Map<Integer, Minion> minions) {
        this.minionMap = minions;
    }

    public void setMinionTargetPosition(int minionID, int targetX, int targetY) {
        if(!(targetX < 0 || targetY < 0 || targetX >= (CommonValues.WIDTH_OF_MAP_ELEMENT * gameField.getColumns()) || targetY >= (CommonValues.HEIGHT_OF_MAP_ELEMENT * gameField
                .getRows()))) {
            Minion m = minionMap.get(minionID);
            m.setTargetPosX(targetX);
            m.setTargetPosY(targetY);
        }
    }

    public GameField getGameField() {
        return gameField;
    }

    private void setGameField(GameField gameField) {
        this.gameField = gameField;
    }

    public boolean isGreyProphetModeActive() {
        return greyProphetMode;
    }

    /**
     * @return The amount of seconds left until the game starts - if this value
     * is {@value #COUNTDOWN_MAX}, the countdown may not have started
     * yet
     */
    public int getStartCountdown() {
        return startCountdown;
    }

    public void setStartCountdown(int startCountdown) {
        this.startCountdown = startCountdown;
    }

    public void print() {
        System.out.println("Objects Created	: " + getNumberOfMinions() + maxConversationId);
        System.out.println("Time			: " + time);
        gameField.print();

        for(Minion m : minionMap.values()) {
            m.print();
        }
    }

    public int getNumberOfMinions() {
        return minionMap.size();
    }

    public Map<Integer, Conversation> getIdConversationMap() {
        return Collections.unmodifiableMap(idConversationMap);
    }

    private void setIdConversationMap(Map<Integer, Conversation> idConversationMap) {
        this.idConversationMap = idConversationMap;
        this.maxConversationId = idConversationMap.keySet().stream().max(Comparator.naturalOrder()).orElse(0);
    }

    public Map<Integer, Conversation> getMinionIDConversationMap() {
        return Collections.unmodifiableMap(minionIDConversationMap);
    }

    private void setMinionIDConversationMap(Map<Integer, Conversation> minionIDConversationMap) {
        this.minionIDConversationMap = minionIDConversationMap;
    }

    public Map<Integer, Minion> getMinionMap() {
        return Collections.unmodifiableMap(minionMap);
    }

    public boolean isGlobalState() {
        return isGlobalState;
    }

    public static int getCountdownMax() {
        return COUNTDOWN_MAX;
    }

    @Override
    public String toString() {
        return "GameState{" +
                "gameField=" + gameField +
                ", minionMap=" + minionMap +
                ", time=" + time +
                ", idConversationMap=" + idConversationMap +
                ", minionIDConversationMap=" + minionIDConversationMap +
                ", maxConversationId=" + maxConversationId +
                ", startCountdown=" + startCountdown +
                ", isGlobalState=" + isGlobalState +
                ", greyProphetMode=" + greyProphetMode +
                '}';
    }
}