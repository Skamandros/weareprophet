/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.datamodel;

public enum DominationStateRange {
    PLAYER1_MAX(20.0f),
    GREY_UPPER(5.0f),
    GREY_LOWER(-5.0f),
    PLAYER2_MAX(-20.0f),
    ;

    public final float border;

    private DominationStateRange(float border) {
        this.border = border;
    }

    public static float normalize(float dominationState) {
        if(dominationState > PLAYER1_MAX.border) {
            return PLAYER1_MAX.border;
        } else if(dominationState < PLAYER2_MAX.border) {
            return PLAYER2_MAX.border;
        } else {
            return dominationState;
        }
    }

    public static Faction getFaction(float dominationState) {

        if(dominationState > GREY_UPPER.border) {
            return Faction.PLAYER1;
        }
        if(dominationState < GREY_LOWER.border) {
            return Faction.PLAYER2;
        }
        return Faction.NEUTRAL;
    }
}
