/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.gamefield.objects;

import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.MinionType;

import java.util.Random;

public class Minion extends GameFieldObject {
    private static final int MIN_MOVE_DISTANCE = 100;
    private static final int MIN_INITIAL_MOVE_TOURNAMENT = 400;
    private static final Random RANDOM_SOURCE = new Random();
    private MinionType minionType;
    private int targetPosX;
    private int targetPosY;
    private boolean move;
    private long invulnerableEndpoint;
    private float speedModifier = 0.5f;

    public Minion() {
    }

    public Minion(int id, float x, float y, MinionType type) {
        super(id, x, y, Faction.NEUTRAL);
        this.targetPosX = (int) x;
        this.targetPosY = (int) y;
        this.minionType = type;
        this.move = true;
        this.invulnerableEndpoint = 0;
    }

    /**
     * Returns a copy of the provided minion, copied along the diagonal between both prophets.
     *
     * @param toMirror The minion to mirror.
     * @return The new mirror copy.
     */
    public static Minion mirrorMinion(final Minion toMirror, final int rows, final int columns) {
        final Minion result = new Minion();
        result.setX(columns * CommonValues.WIDTH_OF_MAP_ELEMENT - toMirror.getX());
        result.setY(rows * CommonValues.WIDTH_OF_MAP_ELEMENT - toMirror.getY());
        result.setTargetPosX(columns * CommonValues.WIDTH_OF_MAP_ELEMENT - toMirror.getTargetPosX());
        result.setTargetPosY(rows * CommonValues.WIDTH_OF_MAP_ELEMENT - toMirror.getTargetPosY());
        result.setMinionType(toMirror.getMinionType());
        result.setFaction(toMirror.getFaction());
        result.moveAgain();
        return result;
    }

    public void fillMinionRandom(int id, int rows, int columns, boolean tournamentMode) {
        this.id = id;
        faction = Faction.NEUTRAL;

        final int maxX = columns * CommonValues.WIDTH_OF_MAP_ELEMENT;
        final int maxY = rows * CommonValues.HEIGHT_OF_MAP_ELEMENT;
        do {
            this.x = RANDOM_SOURCE.nextInt(maxX);
            this.y = RANDOM_SOURCE.nextInt(maxY);
            // In tournament mode, we only create minions in the upper left half of the field and mirror them
        } while(tournamentMode && this.x + this.y >= (maxX + maxY) / 2f);

        // normalize Position and allow him to move
        standStill();
        moveAgain();

        if(tournamentMode) {
            // In tournament mode, the first move action will be mirrored
            moveToRandomPosition(columns, rows, MIN_INITIAL_MOVE_TOURNAMENT);
        }

        do {
            minionType = MinionType.values()[RANDOM_SOURCE.nextInt(MinionType.values().length)];
        } while(minionType.isProphet());
    }

    /**
     * @param timeDelta is the time since last modification
     */
    public void update(int timeDelta, int columns, int rows, boolean isGlobalState) {
        if(move) {
            if(faction == Faction.NEUTRAL && isGlobalState) {
                // grey minion is at target. get a new one. no one likes to
                // stand still ;)
                if((Math.abs(x - (float) targetPosX) < 2.0f && Math.abs(y - (float) targetPosY) < 2.0f)) {
                    moveToRandomPosition(columns, rows, MIN_MOVE_DISTANCE);
                }
            }

            double l = Math.sqrt(Math.pow(targetPosX - x, 2) + Math.pow(targetPosY - y, 2));

            if(l > 5.0) {
                double dx = (targetPosX - x) / l;
                double dy = (targetPosY - y) / l;

                x = (float) (x + (timeDelta / 1000.0f * (minionType.getSpeed() * speedModifier) * dx));
                y = (float) (y + (timeDelta / 1000.0f * (minionType.getSpeed() * speedModifier) * dy));
            } else {
                standStill();
            }
        }
    }

    private void moveToRandomPosition(int columns, int rows, final int minDistance) {
        do {
            targetPosX = RANDOM_SOURCE.nextInt(columns * CommonValues.WIDTH_OF_MAP_ELEMENT);
            targetPosY = RANDOM_SOURCE.nextInt(rows * CommonValues.HEIGHT_OF_MAP_ELEMENT);
        } while(distanceToPoint(targetPosX, targetPosY) < minDistance);
    }

    public double distanceToMinion(Minion otherMinion) {
        return distanceToPoint(otherMinion.getX(), otherMinion.getY());
    }

    public double distanceToPoint(final float x, final float y) {
        return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
    }

    public void print() {
        System.out.println("================================================");
        System.out.println("Minion with id			: " + id);
        System.out.println("	Position			: x=" + x + " y=" + y);
        System.out.println(" 	Target Position		: x=" + targetPosX + " y=" + targetPosY);
        System.out.println("	Faction 			: " + faction.name());
        System.out.println("	Type				: " + minionType.name());
        System.out.println("	Move?				: " + move);
        System.out.println("	Invulnerable till	: " + invulnerableEndpoint);
    }

    public MinionType getMinionType() {
        return minionType;
    }

    public void setMinionType(MinionType minionType) {
        this.minionType = minionType;
    }

    public int getTargetPosX() {
        return targetPosX;
    }

    public void setTargetPosX(int targetPosX) {
        if(targetPosX < 0) {
            targetPosX = 0;
        } else if(targetPosX > (CommonValues.WIDTH_OF_MAP_ELEMENT * CommonValues.NUMBER_OF_COLUMNS)) {
            targetPosX = CommonValues.WIDTH_OF_MAP_ELEMENT * CommonValues.NUMBER_OF_COLUMNS;
        }
        this.targetPosX = targetPosX;
    }

    public int getTargetPosY() {
        return targetPosY;
    }

    public void setTargetPosY(int targetPosY) {
        if(targetPosY < 0) {
            targetPosY = 0;
        } else if(targetPosY > CommonValues.HEIGHT_OF_MAP_ELEMENT * CommonValues.NUMBER_OF_ROWS) {
            targetPosY = CommonValues.HEIGHT_OF_MAP_ELEMENT * CommonValues.NUMBER_OF_ROWS;
        }
        this.targetPosY = targetPosY;
    }

    public void standStill() {
        this.targetPosX = (int) this.x;
        this.targetPosY = (int) this.y;
    }

    public void dontMove() {
        move = false;
    }

    public void moveAgain() {
        move = true;
    }

    public boolean canMove() {
        return move;
    }

    public void setInvulnerableEndpoint(long invulnerableEndpoint) {
        this.invulnerableEndpoint = invulnerableEndpoint;
    }

    public long getInvulnerableEndpoint() {
        return invulnerableEndpoint;
    }

    public boolean isInvulnerable(long atTime) {
        return invulnerableEndpoint > atTime;
    }

    @Override
    public void setFaction(Faction faction) {
        super.setFaction(faction);
        if(faction == Faction.NEUTRAL) {
            this.speedModifier = 0.5f;
        } else {
            this.speedModifier = 1.0f;
        }
    }

    public void speedBuff() {
        this.speedModifier *= 1.6f;
    }
}
