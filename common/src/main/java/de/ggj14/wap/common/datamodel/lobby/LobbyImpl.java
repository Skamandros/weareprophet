/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.common.datamodel.lobby;

import com.google.common.collect.ComparisonChain;
import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.communication.dto.LobbySettings;
import de.ggj14.wap.common.communication.dto.LobbyStateTransferObject;

import java.util.Optional;
import java.util.SortedSet;

public final class LobbyImpl implements Lobby {
    private LobbyStateTransferObject state;

    LobbyImpl(LobbyStateTransferObject state) {
        this.state = state;
    }

    public LobbyImpl() {
        this.state = new LobbyStateTransferObject();
    }

    @Override
    public Optional<String> getPlayer1() {
        return Optional.ofNullable(state.getP1Name());
    }

    @Override
    public Optional<String> getPlayer2() {
        return Optional.ofNullable(state.getP2Name());
    }

    @Override
    public boolean isProphet(String playerName) {
        return getPlayer1().map(playerName::equals).orElse(false) ||
                getPlayer2().map(playerName::equals).orElse(false);
    }

    @Override
    public LobbyStateTransferObject toTransferObject() {
        return state;
    }

    @Override
    public int getScoreOfPlayer1() {
        return state.getScoreP1();
    }

    @Override
    public int getScoreOfPlayer2() {
        return state.getScoreP2();
    }

    @Override
    public boolean isReady() {
        return state.isPlayer2ready();
    }

    @Override
    public LobbyPhase getCurrentPhase() {
        return state.getCurrentPhase();
    }

    @Override
    public LobbySettings getSettings() {
        return state.getSettings();
    }

    @Override
    public SortedSet<String> getObservers() {
        return state.getObservers();
    }


    @Override
    public int getId() {
        return state.getLobbyId();
    }

    @Override
    public GameInformation getGameInformation() {
        if(state.getGameInformation().getGameId() == GameInformation.NO_GAME_ID) {
            throw new IllegalStateException("No game in progress");
        }
        return state.getGameInformation();
    }

    @Override
    public boolean isGameRunning() {
        return state.getGameInformation().getGameId() != GameInformation.NO_GAME_ID;
    }

    @Override
    public void setState(LobbyStateTransferObject lobbyState) {
        this.state = lobbyState;
    }

    @Override
    public int compareTo(Lobby o) {
        return ComparisonChain.start().compare(this.getId(), o.getId()).result();
    }

    @Override
    public String toString() {
        if(getPlayer2().isEmpty()) {
            return getPlayer1().orElseGet(() -> "[Empty lobby]");
        } else {
            return getPlayer1().orElseGet(() -> "[Empty]") + " - " + getPlayer2().get();
        }
    }

}
