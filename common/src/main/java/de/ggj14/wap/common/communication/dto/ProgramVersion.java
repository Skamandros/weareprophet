/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.common.communication.dto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents the version of the currently ran application.
 *
 * @param generation Indicates a rewrite of major parts, e.g. a new engine.
 * @param major      Indicates an incompatible client-server-protocol change.
 * @param minor      Indicates an extension without an incompatible change.
 * @param patch      Indicates a bugfix without an API extension / change.
 */
public record ProgramVersion(int generation, int major, int minor, int patch) {
    private static final Pattern VERSION_PATTERN = Pattern.compile("([0-9]+)[.]([0-9]+)[.]([0-9]+)[.]([0-9]+)");

    /**
     * Creates a {@link ProgramVersion} instance from a plain version string.
     *
     * @param versionString The version String to parse.
     * @return The program version.
     * @throws IllegalArgumentException If the version string doesn't match the expected pattern.
     */
    public static ProgramVersion fromString(final String versionString) {
        Matcher matcher = VERSION_PATTERN.matcher(versionString);
        if(matcher.matches()) {
            return new ProgramVersion(
                    Integer.parseInt(matcher.group(1)),
                    Integer.parseInt(matcher.group(2)),
                    Integer.parseInt(matcher.group(3)),
                    Integer.parseInt(matcher.group(4)));
        } else {
            throw new IllegalArgumentException("Version string could not be parsed: " + versionString);
        }
    }

    public boolean isCompatibleTo(ProgramVersion other) {
        return this.generation == other.generation() && this.major == other.major();
    }

    @Override
    public String toString() {
        return generation + "." + major + "." + minor + "." + patch;
    }
}
