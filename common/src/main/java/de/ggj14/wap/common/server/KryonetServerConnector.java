/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.server;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.serialization.KryoSerialization;
import com.esotericsoftware.kryonet.serialization.Serialization;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.communication.KryoConverter;
import de.ggj14.wap.common.communication.dto.*;
import de.ggj14.wap.common.communication.message.*;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.lobby.Lobby;
import de.ggj14.wap.common.datamodel.lobby.LobbyImpl;
import de.ggj14.wap.common.datamodel.lobby.LobbyPhase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.security.*;
import java.util.List;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.*;

/**
 * Connects to a prophet server using Kryonet.
 *
 * @author SharkOfMetal
 */
public class KryonetServerConnector implements ProphetConnector {

    private static final Logger LOG = LogManager.getLogger(KryonetServerConnector.class);
    private static final int INITIAL_UPDATE_TIME = -1;

    private static final int REQUEST_TIMEOUT_SECONDS = 3;

    private final Client client;
    private final String server;
    private final int tcpPort;
    private final int udpPort;
    private final ExecutorService threadPool;
    private final ProgramVersion clientVersion;
    private final KeyPair clientKeys;

    private final Queue<GameStateUpdateListener> gameStateUpdateListeners = new ConcurrentLinkedQueue<>();
    private final Queue<ConnectionStateListener> connectionStateListeners = new ConcurrentLinkedQueue<>();

    private volatile GameState currentGameState;
    private volatile int currentGameId = GameInformation.NO_GAME_ID;
    private final Lobby currentLobbyState = new LobbyImpl();
    private volatile long lastUpdateTime = INITIAL_UPDATE_TIME;
    private volatile int lobbyId = Lobby.UNSET_LOBBY_ID;
    private volatile CountDownLatch gameStateReceived = new CountDownLatch(1);

    private volatile CountDownLatch lobbyStateReceived = new CountDownLatch(1);

    private final ConcurrentMap<Class<?>, ResponseTypeHandler> responseTypeHandlers = new ConcurrentHashMap<>();

    /**
     * @param server                  The server's host name or IP.
     * @param tcpPort                 The TCP port where the server listens.
     * @param udpPort                 The UDP port where the server listens.
     * @param threadPool              A thread pool for asynchronous operations.
     * @param clientVersion           The current program version.
     * @param clientKeys              This client's key pair.
     * @param lobbyInvitationListener A listener for lobby invitations.
     */
    public KryonetServerConnector(String server, int tcpPort, int udpPort, ExecutorService threadPool, ProgramVersion clientVersion, KeyPair clientKeys, LobbyInvitationListener lobbyInvitationListener) {
        this(server, tcpPort, udpPort, threadPool, clientVersion, clientKeys, lobbyInvitationListener, new KryoSerialization());
    }

    /**
     * @param server                  The server's host name or IP.
     * @param tcpPort                 The TCP port where the server listens.
     * @param udpPort                 The UDP port where the server listens.
     * @param threadPool              A thread pool for asynchronous operations.
     * @param clientVersion           The current program version.
     * @param clientKeys              This client's key pair.
     * @param lobbyInvitationListener A listener for lobby invitations.
     * @param serialization           The serialization instance to use. In case of multiple clients/servers this should always be the same.
     */
    public KryonetServerConnector(String server, int tcpPort, int udpPort, ExecutorService threadPool, ProgramVersion clientVersion, KeyPair clientKeys, LobbyInvitationListener lobbyInvitationListener, Serialization serialization) {
        this.server = server;
        this.tcpPort = tcpPort;
        this.udpPort = udpPort;
        this.threadPool = threadPool;
        this.clientVersion = clientVersion;
        this.clientKeys = clientKeys;
        client = new Client(1 << 15, 1 << 13, serialization);
        KryoConverter.registerKryoClasses(client.getKryo());
        client.addListener(new Listener.ThreadedListener(new MessageListener(lobbyInvitationListener), threadPool));
        client.addListener(new Listener() {
            @Override
            public void connected(Connection connection) {
                for(final ConnectionStateListener l : connectionStateListeners) {
                    l.connected();
                }
            }

            @Override
            public void disconnected(Connection connection) {
                for(final ConnectionStateListener l : connectionStateListeners) {
                    l.disconnected();
                }
            }
        });
    }

    /**
     * @return Whether this connector is currently associated with a game.
     */
    private synchronized boolean ingame() {
        return currentGameId != GameInformation.NO_GAME_ID;
    }

    @Override
    public void connect(final String playerName) throws ConnectionFailedException {
        LOG.info("Connecting to server {} on TCP port {} and UDP port {}...", server, tcpPort, udpPort);
        try {
            client.start();
            client.connect(5000, server, tcpPort, udpPort);
            client.setKeepAliveUDP(500);
            final ProgramVersion serverVersion = singleResponseRequest(new ServerVersionRequest(),
                    ServerVersionResponse.class).version();
            if(serverVersion == null || !serverVersion.isCompatibleTo(clientVersion)) {
                disconnect();
                throw new VersionMismatchException(
                        "Client version " + clientVersion + " and server version " + serverVersion + " are incompatible");
            } else {
                ClientChallengeResponse challengeRequest = singleResponseRequest(
                        new ClientChallengeRequest(clientKeys.getPublic(), playerName), ClientChallengeResponse.class);
                LOG.debug("Received client challenge {}", challengeRequest);
                if(!challengeRequest.challengeAccepted()) {
                    throw new ConnectionFailedException("Connection request rejected by the server", false);
                }
                Signature signer = Signature.getInstance(CommonValues.SIGNATURE_ALGORITHM);
                signer.initSign(clientKeys.getPrivate());
                signer.update(challengeRequest.challenge());
                byte[] signature = signer.sign();
                ClientAuthenticationResponse authenticationResponse = singleResponseRequest(
                        new ClientAuthenticationRequest(signature), ClientAuthenticationResponse.class);
                LOG.debug("Received authentication response {}", authenticationResponse);
                if(!authenticationResponse.authenticationSuccessful()) {
                    throw new ConnectionFailedException("Client signature rejected by the server", false);
                }
            }
        } catch(IOException e) {
            throw new ConnectionFailedException("Connection failed", e, true);
        } catch(NoSuchAlgorithmException | SignatureException | InvalidKeyException e) {
            throw new ConnectionFailedException("Connection failed", e, false);
        }
    }

    @Override
    public void disconnect() {
        LOG.info("Disconnecting from server {} on TCP port {} and UDP port {}...", server, tcpPort, udpPort);
        client.stop();
    }

    @Override
    public void leaveLobby() {
        LOG.info("Leaving lobby...");
        this.resetGameState();
        client.sendTCP(new LeaveRequest(this.lobbyId));
        synchronized(this) {
            this.lobbyId = Lobby.UNSET_LOBBY_ID;
        }
    }


    @Override
    public void createLobby(String otherPlayer) throws JoinFailedException {
        final CreateLobbyRequest request = new CreateLobbyRequest(otherPlayer);
        synchronized(this) {
            try {
                if(this.lobbyStateReceived.getCount() == 0) {
                    this.lobbyStateReceived = new CountDownLatch(1);
                }
                CreateLobbyResponse response = singleResponseRequest(request, CreateLobbyResponse.class);
                if(response.lobbyId() != Lobby.UNSET_LOBBY_ID) {
                    this.lobbyId = response.lobbyId();
                } else {
                    throw new JoinFailedException("Unable to create the lobby");
                }
            } catch(IOException e) {
                throw new JoinFailedException("Unable to create the lobby", e);
            }
        }
        LOG.info("Created lobby {}", this.lobbyId);
    }

    @Override
    public Faction joinLobby(int lobbyId) throws JoinFailedException {
        final JoinRequest request = new JoinRequest(lobbyId);
        synchronized(this) {
            try {
                if(this.lobbyStateReceived.getCount() == 0) {
                    this.lobbyStateReceived = new CountDownLatch(1);
                }
                JoinResponse response = singleResponseRequest(request, JoinResponse.class);
                if(response.lobbyId() != Lobby.UNSET_LOBBY_ID) {
                    this.lobbyId = response.lobbyId();
                    LOG.info("Joined lobby {}", this.lobbyId);
                    return response.assignedFaction();
                } else {
                    throw new JoinFailedException("Unable to join lobby " + lobbyId);
                }
            } catch(IOException e) {
                throw new JoinFailedException("Unable to join lobby " + lobbyId, e);
            }
        }
    }

    @Override
    public void observeLobby(int lobbyId) {
        synchronized(this) {
            if(this.lobbyStateReceived.getCount() == 0) {
                this.lobbyStateReceived = new CountDownLatch(1);
            }
            this.lobbyId = lobbyId;
        }
        client.sendTCP(new ObserveRequest(lobbyId));
    }

    @Override
    public void lobbyChangeSettings(LobbySettings newSettings) {
        synchronized(this) {
            LobbyStateTransferObject lsto = currentLobbyState.toTransferObject();
            lsto.setSettings(newSettings);
            currentLobbyState.setState(lsto);
        }
        threadPool.submit(() -> client.sendTCP(new LobbyChangeSettingsMessage(this.lobbyId, newSettings)));
    }

    @Override
    public void lobbyChangeReadyState(boolean ready) {
        synchronized(this) {
            LobbyStateTransferObject lsto = currentLobbyState.toTransferObject();
            lsto.setPlayer2ready(ready);
            currentLobbyState.setState(lsto);
        }
        threadPool.submit(() -> client.sendTCP(new LobbyReadyMessage(this.lobbyId, ready)));
    }

    @Override
    public void lobbyStartGame() {
        try {
            if(getCurrentLobby().isReady()) {
                threadPool.submit(() -> client.sendTCP(new LobbyAdvancePhaseRequest(lobbyId)));
            }
        } catch(InterruptedException e) {
            LOG.error("Lobby state unavailable", e);
        }
    }

    @Override
    public void declineInvitation(int lobbyId) {
        threadPool.submit(() -> client.sendTCP(new ChallengeDeclinedMessage(lobbyId)));
        LOG.info("Sent decline message for lobby lobby {}", this.lobbyId);
    }

    @Override
    public Lobby getCurrentLobby() throws InterruptedException {
        if(this.lobbyId == Lobby.UNSET_LOBBY_ID) {
            throw new IllegalStateException("Not in a lobby, join one first!");
        }
        if(!lobbyStateReceived.await(1, TimeUnit.SECONDS)) {
            throw new InterruptedException("Waiting for the lobby state latch took too long");
        }
        synchronized(this) {
            return currentLobbyState;
        }
    }


    @Override
    public GameState getCurrentGameState() throws InterruptedException {
        synchronized(this) {
            if(!ingame()) {
                throw new IllegalStateException("Not in a game, join one first!");
            }
        }
        if(!gameStateReceived.await(2, TimeUnit.SECONDS)) {
            throw new IllegalStateException("Waiting for the game state latch took too long");
        }
        synchronized(this) {
            return currentGameState;
        }
    }


    @Override
    public synchronized GameInformation getGameInformation() throws IOException {
        if(ingame()) {
            return currentLobbyState.getGameInformation();
        } else {
            throw new IllegalStateException("Not in a game");
        }
    }

    public void handleMoveAction(MoveAction action) {
        synchronized(this) {
            if(!ingame()) {
                LOG.warn("Move action discarded because this connector is not in a game");
                return;
            }
        }
        LOG.debug("Send move action: {}", action);
        threadPool.submit(() -> client.sendTCP(action));
    }


    @Override
    public SortedSet<Lobby> listMyLobbies() throws IOException {
        return listLobbiesOfType(ListLobbiesRequest.Type.MINE);
    }

    @Override
    public SortedSet<Lobby> listOtherLobbies() throws IOException {
        return listLobbiesOfType(ListLobbiesRequest.Type.OTHER);
    }

    private SortedSet<Lobby> listLobbiesOfType(ListLobbiesRequest.Type lobbyType) throws IOException {
        final ListLobbiesRequest request = new ListLobbiesRequest(lobbyType);
        List<LobbyStateTransferObject> lobbyDtos = singleResponseRequest(request, LobbyListResponse.class).lobbies();
        final SortedSet<Lobby> result = new TreeSet<>();
        for(final LobbyStateTransferObject lobbyDto : lobbyDtos) {
            result.add(Lobby.fromTransferObject(lobbyDto));
        }
        return result;
    }


    @Override
    public SortedSet<String> getPlayersOnServer() throws IOException {
        final ListPlayersRequest request = new ListPlayersRequest();
        return singleResponseRequest(request, ListPlayersResponse.class).playerNames();
    }

    @Override
    public boolean isConnected() {
        return client.isConnected();
    }

    @Override
    public void addListener(GameStateUpdateListener toAdd) {
        this.gameStateUpdateListeners.add(toAdd);
    }

    @Override
    public void removeListener(GameStateUpdateListener toRemove) {
        this.gameStateUpdateListeners.remove(toRemove);
    }

    @Override
    public void addListener(ConnectionStateListener toAdd) {
        this.connectionStateListeners.add(toAdd);
    }

    @Override
    public void removeListener(ConnectionStateListener toRemove) {
        this.connectionStateListeners.remove(toRemove);
    }

    @Override
    public int getPing() {
        client.updateReturnTripTime();
        return client.getReturnTripTime();
    }

    private synchronized void resetGameState() {
        if(gameStateReceived.getCount() == 0) {
            LOG.trace("Replacing game state latch because it has been fired before");
            gameStateReceived = new CountDownLatch(1);
        } else {
            LOG.trace("Keeping game state latch because it has not been fired yet");
        }
        currentGameState = null;
        lastUpdateTime = INITIAL_UPDATE_TIME;
        currentGameId = GameInformation.NO_GAME_ID;
    }

    private <T> T singleResponseRequest(final Object request, final Class<T> expectedResponseType) throws IOException {
        final SingleReponseHandler<T> handler = new SingleReponseHandler<>(expectedResponseType);
        responseTypeHandlers.put(expectedResponseType, handler);
        client.sendTCP(request);
        T result = handler.getResult();
        LOG.trace("Request: {}; Result: {}", request, result);
        return result;
    }


    private interface ResponseTypeHandler {
        void responseReceived(Object response);
    }

    private static class SingleReponseHandler<T> implements ResponseTypeHandler {
        private final Class<T> clazz;
        private final CountDownLatch latch = new CountDownLatch(1);
        private T result = null;

        public SingleReponseHandler(Class<T> clazz) {
            this.clazz = clazz;
        }

        @Override
        public void responseReceived(Object response) {
            result = clazz.cast(response);
            latch.countDown();
        }

        public T getResult() throws IOException {
            try {
                if(!latch.await(REQUEST_TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
                    LOG.warn("Request for response of type {} has timed out", clazz.getSimpleName());
                }
                return result;
            } catch(InterruptedException e) {
                throw new IOException("Response handler has been interrupted", e);
            }
        }
    }

    private final class MessageListener implements Listener {
        private final LobbyInvitationListener lobbyInvitationListener;

        public MessageListener(LobbyInvitationListener lobbyInvitationListener) {
            this.lobbyInvitationListener = lobbyInvitationListener;
        }

        @Override
        public void received(Connection connection, Object o) {
            if(o instanceof final GameStateTransferObject gsto) {
                LOG.trace("Received game state update");
                if(currentGameId == gsto.gameId() && lastUpdateTime <= gsto.time()) {
                    GameState newGameState = GameState.fromGameStateTransferObject(gsto);
                    LOG.trace("Received game state is newer than the currently known");
                    final boolean firstUpdate = lastUpdateTime == INITIAL_UPDATE_TIME;
                    lastUpdateTime = newGameState.getTime();
                    synchronized(KryonetServerConnector.this) {
                        KryonetServerConnector.this.currentGameState = newGameState;
                    }
                    gameStateReceived.countDown();
                    for(GameStateUpdateListener l : gameStateUpdateListeners) {
                        l.update(currentGameState, firstUpdate);
                    }
                    final Faction winningFaction = newGameState.checkWinningCondition();
                    if(winningFaction != Faction.NEUTRAL && lobbyId != Lobby.UNSET_LOBBY_ID) {
                        LOG.info("Game has been won by {}", winningFaction);
                        resetGameState();
                    }
                }
            } else if(o instanceof final LobbyStateTransferObject lobbyState) {
                LOG.debug("Received lobby state update");
                synchronized(KryonetServerConnector.this) {
                    if(lobbyState.getLobbyId() == lobbyId) {
                        if(!ingame() && lobbyState.getCurrentPhase().equals(LobbyPhase.INGAME)) {
                            if(gameStateReceived.getCount() == 0) {
                                LOG.trace("Replacing game state latch because it has been fired before");
                                gameStateReceived = new CountDownLatch(1);
                            } else {
                                LOG.trace("Keeping game state latch because it has not been fired yet");
                            }
                            currentGameId = lobbyState.getGameInformation().getGameId();
                        }
                        KryonetServerConnector.this.currentLobbyState.setState(lobbyState);
                        lobbyStateReceived.countDown();
                    }
                }
            } else if(o instanceof final LobbyInvitation lobbyInvitation) {
                lobbyInvitationListener.challengeReceived(lobbyInvitation.challengeBy(), lobbyInvitation.lobbyId());
            } else {
                ResponseTypeHandler responseTypeHandler = responseTypeHandlers.get(o.getClass());
                if(responseTypeHandler != null) {
                    responseTypeHandler.responseReceived(o);
                }
            }
        }
    }
}
