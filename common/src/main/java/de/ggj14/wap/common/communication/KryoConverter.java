/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.communication;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serializers.CollectionSerializer;
import de.ggj14.wap.common.communication.dto.*;
import de.ggj14.wap.common.communication.message.*;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameFieldElementPosition;
import de.ggj14.wap.common.datamodel.MatchMode;
import de.ggj14.wap.common.datamodel.MinionType;
import de.ggj14.wap.common.datamodel.conversation.Conversation;
import de.ggj14.wap.common.datamodel.lobby.LobbyPhase;
import de.ggj14.wap.common.gamefield.GameFieldElement;
import de.ggj14.wap.common.gamefield.objects.Minion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;

public class KryoConverter {

    private static final Logger LOG = LogManager.getLogger(KryoConverter.class);

    public static void registerKryoClasses(Kryo k) {
        k.register(GameStateTransferObject.class);
        k.register(GameFieldTransferObject.class);
        k.register(GameFieldElement.class);
        k.register(GameFieldElementPosition.class);
        k.register(Conversation.class);
        k.register(Faction.class);
        k.register(Minion.class);
        k.register(MinionType.class);
        k.register(Rectangle.class);
        k.register(MoveAction.class);
        k.register(ArrayList.class, new CollectionSerializer<>());
        k.register(LeaveRequest.class);
        k.register(LobbyListResponse.class);
        k.register(JoinRequest.class);
        k.register(JoinResponse.class);
        k.register(ListLobbiesRequest.class);
        k.register(ListLobbiesRequest.Type.class);
        k.register(ServerStatus.class);
        k.register(GameInformationRequest.class);
        k.register(GameInformation.class);
        k.register(ObserveRequest.class);
        k.register(TreeSet.class);
        k.register(TreeMap.class);
        k.register(ListPlayersRequest.class);
        k.register(ListPlayersResponse.class);
        k.register(ProgramVersion.class);
        k.register(ServerVersionRequest.class);
        k.register(ServerVersionResponse.class);
        k.register(LobbyPhase.class);
        k.register(MatchMode.class);
        k.register(LobbySettings.class);
        k.register(LobbyStateTransferObject.class);
        k.register(ClientChallengeRequest.class);
        k.register(ClientAuthenticationResponse.class);
        k.register(ClientChallengeResponse.class);
        k.register(ClientAuthenticationRequest.class);
        k.register(byte[].class);
        k.register(LobbyInvitation.class);
        k.register(CreateLobbyRequest.class);
        k.register(CreateLobbyResponse.class);
        k.register(ChallengeDeclinedMessage.class);
        k.register(LobbyChangeSettingsMessage.class);
        k.register(LobbyReadyMessage.class);
        k.register(LobbyAdvancePhaseRequest.class);
    }

}
