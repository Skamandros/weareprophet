/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.datamodel;

public enum MinionType {
    OLD_MAN(100.0f, 32, 64, 1, 30, "old_man_grey.png", "old_man_blue.png", "old_man_orange.png", "old_man_selected.png", false),
    YOUNG_MAN(120.0f, 32, 64, 2, 25, "young_man_grey.png", "young_man_blue.png", "young_man_orange.png", "young_man_selected.png", false),
    CHILD(150.0f, 32, 64, 1, 15, "child_grey.png", "child_blue.png", "child_orange.png", "child_selected.png", false),
    GOSPEL_WOMAN(80.0f, 32, 64, 3, 20, "gospel_woman_grey.png", "gospel_woman_blue.png", "gospel_woman_orange.png", "gospel_woman_selected.png", false),
    PROPHET(50.0f, 32, 64, 2, Integer.MAX_VALUE, "prophet_grey.png", "prophet_blue.png", "prophet_orange.png", "prophet_selected.png", true),
    UBER_PROPHET(20.0f, 32, 64, 400, Integer.MAX_VALUE, "prophet_grey.png", "prophet_grey.png", "prophet_grey.png", "prophet_selected.png", true),
    ;

    private static final String ASSET_DIRECTORY = "minions/";

    private final float speed;
    private final int width;
    private final int height;
    private final int persuasion;
    private final int willpower;
    private final String greyFile;
    private final String blueFile;
    private final String orangeFile;
    private final String selectedFile;
    private final boolean prophet;

    MinionType(float speed, int width, int height, int persuasion, int willpower,
               String greyFile, String blueFile, String orangeFile, String selectedFile, boolean prophet) {
        this.speed = speed;
        this.width = width;
        this.height = height;
        this.persuasion = persuasion;
        this.willpower = willpower;
        this.greyFile = ASSET_DIRECTORY + greyFile;
        this.blueFile = ASSET_DIRECTORY + blueFile;
        this.orangeFile = ASSET_DIRECTORY + orangeFile;
        this.selectedFile = ASSET_DIRECTORY + selectedFile;
        this.prophet = prophet;
    }

    public float getSpeed() {
        return speed;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getPersuasion() {
        return persuasion;
    }

    public int getWillpower() {
        return willpower;
    }

    public String getGreyFile() {
        return greyFile;
    }

    public String getBlueFile() {
        return blueFile;
    }

    public String getOrangeFile() {
        return orangeFile;
    }

    public boolean isProphet() {
        return prophet;
    }

    public String getSelectedFile() {
        return selectedFile;
    }
}
