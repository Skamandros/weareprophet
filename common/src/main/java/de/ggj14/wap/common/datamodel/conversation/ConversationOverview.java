/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.datamodel.conversation;

public class ConversationOverview {
    private final float player1Percentage;
    private final float player2Percentage;
    private final float greyPercentage;

    public ConversationOverview(float player1Percentage, float player2Percentage, float greyPercentage) {
        this.player1Percentage = player1Percentage;
        this.player2Percentage = player2Percentage;
        this.greyPercentage = greyPercentage;
    }

    public float getPlayer1Percentage() {
        return player1Percentage;
    }

    public float getPlayer2Percentage() {
        return player2Percentage;
    }

    public float getGreyPercentage() {
        return greyPercentage;
    }

    public int getNumberOfParticipatingFactions() {
        int count = 0;
        count += this.player1Percentage > 0.0f ? 1 : 0;
        count += this.player2Percentage > 0.0f ? 1 : 0;
        count += this.greyPercentage > 0.0f ? 1 : 0;
        return count;
    }

    @Override
    public String toString() {
        return "ConversationOverview [player1Percentage=" + player1Percentage
                + ", player2Percentage=" + player2Percentage + "]";
    }


}
