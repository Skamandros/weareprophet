/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

public class CommonValues {
    /**
     * the time of invulnerability is stated in milliseconds
     */
    public static final int TIME_OF_INVULNERABILITY = 2500;

    /**
     * the total minions on the map including prophets
     */
    public static final int MINIONS_AT_MAP = 10;

    /**
     * default number of columns
     */
    public static final int NUMBER_OF_COLUMNS = 6;

    /**
     * default number of rows
     */
    public static final int NUMBER_OF_ROWS = 4;

    /**
     * the width of a single map element in pixel
     */
    public static final int WIDTH_OF_MAP_ELEMENT = 180;

    /**
     * the height of a single map element in pixel
     */
    public static final int HEIGHT_OF_MAP_ELEMENT = 180;

    /**
     * the value returns the percentage a player has to
     * obtain of the total map to win the game
     */
    public static final float WIN_PERCENTAGE = 0.6f;

    /**
     * The maximum amount of characters that a player's nickname is allowed to have.
     */
    public static final int MAX_NICKNAME_LENGTH = 11;

    /**
     * The pattern of allowed nicknames.
     */
    public static final Pattern ALLOWED_NICKNAMES = Pattern.compile("[A-Za-z0-9]{1," + MAX_NICKNAME_LENGTH + "}");

    /**
     * The initial cooldown for server refreshes, in seconds.
     */
    public static final int REFRESH_COOLDOWN = 1;

    public static final String SIGNATURE_ALGORITHM = "SHA1WithRSA";
    public static final String CLIENT_KEY_ALGORITHM = "RSA";

    private CommonValues() {

    }

    public static int getNumberOfFieldsToWin() {
        return getNumberOfFieldsToWin(0);
    }

    public static int getNumberOfFieldsToWin(int lockedTiles) {
        return (int) Math.ceil((NUMBER_OF_COLUMNS * NUMBER_OF_ROWS - lockedTiles) * WIN_PERCENTAGE);
    }

    public static KeyPair generateClientKeys() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance(CommonValues.CLIENT_KEY_ALGORITHM);
        generator.initialize(1024);
        return generator.generateKeyPair();
    }
}
