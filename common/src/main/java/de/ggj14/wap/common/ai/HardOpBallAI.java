/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.common.ai;

import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameFieldElementPosition;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.MinionType;
import de.ggj14.wap.common.datamodel.conversation.Conversation;
import de.ggj14.wap.common.gamefield.GameFieldElement;
import de.ggj14.wap.common.gamefield.MoveActionHandler;
import de.ggj14.wap.common.gamefield.objects.Minion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * <pre>
 * This AI does the following:
 * (A) Prophet:
 *    1. Evade pensioner, unless an own minion is nearby
 *    2. Help an own minion in a conversation
 *    3. Try to catch a Gospel Woman
 *    4. Try to catch a Young Man
 *    5. Try to catch a Little child
 *    6. Meet with the ball
 * (B) Minions:
 *    1. Evade the other prophet OR attack the prophet if you're an old man
 *    2. Gather to a ball
 *    3. Help the prophet in a conversation
 *    4. Attack other minions, if the own prophet is closer than the enemy prophet
 *    5. Capture fields
 * </pre>
 *
 * @author SharkofMetal
 */
public class HardOpBallAI implements AI {

    private static final Logger LOG = LogManager.getLogger(HardOpBallAI.class);
    private MoveActionHandler moveActionHandler;
    private static final long THOUGHT_INTERVAL = 200;
    private ProphetAI prophetAI = null;
    private long lastThought = 0;
    private Minion enemyProphet = null;
    private int ballX;
    private int ballY;
    private Faction faction;

    @Override
    public void init(int rows, int columns, Faction assigedFaction, MoveActionHandler moveActionHandler) {
        ballX = rows * CommonValues.WIDTH_OF_MAP_ELEMENT / 2;
        ballY = columns * CommonValues.HEIGHT_OF_MAP_ELEMENT / 2;
        this.faction = assigedFaction;
        this.moveActionHandler = moveActionHandler;
    }

    @Override
    public void update(GameState gs) {
        for(Minion m : gs.getMinionMap().values()) {
            if(m.getMinionType().isProphet() && m.getFaction().equals(faction)) {
                this.prophetAI = new ProphetAI(m);
            } else if(m.getMinionType().isProphet()) {
                enemyProphet = m;
            }
        }
        if(gs.getTime() - THOUGHT_INTERVAL > lastThought) {
            this.prophetAI.think(gs);
            for(final Minion myMinion : filterByType(
                    filterByFaction(gs.getMinionMap().values(), EnumSet.of(faction)),
                    EnumSet.complementOf(EnumSet.of(MinionType.PROPHET)))) {
                minionThink(gs, myMinion);
            }
            applyBallAi(gs);
            lastThought = gs.getTime();
        }
    }

    @Override
    public String getName() {
        return "OP ball prophet";
    }

    private void applyBallAi(GameState gs) {
        // (B) 3. Help the prophet in a conversation
        final Conversation prophetConversation = gs.getMinionIDConversationMap().get(prophetAI.myProphet.getId());
        if(prophetConversation != null) {
            List<Minion> enemyMinions = filterByFaction(prophetConversation.getParticipants(),
                    EnumSet.complementOf(EnumSet.of(faction)));
            ballX = Math.round(enemyMinions.get(0).getX());
            ballY = Math.round(enemyMinions.get(0).getY());
            LOG.debug("[Ball] 3. Help the prophet in a conversation");
            return;
        }
        // (B) 4. Attack other minions, if the own prophet is closer than the
        // enemy prophet
        if(attackOtherMinions(gs)) {
            LOG.debug("[Ball] 4. Attack other minions, if the own prophet is closer than the enemy prophet");
            return;
        }
        // (B) 5. Capture fields
        Rectangle dominationTarget = determineDominationTarget(gs);
        ballX = dominationTarget.x + dominationTarget.width / 2;
        ballY = dominationTarget.y + dominationTarget.height / 2;
        LOG.debug("[Ball] 5. Capture fields");
    }

    private boolean attackOtherMinions(GameState gs) {
        final List<Minion> candidates = filterByFaction(gs.getMinionMap().values(),
                EnumSet.complementOf(EnumSet.of(faction)));
        Iterator<Minion> iterator = candidates.iterator();
        while(iterator.hasNext()) {
            Minion minion = iterator.next();
            Conversation conversation = gs.getMinionIDConversationMap().get(minion);
            if(conversation != null && conversation.hasProphet()) {
                iterator.remove();
            } else if(minion.distanceToMinion(enemyProphet) < minion.distanceToMinion(prophetAI.myProphet)) {
                iterator.remove();
            } else if(minion.getMinionType().equals(MinionType.PROPHET)) {
                iterator.remove();
            }
        }
        if(!candidates.isEmpty()) {
            Minion targetMinion = candidates.get(0);
            ballX = Math.round(targetMinion.getX());
            ballY = Math.round(targetMinion.getY());
            return true;
        }
        return false;
    }

    private void minionThink(GameState gs, final Minion shouldThink) {
        // (B) 1. Evade the other prophet OR attack the other prophet (old man
        // only)
        if(shouldThink.getMinionType() == MinionType.OLD_MAN
                && shouldThink.distanceToMinion(enemyProphet) <= DistanceQuality.NEARBY.maxDistance) {
            moveActionHandler.handleMoveAction(new MoveAction(shouldThink.getId(),
                    Math.round(enemyProphet.getX()),
                    Math.round(enemyProphet.getY())));
            LOG.debug("[" + shouldThink.getMinionType().toString() + " " + shouldThink.getId()
                    + "] 1. Attack the other prophet");
        } else if(shouldThink.distanceToMinion(enemyProphet) <= DistanceQuality.CLOSE.maxDistance) {
            evasiveMove(shouldThink, enemyProphet);
            LOG.debug("[" + shouldThink.getMinionType().toString() + " " + shouldThink.getId()
                    + "] 1. Evade the other prophet");
            return;
        }
        // (B) 2. Gather to a ball
        if(distanceToBall(shouldThink) > DistanceQuality.CLOSE.maxDistance) {
            moveActionHandler.handleMoveAction(new MoveAction(shouldThink.getId(), ballX, ballY));
            LOG.debug("[" + shouldThink.getMinionType().toString() + " " + shouldThink.getId()
                    + "] 2. Gather to a ball (non-exclusive)");
            return;
        }
    }

    private Rectangle determineDominationTarget(GameState gs) {
        Rectangle position = null;
        float bestValue = -Float.MAX_VALUE;
        for(ObjectObjectCursor<GameFieldElementPosition, GameFieldElement> gf : gs.getGameField().getGameFieldMap()) {
            final float x = gf.value.getField().x + gf.value.getField().width / 2;
            final float y = gf.value.getField().y + gf.value.getField().height / 2;
            final float distanceToField = (float) Math.sqrt(Math.pow((ballX - x), 2.0f) + Math.pow((ballY - y), 2.0f));
            final float distanceToEnemyProphet = (float) Math
                    .sqrt(Math.pow((enemyProphet.getX() - x), 2.0f) + Math.pow((enemyProphet.getY() - y), 2.0f));
            final float distanceToOurProphet = (float) Math.sqrt(Math.pow((prophetAI.myProphet.getX() - x), 2.0f)
                    + Math.pow((prophetAI.myProphet.getY() - y),
                    2.0f));
            final float dominationStateValue = gf.value.getDominationState();
            float value = (float) dominationStateValue;
            switch(DistanceQuality.forDistance(distanceToEnemyProphet)) {
                case CLOSE:
                    value -= 20;
                    break;
                case NEARBY:
                    value -= 8;
                    break;
                default:
            }
            switch(DistanceQuality.forDistance(distanceToOurProphet)) {
                case CLOSE:
                    value += 5;
                    break;
                case NEARBY:
                    value += 3;
                    break;
                case IN_RANGE:
                    value += 2;
                    break;
                default:
            }
            switch(DistanceQuality.forDistance(distanceToField)) {
                case CLOSE:
                    value += 10;
                    break;
                case NEARBY:
                    value += 5;
                    break;
                case IN_RANGE:
                    value += 3;
                    break;
                default:
            }
            if(value > bestValue) {
                bestValue = value;
                position = gf.value.getField();
            }
        }
        return position;
    }

    private double distanceToBall(Minion toCheck) {
        return Math.sqrt(Math.pow(toCheck.getX() - ballX, 2) + Math.pow(toCheck.getY() - ballY, 2));
    }

    private void evasiveMove(Minion evasiveMinion, Minion fleeFrom) {
        float vectorX = evasiveMinion.getX() - fleeFrom.getX();
        float vectorY = evasiveMinion.getY() - fleeFrom.getY();
        double length = Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));

        double normalizedX = vectorX / length;
        double normalizedY = vectorY / length;

        final int x = (int) (normalizedX * (70.0 + Math.random() * 60.0) + evasiveMinion.getX());
        final int y = (int) (normalizedY * (70.0 + Math.random() * 60.0) + evasiveMinion.getY());
        moveActionHandler.handleMoveAction(new MoveAction(evasiveMinion.getId(), x, y));
    }

    /**
     * Filters a list of minions by their type.
     * <p>
     * Does not modify the supplied list, but rather return a new one.
     *
     * @param toFilter The minion list to filter.
     * @param types    The types to look for.
     * @return A list of filtered minions.
     */
    private List<Minion> filterByType(final Collection<Minion> toFilter, final Collection<MinionType> types) {
        final List<Minion> result = new ArrayList<Minion>(toFilter.size());
        for(final Minion m : toFilter) {
            if(types.contains(m.getMinionType())) {
                result.add(m);
            }
        }
        return result;
    }

    /**
     * Filters a list of minions by their faction.
     * <p>
     * Does not modify the supplied list, but rather return a new one.
     *
     * @param toFilter The minion list to filter.
     * @param factions The factions to look for.
     * @return A list of filtered minions.
     */
    private List<Minion> filterByFaction(final Collection<Minion> toFilter, final Collection<Faction> factions) {
        final List<Minion> result = new ArrayList<Minion>(toFilter.size());
        for(final Minion m : toFilter) {
            if(factions.contains(m.getFaction())) {
                result.add(m);
            }
        }
        return result;
    }

    /**
     * Filters a list of minions from those in conversation.
     * <p>
     * Does not modify the supplied list, but rather return a new one.
     *
     * @param toFilter The minion list to filter.
     * @return A list of filtered minions.
     */
    private List<Minion> filterMinionsInConversation(final GameState gs, final Collection<Minion> toFilter) {
        final List<Minion> result = new ArrayList<Minion>(toFilter.size());
        for(final Minion m : toFilter) {
            if(!gs.getMinionIDConversationMap().containsKey(m.getId())) {
                result.add(m);
            }
        }
        return result;
    }

    /**
     * Determines a list of allied minions which are in a conversation without
     * the opposing prophet.
     *
     * @param gs The current game state.
     * @return A list of minions in conversation.
     */
    private List<Minion> determineOwnMinionsInConversation(GameState gs) {
        final List<Minion> result = new ArrayList<Minion>();
        for(Conversation c : gs.getIdConversationMap().values()) {
            if(!c.hasProphet()) { // we can't help if there's a prophet
                for(Minion m : c.getParticipants()) {
                    if(m.getFaction().equals(faction)) {
                        result.add(m);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Determines whether an allied minion is nearby the supplied minion.
     *
     * @param whoNeedsSupport The minion which is the reference point.
     * @param gs              The current game state.
     * @return True if an allied minion is nearby the supplied minion.
     */
    private boolean isSupportNearby(Minion whoNeedsSupport, GameState gs) {
        for(Minion m : gs.getMinionMap().values()) {
            if(m.getFaction().equals(faction) && !m.equals(whoNeedsSupport)
                    && m.distanceToMinion(whoNeedsSupport) <= DistanceQuality.NEARBY.maxDistance) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines the most persuasive minion from a list of minions.
     *
     * @param gs         The current game state.
     * @param minionList The list from which to determine the minion.
     * @return The determined minion, that is, the one with the highest
     * persuasion from the supplied list.
     */
    private Minion determineMostPersuasiveMinion(GameState gs, List<Minion> minionList) {
        Minion result = minionList.get(0);
        for(Minion m : minionList.subList(1, minionList.size())) {
            if(result.getMinionType().getPersuasion() < m.getMinionType().getPersuasion()) {
                result = m;
            }
        }
        return result;
    }


    private enum DistanceQuality {
        CLOSE(120), NEARBY(350), IN_RANGE(600), FAR(Integer.MAX_VALUE);

        private final int maxDistance;

        private DistanceQuality(int maxDistance) {
            this.maxDistance = maxDistance;
        }

        static DistanceQuality forDistance(float distance) {
            for(DistanceQuality d : values()) {
                if(d.maxDistance >= distance) {
                    return d;
                }
            }
            return null;
        }
    }

    private final class ProphetAI {
        private final Minion myProphet;

        ProphetAI(Minion myProphet) {
            this.myProphet = myProphet;
        }

        public void think(GameState gs) {
            // (A) 1. Evade pensioner, unless an own minion is nearby
            if(evadePensioners(gs)) {
                LOG.debug("[Prophet] 1. Evade pensioner, unless an own minion is nearby");
                return;
            }
            // (A) 2. Help an own minion in a conversation
            if(helpAlliedMinion(gs)) {
                LOG.debug("[Prophet] 2. Help an own minion in a conversation");
                return;
            }
            // (A) 3. Try to catch a Gospel Woman
            if(catchMinion(gs, MinionType.GOSPEL_WOMAN, EnumSet.complementOf(EnumSet.of(faction)))) {
                LOG.debug("[Prophet] 3. Try to catch a Gospel Woman");
                return;
            }
            // (A) 4. Try to catch a Young Man
            if(catchMinion(gs, MinionType.YOUNG_MAN, EnumSet.complementOf(EnumSet.of(faction)))) {
                LOG.debug("[Prophet] 4. Try to catch a Young Man");
                return;
            }
            // (A) 5. Try to catch a Little Child
            if(catchMinion(gs, MinionType.CHILD, EnumSet.of(Faction.NEUTRAL))) {
                LOG.debug("[Prophet] 5. Try to catch a Little Child");
                return;
            }
            // (A) 6. Meet with the ball
            moveActionHandler.handleMoveAction(new MoveAction(myProphet.getId(), ballX, ballY));
            LOG.debug("[Prophet] 6. Position in the center of the board");
        }

        private boolean catchMinion(GameState gs, final MinionType typeToCatch, final Collection<Faction> factions) {
            List<Minion> candidates = filterMinionsInConversation(gs,
                    filterByMaxDistance(
                            filterByType(filterByFaction(
                                            gs.getMinionMap().values(), factions),
                                    EnumSet.of(typeToCatch)),
                            DistanceQuality.NEARBY));
            if(candidates.isEmpty()) {
                return false;
            } else {
                Minion targetMinion = candidates.get(0);
                for(Minion m : candidates.subList(1, candidates.size())) {
                    if(targetMinion.distanceToMinion(myProphet) > m.distanceToMinion(myProphet)) {
                        targetMinion = m;
                    }
                }
                moveToMinion(targetMinion);
                return true;
            }
        }

        /**
         * Helps an allied minion.
         * <p>
         * This is a prophet move.
         *
         * @param gs The current game state.
         * @return True if this action was taken.
         */
        private boolean helpAlliedMinion(GameState gs) {
            final List<Minion> ownMinionsInConversation = determineOwnMinionsInConversation(gs);
            List<Minion> inRange = filterByMaxDistance(ownMinionsInConversation, DistanceQuality.IN_RANGE);
            if(inRange.isEmpty()) {
                return false;
            } else {
                final Minion myMinion = determineMostPersuasiveMinion(gs, inRange);
                final Minion targetMinion = filterByFaction(
                        gs.getMinionIDConversationMap().get(myMinion.getId()).getParticipants(),
                        EnumSet.complementOf(EnumSet.of(faction))).get(0);
                moveToMinion(targetMinion);
                return true;
            }
        }

        private void moveToMinion(final Minion targetMinion) {
            final float targetStartX = targetMinion.getX();
            final float targetStartY = targetMinion.getY();
            double l = Math.sqrt(Math.pow(targetMinion.getTargetPosX() - targetStartX, 2)
                    + Math.pow(targetMinion.getTargetPosY() - targetStartY, 2));

            double dx = (targetMinion.getTargetPosX() - targetStartX) / l;
            double dy = (targetMinion.getTargetPosY() - targetStartY) / l;

            final float targetX = (float) (targetStartX
                    + (THOUGHT_INTERVAL / 1000.0f * (targetMinion.getMinionType().getSpeed()) * dx));
            final float targetY = (float) (targetStartY
                    + (THOUGHT_INTERVAL / 1000.0f * (targetMinion.getMinionType().getSpeed()) * dy));

            moveActionHandler.handleMoveAction(
                    new MoveAction(myProphet.getId(), Math.round(targetX), Math.round(targetY)));
        }

        /**
         * Evades enemy pensioners, unless an allied minion is nearby.
         * <p>
         * This is a prophet move.
         *
         * @param gs The current game state.
         * @return True if this action was taken.
         */
        private boolean evadePensioners(GameState gs) {
            boolean moveApplied = false;
            final List<Minion> enemyPensioners = filterByFaction(
                    filterByType(gs.getMinionMap().values(), EnumSet.of(MinionType.OLD_MAN)),
                    EnumSet.of(faction));
            final boolean supportNearby = isSupportNearby(myProphet, gs);
            if(!supportNearby) {
                double closestOldMan = Double.POSITIVE_INFINITY;
                for(Minion oldMan : enemyPensioners) {
                    final double d = oldMan.distanceToMinion(myProphet);
                    if(d <= DistanceQuality.CLOSE.maxDistance && d <= closestOldMan) {
                        evasiveMove(myProphet, oldMan);
                        closestOldMan = d;
                        moveApplied = true;
                    }
                }
            }
            return moveApplied;
        }

        /**
         * Filters a list of minions by their distance to our prophet.
         * <p>
         * Does not modify the supplied list, but rather return a new one.
         *
         * @param toFilter    The minion list to filter.
         * @param maxDistance The maximum distance allowed.
         * @return A list of filtered minions.
         */
        private List<Minion> filterByMaxDistance(final Collection<Minion> toFilter, final DistanceQuality maxDistance) {
            final List<Minion> result = new ArrayList<Minion>(toFilter.size());
            for(final Minion m : toFilter) {
                if(m.distanceToMinion(myProphet) <= maxDistance.maxDistance) {
                    result.add(m);
                }
            }
            return result;
        }

    }

}
