/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.common.datamodel;

import de.ggj14.wap.common.I18NProperty;

/**
 * Enumerates possible modes for a multiplayer match.
 */
public enum MatchMode {
    SINGLE_GAME(
            MatchModeText.SINGLE_GAME_LABEL,
            MatchModeText.SINGLE_GAME_DESCRIPTION,
            1),
    BO_3(
            MatchModeText.BEST_OF_3_LABEL,
            MatchModeText.BEST_OF_3_DESCRIPTION,
            2),
    BO_3_WITH_ADVANTAGE(
            MatchModeText.BEST_OF_3_WITH_ADVANTAGE_LABEL,
            MatchModeText.BEST_OF_3_WITH_ADVANTAGE_DESCRIPTION,
            2,
            1),
    BO_5(
            MatchModeText.BEST_OF_5_LABEL,
            MatchModeText.BEST_OF_5_DESCRIPTION,
            3),
    BO_5_WITH_ADVANTAGE(
            MatchModeText.BEST_OF_5_WITH_ADVANTAGE_LABEL,
            MatchModeText.BEST_OF_5_WITH_ADVANTAGE_DESCRIPTION,
            3,
            1),
    BO_7(
            MatchModeText.BEST_OF_7_LABEL,
            MatchModeText.BEST_OF_7_DESCRIPTION,
            4),
    BO_7_WITH_ADVANTAGE(
            MatchModeText.BEST_OF_7_WITH_ADVANTAGE_LABEL,
            MatchModeText.BEST_OF_7_WITH_ADVANTAGE_DESCRIPTION,
            4,
            1),
    ;
    public final I18NProperty labelProperty;

    public final I18NProperty descriptionProperty;

    public final int scoreRequiredToWin;

    public final int startScoreForPlayer1;

    MatchMode(final I18NProperty labelProperty, final I18NProperty descriptionProperty, final int scoreRequiredToWin) {
        this(labelProperty, descriptionProperty, scoreRequiredToWin, 0);
    }

    MatchMode(final I18NProperty labelProperty, final I18NProperty descriptionProperty, final int scoreRequiredToWin, final int startScoreForPlayer1) {
        this.labelProperty = labelProperty;
        this.descriptionProperty = descriptionProperty;
        this.scoreRequiredToWin = scoreRequiredToWin;
        this.startScoreForPlayer1 = startScoreForPlayer1;
    }


    public I18NProperty getLabelProperty() {
        return labelProperty;
    }

    public I18NProperty getDescriptionProperty() {
        return descriptionProperty;
    }


    private enum MatchModeText implements I18NProperty {
        SINGLE_GAME_LABEL, SINGLE_GAME_DESCRIPTION,
        BEST_OF_3_LABEL, BEST_OF_3_DESCRIPTION,
        BEST_OF_3_WITH_ADVANTAGE_LABEL, BEST_OF_3_WITH_ADVANTAGE_DESCRIPTION,
        BEST_OF_5_LABEL, BEST_OF_5_DESCRIPTION,
        BEST_OF_5_WITH_ADVANTAGE_LABEL, BEST_OF_5_WITH_ADVANTAGE_DESCRIPTION,
        BEST_OF_7_LABEL, BEST_OF_7_DESCRIPTION,
        BEST_OF_7_WITH_ADVANTAGE_LABEL, BEST_OF_7_WITH_ADVANTAGE_DESCRIPTION,
    }
}
