/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.gamefield;

import com.carrotsearch.hppc.ObjectIntHashMap;
import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.datamodel.DominationStateRange;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.MinionType;
import de.ggj14.wap.common.gamefield.objects.Minion;

import java.awt.*;

public class GameFieldElement {
    private Faction fieldFaction;
    private boolean wasCapturedAtLeastOnce;
    private Rectangle field;
    private float dominationState = 0f;
    private boolean gameFieldIsLocked;
    transient private ObjectIntHashMap<Faction> factionCountMap;
    transient private boolean greyProphetAttacking;

    /**
     * only use for deserialization
     */
    public GameFieldElement() {
    }

    public GameFieldElement(int row, int col) {
        fieldFaction = Faction.NEUTRAL;
        wasCapturedAtLeastOnce = false;
        int height = CommonValues.HEIGHT_OF_MAP_ELEMENT;
        int width = CommonValues.WIDTH_OF_MAP_ELEMENT;
        field = new Rectangle(col * width, row * height, width, height);

        factionCountMap = new ObjectIntHashMap<Faction>();
        greyProphetAttacking = false;
        gameFieldIsLocked = false;
    }

    public Faction getFieldFaction() {
        return fieldFaction;
    }

    public void resetFactionCountMap() {
        getFactionCountMap().clear();
    }

    public void incrementFactionMapElement(Minion min) {
        getFactionCountMap().putOrAdd(min.getFaction(),
                min.getMinionType().getPersuasion(),
                min.getMinionType().getPersuasion());

        if(min.getFaction().equals(Faction.NEUTRAL)) {
            if(min.getMinionType().equals(MinionType.PROPHET)) {
                greyProphetAttacking = true;
            }
        }
    }

    public void calculateDominatingFaction(int timeDelta) {
        // if gamefield is locked, return immediately
        if(isGameFieldLocked())
            return;

        if(!getFactionCountMap().isEmpty()) {
            int play1Count = 0;
            int play2Count = 0;
            int greyCount = 0;

            int index;
            if((index = getFactionCountMap().indexOf(Faction.PLAYER1)) >= 0) {
                play1Count = getFactionCountMap().indexGet(index);
            }

            if((index = getFactionCountMap().indexOf(Faction.PLAYER2)) >= 0) {
                // invert value for preparation to calc dominationState
                play2Count = -1 * getFactionCountMap().indexGet(index);
            }

            dominationState += (float) (play1Count / 1000.0 * timeDelta);
            dominationState += (float) (play2Count / 1000.0 * timeDelta);

            // the grey faction is always working against the dominating faction
            if((index = getFactionCountMap().indexOf(Faction.NEUTRAL)) >= 0) {
                greyCount = getFactionCountMap().indexGet(index);

                if(dominationState > 0) {
                    greyCount *= -1;
                    float greyDominationStateChange = (float) (greyCount / 1000.0 * timeDelta);

                    // remove maximum of current dominion state, but result should be not lower then 0
                    dominationState += Math.max(greyDominationStateChange, -1 * dominationState);
                } else {
                    float greyDominationStateChange = (float) (greyCount / 1000.0 * timeDelta);

                    // remove maximum of current dominion state, but result should be not higher then 0
                    dominationState += Math.min(greyDominationStateChange, -1 * dominationState);
                }
            }

            dominationState = DominationStateRange.normalize(dominationState);
            setFieldFaction(DominationStateRange.getFaction(dominationState));

            // when the gamefield is grey, attacked by the grey prophet and was once captures by orange/blue it will
            // now be locked and not be attackable anymore
            if(getFieldFaction().equals(Faction.NEUTRAL) && dominationState == 0.0f && greyProphetAttacking && wasCapturedAtLeastOnce) {
                gameFieldIsLocked = true;
            }

            resetFactionCountMap();
            greyProphetAttacking = false;
        }
    }

    public Rectangle getField() {
        return field;
    }

    public void setField(Rectangle field) {
        this.field = field;
    }

    public ObjectIntHashMap<Faction> getFactionCountMap() {
        if(factionCountMap == null) {
            factionCountMap = new ObjectIntHashMap<Faction>();
        }
        return factionCountMap;
    }

    public float getDominationState() {
        return dominationState;
    }

    public void setFactionCountMap(ObjectIntHashMap<Faction> factionCountMap) {
        this.factionCountMap = factionCountMap;
    }

    public void setFieldFaction(Faction fieldFaction) {
        if(!fieldFaction.equals(Faction.NEUTRAL)) {
            this.wasCapturedAtLeastOnce = true;
        }
        this.fieldFaction = fieldFaction;
    }

    /**
     * Function to get information whether this field was captured once.
     *
     * @return true if the field was captured once by either orange or blue, false otherwise
     */
    public boolean isWasCapturedAtLeastOnce() {
        return wasCapturedAtLeastOnce;
    }

    public boolean isGameFieldLocked() {
        return gameFieldIsLocked;
    }
}
