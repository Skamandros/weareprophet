/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.common.datamodel.lobby;

import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.communication.dto.LobbySettings;
import de.ggj14.wap.common.communication.dto.LobbyStateTransferObject;

import java.util.Optional;
import java.util.SortedSet;

/**
 * Represents a game lobby.
 */
public interface Lobby extends Comparable<Lobby> {
    /**
     * Indicates a lobby which has not been created on the server yet.
     */
    int UNSET_LOBBY_ID = -1;

    /**
     * @return A new lobby.
     */
    static Lobby create() {
        return new LobbyImpl();
    }

    /**
     * Restores a lobby from its transfer object.
     *
     * @param transferObject The transfer object to use.
     * @return The restored lobby.
     */
    static Lobby fromTransferObject(LobbyStateTransferObject transferObject) {
        return new LobbyImpl(transferObject);
    }

    /**
     * @return The name of player 1, if a player has joined that slot.
     */
    Optional<String> getPlayer1();

    /**
     * @return The name of player 2, if a player has joined that slot.
     */
    Optional<String> getPlayer2();

    /**
     * Checks whether a given player is prophet orange or blue of this lobby.
     *
     * @param playerName The name of the player to check.
     * @return True iff this player is one of the prophets in this lobby.
     */
    boolean isProphet(String playerName);

    /**
     * Transforms this lobby into its transfer object.
     *
     * @return The corresponding transfer object.
     */
    LobbyStateTransferObject toTransferObject();

    /**
     * @return The score of player 1 which is >= 0.
     */
    int getScoreOfPlayer1();

    /**
     * @return The score of player 2 which is >= 0.
     */
    int getScoreOfPlayer2();

    /**
     * Whether the second player has set the ready flag.
     *
     * @return True iff this lobby is ready to transition to the next phase.
     */
    boolean isReady();

    /**
     * @return The phase in which this lobby is currently in.
     */
    LobbyPhase getCurrentPhase();

    /**
     * @return The current settings of the lobby. Can only be changed during {@link LobbyPhase#SETUP}.
     */
    LobbySettings getSettings();

    /**
     * @return A list of the current observers.
     */
    SortedSet<String> getObservers();

    /**
     * @return The id of this lobby.
     */
    int getId();

    GameInformation getGameInformation();

    /**
     * @return True iff a game is currently in progress in this lobby.
     */
    boolean isGameRunning();

    void setState(LobbyStateTransferObject lobbyState);
}
