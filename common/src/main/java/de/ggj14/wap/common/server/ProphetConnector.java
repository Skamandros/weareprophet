/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.common.server;

import de.ggj14.wap.common.communication.dto.GameInformation;
import de.ggj14.wap.common.communication.dto.LobbySettings;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.lobby.Lobby;
import de.ggj14.wap.common.gamefield.MoveActionHandler;

import java.io.IOException;
import java.util.SortedSet;

/**
 * Sends and receives data from a prophet server. Instances of this type are stateful and know the connection status
 * and current game.
 */
public interface ProphetConnector extends MoveActionHandler {
    /**
     * Connect to the configured server.
     *
     * @param playerName The nickname which shall be used for the connection.
     * @throws ConnectionFailedException Thrown if the connection can't be established.
     */
    void connect(String playerName) throws ConnectionFailedException;

    /**
     * Disconnects from the server.
     */
    void disconnect();

    /**
     * Leave the current lobby.
     */
    void leaveLobby();


    /**
     * Creates a new lobby.
     *
     * @param otherPlayer The player who shall be challenged.
     * @throws JoinFailedException Thrown, if the server refused to create the lobby
     */
    void createLobby(String otherPlayer) throws JoinFailedException;

    /**
     * Joins a lobby.
     *
     * @param lobbyId The lobby to join
     * @return The assigned faction
     * @throws JoinFailedException Thrown, if the lobby cannot be joined, e.g. because the server
     *                             has reached the maximum number of running games
     */
    Faction joinLobby(int lobbyId) throws JoinFailedException;

    /**
     * Joins a lobby as an observer.
     *
     * @param lobbyId The lobby to join
     * @throws JoinFailedException Thrown, if the lobby cannot be joined
     */
    void observeLobby(int lobbyId) throws JoinFailedException;


    /**
     * Attempts to change the settings of the current lobby.
     *
     * @param toSet The new settings to set.
     */
    void lobbyChangeSettings(LobbySettings toSet);

    void lobbyChangeReadyState(boolean ready);

    /**
     * Attempts to start the next game in the current lobby.
     */
    void lobbyStartGame();

    /**
     * Declines a lobby invitation.
     *
     * @param lobbyId The lobby ID of the challenge.
     */
    void declineInvitation(int lobbyId);

    /**
     * Retrieves the current lobby state.
     *
     * @return The current lobby in which we are in.
     * @throws InterruptedException Thrown, if the lobby state isn't available in time
     */
    Lobby getCurrentLobby() throws InterruptedException;

    /**
     * Retrieves the current global game state.
     *
     * @return The game state of the game this player is currently in
     * @throws IllegalStateException Thrown, if the player hasn't joined a game
     * @throws InterruptedException  Thrown, if the game state isn't available in time
     */
    GameState getCurrentGameState() throws IllegalStateException, InterruptedException;

    /**
     * @return Information about the current game
     */
    GameInformation getGameInformation() throws IOException;

    /**
     * @return A list of all lobbies in which this client is a player.
     * @see #listOtherLobbies()
     */
    SortedSet<Lobby> listMyLobbies() throws IOException;

    /**
     * @return A list of all lobbies in which this client is <em>not</em> a player.
     * @see #listMyLobbies()
     */
    SortedSet<Lobby> listOtherLobbies() throws IOException;

    /**
     * Requests a list of all players on the current server.
     *
     * @return A list of all players on the current server.
     */
    SortedSet<String> getPlayersOnServer() throws IOException;

    /**
     * Whether this connector is connected at the moment.
     *
     * @return True iff there is an active server connection.
     */
    boolean isConnected();

    /**
     * Adds a listener for game state updates from the server.
     *
     * @param toAdd The listener to add.
     */
    void addListener(final GameStateUpdateListener toAdd);

    /**
     * Removes a game state update listener.
     *
     * @param toRemove The listener to remove.
     */
    void removeListener(final GameStateUpdateListener toRemove);

    /**
     * Adds a listener for connection state changes.
     *
     * @param toAdd The listener to add.
     */
    void addListener(final ConnectionStateListener toAdd);

    /**
     * Removes a connection state listener.
     *
     * @param toRemove The listener to remove.
     */
    void removeListener(final ConnectionStateListener toRemove);

    int getPing();

    /**
     * Handles game state updates received from the server.
     */
    interface GameStateUpdateListener {
        /**
         * Called after the client connector has received a new server game state.
         *
         * @param receivedFromServer The game state received from the server.
         * @param firstUpdate        True iff this is the first game state received since the last (re)connect.
         */
        void update(final GameState receivedFromServer, boolean firstUpdate);
    }

    /**
     * Handles connection state changes.
     */
    interface ConnectionStateListener {
        void disconnected();

        void connected();
    }
}
