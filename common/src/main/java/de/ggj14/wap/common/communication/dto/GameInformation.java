/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.communication.dto;

import com.google.common.collect.ComparisonChain;
import de.ggj14.wap.common.datamodel.Faction;

import java.util.Map;
import java.util.TreeMap;

/**
 * Contains information about an open or running game.
 *
 * @author SharkOfMetal
 */
public class GameInformation implements Comparable<GameInformation> {

    /**
     * A game ID which indicates that no game ID has been set yet.
     */
    public static final int NO_GAME_ID = -1;

    private int gameID;

    private Map<Faction, String> playerNames = new TreeMap<>();

    private boolean greyProphetMode;

    /**
     * For deserialization.
     */
    public GameInformation() {
    }

    public GameInformation(int gameID, Map<Faction, String> playerNames, boolean greyProphetMode) {
        super();
        this.gameID = gameID;
        this.playerNames = playerNames;
        this.greyProphetMode = greyProphetMode;
    }

    /**
     * Copy constructor for defensive copies.
     *
     * @param gameInformation The object to copy.
     */
    public GameInformation(GameInformation gameInformation) {
        this(gameInformation.gameID, new TreeMap<>(gameInformation.playerNames), gameInformation.greyProphetMode);
    }

    public int getGameId() {
        return gameID;
    }

    public void setGameID(int gameID) {
        this.gameID = gameID;
    }

    public Map<Faction, String> getPlayerNames() {
        return playerNames;
    }

    public void setPlayerNames(Map<Faction, String> playerNames) {
        this.playerNames = playerNames;
    }

    @Override
    public String toString() {
        if(playerNames.containsKey(Faction.PLAYER2)) {
            return playerNames.get(Faction.PLAYER1) + " vs. " + playerNames.get(Faction.PLAYER2);
        } else {
            return playerNames.get(Faction.PLAYER1);
        }
    }

    @Override
    public int compareTo(GameInformation o) {
        return ComparisonChain.start().compare(this.getGameId(), o.getGameId()).result();
    }

    public boolean isGreyProphetMode() {
        return greyProphetMode;
    }
}
