/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.ai;

import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameFieldElementPosition;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.MinionType;
import de.ggj14.wap.common.gamefield.GameFieldElement;
import de.ggj14.wap.common.gamefield.MoveActionHandler;
import de.ggj14.wap.common.gamefield.objects.Minion;

import java.awt.*;
import java.util.EnumSet;
import java.util.Random;

public class MediumEnhancedAI implements AI {
    private int rows;
    private int columns;
    private Minion enemyProphet = null;
    Random r = new Random();
    int minionsUnderControl = 1;
    long lastTargetUpdate = 0;
    long targetUpdateCooldown = 1000000000; // in nanoseconds -> 1s
    boolean updateNow = false;
    private MoveActionHandler moveActionHandler;
    private Faction faction;

    public void update(GameState gs) {

        // check: ai needs new orders?
        long currentTime = System.nanoTime();
        if(currentTime - targetUpdateCooldown > lastTargetUpdate) {
            lastTargetUpdate = currentTime;
            updateNow = true;
        }

        int tempMinionCounter = 0;
        for(Minion m : gs.getMinionMap().values()) {
            if(m.getFaction().equals(faction)) {
                tempMinionCounter++;
                decideFor(m, gs);
            }
            // the enemy prophet
            else if(m.getMinionType().isProphet()) {
                enemyProphet = m;
            }
        }
        minionsUnderControl = tempMinionCounter;
        updateNow = false;
    }

    @Override
    public String getName() {
        return "medium prophet";
    }

    private void decideFor(Minion minion, GameState gs) {
        // if the distance to the enemy prophet is quiet small and I'm not the
        // prophet -> I have to do something!
        if(!evadeEnemyProphet(minion)) {
            if(!evadeChaseEnemyMinion(minion, gs)) {
                // if we dont need to evade the prophet and other minions
                if(isAtTarget(minion)) {
                    if(minion.getMinionType().isProphet()) {
                        prophetsChoice(minion, gs);
                    } else {
                        Rectangle targetField = getWorthwhileGoal(gs, minion);
                        final int x = r.nextInt(targetField.width) + targetField.x;
                        final int y = r.nextInt(targetField.height) + targetField.y;
                        moveActionHandler.handleMoveAction(new MoveAction(minion.getId(), x, y));
                    }
                }
            }
        }
    }

    private void prophetsChoice(Minion prophet, GameState gs) {
        double distance = Double.MAX_VALUE;
        Minion tMinion = null;
        for(Minion other : gs.getMinionMap().values()) {
            if(!other.getFaction().equals(faction)
                    && !other.getMinionType().isProphet()) {
                double dist = other.distanceToMinion(prophet);
                if(dist < distance) {
                    distance = dist;
                    tMinion = other;
                }
            }
        }
        if(tMinion != null) {
            chase(prophet, tMinion);
        }
    }

    private boolean evadeChaseEnemyMinion(Minion minion, GameState gs) {
        // iterate over all enemy minions
        // if one is < 100 units away from me: evade if the current minion would
        // loose 1v1
        if(minion.getMinionType().isProphet())
            return false;
        for(Minion other : gs.getMinionMap().values()) {
            if(EnumSet.complementOf(EnumSet.of(faction, Faction.NEUTRAL)).contains(other.getFaction())) {
                if(other.distanceToMinion(minion) < 350) {
                    // check: would I win 1v1?
                    if((other.getMinionType().equals(MinionType.CHILD))
                            || (other.getMinionType().equals(
                            MinionType.OLD_MAN) && !minion
                            .getMinionType().equals(MinionType.CHILD))
                            || (other.getMinionType().equals(
                            MinionType.YOUNG_MAN) && minion
                            .getMinionType().equals(
                                    MinionType.GOSPEL_WOMAN))) {
                        chase(minion, other);
                        return true;
                    }
                }

                if(other.distanceToMinion(minion) < 100) {
                    // check: would I win 1v1?
                    if((minion.getMinionType().equals(MinionType.CHILD))
                            || (minion.getMinionType().equals(
                            MinionType.OLD_MAN) && !other
                            .getMinionType().equals(MinionType.CHILD))
                            || (minion.getMinionType().equals(
                            MinionType.YOUNG_MAN) && other
                            .getMinionType().equals(
                                    MinionType.GOSPEL_WOMAN))) {
                        evade(minion, other);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private boolean evadeEnemyProphet(Minion minion) {
        if(minion.distanceToMinion(enemyProphet) < 100
                && !minion.getMinionType().isProphet()) {
            // we have to evade the enemy prophet if we are not the OldMen
            if(minion.getMinionType() != MinionType.OLD_MAN) {
                evade(minion, enemyProphet);
                return true;
            } else {
                // evade with OldMan as well if we have less than 5 units
                // else -> Grandpa Tangle (Rentnern)
                if(minionsUnderControl < 5) {
                    evade(minion, enemyProphet);
                    return true;
                }
            }
        }
        return false;
    }

    private void evade(Minion evasiveMinion, Minion enemyMinion) {
        float vectorX = evasiveMinion.getX() - enemyMinion.getX();
        float vectorY = evasiveMinion.getY() - enemyMinion.getY();
        double length = Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));

        double normalizedX = vectorX / length;
        double normalizedY = vectorY / length;

        final int x = (int) (normalizedX * 100.0 + evasiveMinion.getX());
        final int y = (int) (normalizedY * 100.0 + evasiveMinion.getY());
        moveActionHandler.handleMoveAction(new MoveAction(evasiveMinion.getId(), x, y));
    }

    private void chase(Minion chasingMinion, Minion enemyMinion) {
        moveActionHandler.handleMoveAction(
                new MoveAction(chasingMinion.getId(), (int) enemyMinion.getX(), (int) enemyMinion.getY()));
    }

    private boolean isAtTarget(Minion minion) {
        // every second we update our commands regardless if we are at the
        // target
        if(updateNow) {
            return true;
        }
        // normal we check whether we are really at the target
        if((Math.abs(minion.getX() - (float) minion.getTargetPosX()) < 2.0f && Math
                .abs(minion.getY() - (float) minion.getTargetPosY()) < 2.0f)) {
            return true;
        } else {
            return false;
        }
    }

    private Rectangle getWorthwhileGoal(GameState gs, Minion minion) {
        Rectangle position = null;
        float bestValue = Float.MIN_VALUE;
        for(ObjectObjectCursor<GameFieldElementPosition, GameFieldElement> gf : gs
                .getGameField().getGameFieldMap()) {
            float x = gf.value.getField().x + gf.value.getField().width / 2;
            float y = gf.value.getField().y + gf.value.getField().height / 2;
            float distance = (float) Math.sqrt(
                    Math.pow((minion.getX() - x), 2.0f) + Math.pow((minion.getY() - y), 2.0f));
            float value = (float) (gf.value.getDominationState() + (10 - Math.log10(distance)));
            if(value > bestValue) {
                bestValue = value;
                position = gf.value.getField();
            }
        }
        return position;
    }

    public void init(int rows, int columns, Faction assignedFaction, MoveActionHandler moveActionHandler) {
        this.rows = rows;
        this.columns = columns;
        this.faction = assignedFaction;
        this.moveActionHandler = moveActionHandler;
    }
}
