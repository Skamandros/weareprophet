/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.common.communication.message;

import de.ggj14.wap.common.CommonValues;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

/**
 * Requests to switch to authenticated server connection mode.
 */
public class ClientChallengeRequest implements Message {
    private static final Logger LOG = LogManager.getLogger(ClientChallengeRequest.class);

    private byte[] publicKeyEncoded;
    private String playerName;
    private transient final KeyFactory keyFactory;

    /**
     * For deserialization
     */
    public ClientChallengeRequest() throws NoSuchAlgorithmException {
        this.keyFactory = KeyFactory.getInstance(CommonValues.CLIENT_KEY_ALGORITHM);
    }

    public ClientChallengeRequest(PublicKey publicKey, String playerName) throws NoSuchAlgorithmException {
        this();
        this.publicKeyEncoded = publicKey.getEncoded();
        this.playerName = playerName;
    }

    public PublicKey getPublicKey() throws InvalidKeySpecException {
        X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKeyEncoded);
        return keyFactory.generatePublic(spec);
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKeyEncoded = publicKey.getEncoded();
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return "ClientAuthenticationRequest{" +
                "publicKey=" + publicKeyEncoded +
                ", playerName='" + playerName + '\'' +
                '}';
    }
}
