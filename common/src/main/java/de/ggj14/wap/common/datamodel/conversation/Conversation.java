/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.datamodel.conversation;

import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.gamefield.objects.Minion;

import java.util.ArrayList;
import java.util.List;

public class Conversation {
    private boolean greyProphetInvolved;

    private int id;
    private List<Minion> participants;
    private Minion prophet;

    private float convoCenterX;
    private float convoCenterY;

    // player 1 - values
    private int player1_Persuasion;
    private float player1_totalPersuasionDone;
    private int player1_EnemyWillPower;

    // player 2 - values
    private int player2_Persuasion;
    private float player2_totalPersuasionDone;
    private int player2_EnemyWillPower;

    // grey - values
    private int grey_Persuasion;
    private float grey_totalPersuasionDone;
    private int grey_EnemyWillPower;

    /**
     * For deserialization.
     */
    public Conversation() {
    }

    public Conversation(int id) {
        this.id = id;
        participants = new ArrayList<>(CommonValues.MINIONS_AT_MAP);
        prophet = null;
        convoCenterX = Float.MAX_VALUE;
        convoCenterY = Float.MAX_VALUE;
    }

    public List<Minion> getParticipants() {
        return participants;
    }

    private void resetFactionPowers() {
        player1_Persuasion = 0;
        player1_EnemyWillPower = 0;

        player2_Persuasion = 0;
        player2_EnemyWillPower = 0;

        grey_Persuasion = 0;
        grey_EnemyWillPower = 0;
    }

    private void updateFactionPower() {
        resetFactionPowers();
        int greyWillPower = 0;
        for(Minion min : participants) {
            if(min.getFaction() == Faction.PLAYER1) {
                player1_Persuasion += min.getMinionType().getPersuasion();
                player2_EnemyWillPower += min.getMinionType().getWillpower();
                grey_EnemyWillPower += min.getMinionType().getWillpower();
            } else if(min.getFaction() == Faction.PLAYER2) {
                player2_Persuasion += min.getMinionType().getPersuasion();
                player1_EnemyWillPower += min.getMinionType().getWillpower();
                grey_EnemyWillPower += min.getMinionType().getWillpower();
            } else {
                greyWillPower += min.getMinionType().getWillpower();
                grey_Persuasion += min.getMinionType().getPersuasion();
            }
        }
        // reduce willpower of unfactioned members by 50%
        greyWillPower /= 2;
        player1_EnemyWillPower += greyWillPower;
        player2_EnemyWillPower += greyWillPower;

        // if the prophet is part of the conversation and another of his faction you will have
        // int-max + something which will be negative. Here we reset this WillPower to the Prophets will power
        if(player1_EnemyWillPower < 0) player1_EnemyWillPower = Integer.MAX_VALUE;
        if(player2_EnemyWillPower < 0) player2_EnemyWillPower = Integer.MAX_VALUE;
    }

    /**
     * updates the conversation and returns the state of the battle.
     * if Grey faction is returned the battle is still on the way.
     *
     * @param timeDelta
     * @return the
     */
    public ConversationState update(int timeDelta) {
        updateFactionPower();

        // case: grey prophet not involved in th talks
        if(!greyProphetInvolved) {
            // case: two prophet factions are battling, probably with grey, without grey prophet
            if(player1_Persuasion > 0 && player2_Persuasion > 0) {
                player1_totalPersuasionDone += (player1_Persuasion / 1000.0f * timeDelta);
                player2_totalPersuasionDone += (player2_Persuasion / 1000.0f * timeDelta);

                // case: both factions bet the enemy, without grey prophet
                if(player1_totalPersuasionDone > player1_EnemyWillPower
                        && player2_totalPersuasionDone > player2_EnemyWillPower) {

                    if(player1_totalPersuasionDone > player2_totalPersuasionDone) {
                        return ConversationState.PLAYER_1_WINS;
                    } else if(player1_totalPersuasionDone < player2_totalPersuasionDone) {
                        return ConversationState.PLAYER_2_WINS;
                    }
                }
                // case: only player1 wins, without grey prophet
                else if(player1_totalPersuasionDone > player1_EnemyWillPower) {
                    return ConversationState.PLAYER_1_WINS;
                }
                // case: only player2 wins, without grey prophet
                else if(player2_totalPersuasionDone > player2_EnemyWillPower) {
                    return ConversationState.PLAYER_2_WINS;
                }
                // case: player1 faction vs grey, without grey prophet
            } else if(player1_Persuasion > 0) {
                player1_totalPersuasionDone += (player1_Persuasion / 1000.0f * timeDelta);

                if(player1_totalPersuasionDone > player1_EnemyWillPower) {
                    return ConversationState.PLAYER_1_WINS;
                }
                // case: player2 faction vs grey, without grey prophet
            } else if(player2_Persuasion > 0) {
                player2_totalPersuasionDone += (player2_Persuasion / 1000.0f * timeDelta);

                if(player2_totalPersuasionDone > player2_EnemyWillPower) {
                    return ConversationState.PLAYER_2_WINS;
                }
            }
        }
        // case: grey prophet is involved
        else {
            grey_totalPersuasionDone += (grey_Persuasion / 1000.0f * timeDelta);
            if(grey_totalPersuasionDone > grey_EnemyWillPower) {
                return ConversationState.GREY_PROPHET_WINS;
            }
        }
        return ConversationState.BATTLING;
    }


    public void addAll(Conversation otherConv) {
        for(Minion min : otherConv.getParticipants()) {
            addParticipant(min);
        }
    }

    public boolean addParticipant(Minion min) {
        if(min.getMinionType().isProphet()) {
            if(prophet == null) {
                prophet = min;
                greyProphetInvolved = prophet.getFaction().equals(Faction.NEUTRAL);
            } else {
                return false;
            }
        }
        participants.add(min);
        min.dontMove();
        if(participants.size() == 2) {
            convoCenterX = (participants.get(0).getX() + participants.get(1).getX()) / 2;
            convoCenterY = (participants.get(0).getY() + participants.get(1).getY()) / 2;
        }
        return true;
    }

    public double getDistanceToMinion(Minion minion) {
        return Math.sqrt(Math.pow(this.convoCenterX - minion.getX(), 2) + Math.pow(this.convoCenterY - minion.getY(), 2));
    }


    public int getId() {
        return id;
    }

    public ConversationOverview getConversationOverview() {
        float player1Val = 0.0f;
        float player2Val = 0.0f;
        float greyVal = 0.0f;

        // if a prophet is present, the other values could be 0
        if(hasProphet()) {
            if(prophet.getFaction().equals(Faction.PLAYER1))
                player1Val = 1.0f / player1_EnemyWillPower * player1_totalPersuasionDone;
            else if(prophet.getFaction().equals(Faction.PLAYER2))
                player2Val = 1.0f / player2_EnemyWillPower * player2_totalPersuasionDone;
            else if(prophet.getFaction().equals(Faction.NEUTRAL))
                greyVal = 1.0f / grey_EnemyWillPower * grey_totalPersuasionDone;
        } else {
            if(player1_Persuasion > 0) player1Val = 1.0f / player1_EnemyWillPower * player1_totalPersuasionDone;
            if(player2_Persuasion > 0) player2Val = 1.0f / player2_EnemyWillPower * player2_totalPersuasionDone;
        }

        return new ConversationOverview(Math.min(player1Val, 1.0f), Math.min(player2Val, 1.0f), Math.min(greyVal, 1.0f));
    }

    public boolean hasProphet() {
        return prophet != null;
    }

    public float getConvoCenterX() {
        return convoCenterX;
    }

    public float getConvoCenterY() {
        return convoCenterY;
    }
}