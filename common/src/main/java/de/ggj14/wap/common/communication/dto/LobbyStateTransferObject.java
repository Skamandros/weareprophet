/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package de.ggj14.wap.common.communication.dto;

import de.ggj14.wap.common.datamodel.MatchMode;
import de.ggj14.wap.common.datamodel.lobby.LobbyPhase;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Represents the state of a game lobby.
 */
public class LobbyStateTransferObject {

    private int lobbyId = GameInformation.NO_GAME_ID;
    private LobbyPhase currentPhase = LobbyPhase.SETUP;
    private int scoreP1 = 0;
    private int scoreP2 = 0;
    private String p1Name;
    private String p2Name;
    private SortedSet<String> observers = new TreeSet<>();
    private LobbySettings settings = new LobbySettings(MatchMode.SINGLE_GAME, true);
    private GameInformation gameInformation = new GameInformation();
    private boolean player2ready = false;

    public LobbyStateTransferObject() {
    }

    /**
     * Copy constructor for defensive copies.
     *
     * @param state The state to copy.
     */
    public LobbyStateTransferObject(LobbyStateTransferObject state) {
        this.lobbyId = state.lobbyId;
        this.currentPhase = state.currentPhase;
        this.scoreP1 = state.scoreP1;
        this.scoreP2 = state.scoreP2;
        this.p1Name = state.p1Name;
        this.p2Name = state.p2Name;
        this.observers = new TreeSet<>(state.observers);
        this.settings = new LobbySettings(state.settings);
        this.gameInformation = new GameInformation(state.gameInformation);
        this.player2ready = state.player2ready;
    }

    public String getP1Name() {
        return p1Name;
    }

    public void setP1Name(String p1Name) {
        this.p1Name = p1Name;
    }

    public String getP2Name() {
        return p2Name;
    }

    public void setP2Name(String p2Name) {
        this.p2Name = p2Name;
    }

    public SortedSet<String> getObservers() {
        return observers;
    }

    public void setObservers(SortedSet<String> observers) {
        this.observers = observers;
    }

    public int getScoreP1() {
        return scoreP1;
    }

    public void setScoreP1(int scoreP1) {
        this.scoreP1 = scoreP1;
    }

    public int getScoreP2() {
        return scoreP2;
    }

    public void setScoreP2(int scoreP2) {
        this.scoreP2 = scoreP2;
    }

    public boolean isPlayer2ready() {
        return player2ready;
    }

    public void setPlayer2ready(boolean player2ready) {
        this.player2ready = player2ready;
    }

    public LobbyPhase getCurrentPhase() {
        return currentPhase;
    }

    public void setCurrentPhase(LobbyPhase currentPhase) {
        this.currentPhase = currentPhase;
    }

    public int getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(int lobbyId) {
        this.lobbyId = lobbyId;
    }

    public GameInformation getGameInformation() {
        return gameInformation;
    }

    public LobbySettings getSettings() {
        return settings;
    }

    public void setSettings(LobbySettings settings) {
        this.settings = settings;
    }

    public void setGameInformation(GameInformation gameInformation) {
        this.gameInformation = gameInformation;
    }

    @Override
    public String toString() {
        return "LobbyStateTransferObject{" +
                "lobbyId=" + lobbyId +
                ", currentPhase=" + currentPhase +
                ", scoreP1=" + scoreP1 +
                ", scoreP2=" + scoreP2 +
                ", p1Name='" + p1Name + '\'' +
                ", p2Name='" + p2Name + '\'' +
                ", observers=" + observers +
                ", settings=" + settings +
                ", gameInformation=" + gameInformation +
                ", player2ready=" + player2ready +
                '}';
    }
}
