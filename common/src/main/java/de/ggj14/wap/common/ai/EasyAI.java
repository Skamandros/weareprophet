/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.ai;

import de.ggj14.wap.common.CommonValues;
import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.gamefield.MoveActionHandler;
import de.ggj14.wap.common.gamefield.objects.Minion;

import java.util.Random;

public class EasyAI implements AI {

    private int rows;
    private int columns;
    private MoveActionHandler moveActionHandler;
    private Faction faction;

    public void update(GameState gs) {
        for(Minion m : gs.getMinionMap().values()) {
            if(m.getFaction().equals(faction)) {
                Random r = new Random();
                if((Math.abs(m.getX() - (float) m.getTargetPosX()) < 2.0f
                        && Math.abs(m.getY() - (float) m.getTargetPosY()) < 2.0f)
                        || r.nextFloat() < 0.0002f) {
                    final int x = r.nextInt(columns * CommonValues.WIDTH_OF_MAP_ELEMENT);
                    final int y = r.nextInt(rows * CommonValues.HEIGHT_OF_MAP_ELEMENT);
                    moveActionHandler.handleMoveAction(new MoveAction(m.getId(), x, y));
                }
            }
        }
    }

    @Override
    public String getName() {
        return "easy prophet";
    }

    public void init(int rows, int columns, Faction assignedFaction, MoveActionHandler moveActionHandler) {
        this.rows = rows;
        this.columns = columns;
        this.faction = assignedFaction;
        this.moveActionHandler = moveActionHandler;
    }

}
