/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.gamefield;

import com.carrotsearch.hppc.ObjectIntHashMap;
import com.carrotsearch.hppc.ObjectObjectHashMap;
import com.carrotsearch.hppc.cursors.ObjectIntCursor;
import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import de.ggj14.wap.common.communication.dto.GameFieldTransferObject;
import de.ggj14.wap.common.datamodel.Faction;
import de.ggj14.wap.common.datamodel.GameFieldElementPosition;

import java.util.ArrayList;
import java.util.List;

public final class GameField {
    private int rows;
    private int columns;
    private ObjectObjectHashMap<GameFieldElementPosition, GameFieldElement> gameFieldMap;
    private final transient ObjectIntHashMap<Faction> gameFieldFactionCountMap;

    private transient volatile int numberOfLockedGameFieldElements = 0;

    public GameField() {
        gameFieldFactionCountMap = new ObjectIntHashMap<>();
    }

    public void init(int rows, int columns) {
        gameFieldMap = new ObjectObjectHashMap<>();

        this.rows = rows;
        this.columns = columns;
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                GameFieldElementPosition gfep = new GameFieldElementPosition(i, j);
                GameFieldElement gfe = new GameFieldElement(i, j);
                gameFieldMap.put(gfep, gfe);
            }
        }
        gameFieldFactionCountMap.clear();
    }

    public GameFieldElement getGameFieldElement(int row, int column) {
        GameFieldElementPosition gfep = new GameFieldElementPosition(row, column);
        return gameFieldMap.get(gfep);
    }

    public void update(int timeDelta) {
        gameFieldFactionCountMap.clear();
        int lockedElements = 0;

        for(ObjectObjectCursor<GameFieldElementPosition, GameFieldElement> field : gameFieldMap) {
            if(field.value.isGameFieldLocked()) {
                lockedElements++;
                continue;
            }

            field.value.calculateDominatingFaction(timeDelta);
            // add 1 to the corresponding faction
            gameFieldFactionCountMap.putOrAdd(field.value.getFieldFaction(), 1, 1);
        }
        this.numberOfLockedGameFieldElements = lockedElements;
    }

    public void print() {
        ObjectIntHashMap<Faction> countMap = new ObjectIntHashMap<Faction>();
        for(ObjectObjectCursor<GameFieldElementPosition, GameFieldElement> gfe : gameFieldMap) {
            countMap.putOrAdd(gfe.value.getFieldFaction(), 1, 1);
        }
        for(ObjectIntCursor<Faction> counts : countMap) {
            System.out.println("Members of " + counts.key.name() + "		: " + counts.value);
        }
    }

    public ObjectIntHashMap<Faction> getGameFieldFactionCountMap() {
        return gameFieldFactionCountMap;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public ObjectObjectHashMap<GameFieldElementPosition, GameFieldElement> getGameFieldMap() {
        return gameFieldMap;
    }

    public void setGameFieldMap(
            ObjectObjectHashMap<GameFieldElementPosition, GameFieldElement> gameFieldMap) {
        this.gameFieldMap = gameFieldMap;
    }

    public GameFieldTransferObject getGameFieldTransferObject() {

        List<GameFieldElementPosition> gfep = new ArrayList<GameFieldElementPosition>();
        List<GameFieldElement> gfe = new ArrayList<GameFieldElement>();
        for(ObjectObjectCursor<GameFieldElementPosition, GameFieldElement> gfme : gameFieldMap) {
            gfep.add(gfme.key);
            gfe.add(gfme.value);
        }
        return new GameFieldTransferObject(gfep, gfe, rows, columns);
    }

    /**
     * Transfers the DTO to the Object itself.
     */
    public static GameField fromGameFieldTransferObject(GameFieldTransferObject gfto) {
        GameField gf = new GameField();
        gf.setColumns(gfto.columns());
        gf.setRows(gfto.rows());

        ObjectObjectHashMap<GameFieldElementPosition, GameFieldElement> gameFieldMap =
                new ObjectObjectHashMap<GameFieldElementPosition, GameFieldElement>();
        for(int i = 0; i < gfto.gameFieldElements().size(); i++) {
            gameFieldMap.put(gfto.gameFieldElementPositions().get(i),
                    gfto.gameFieldElements().get(i));
        }
        gf.setGameFieldMap(gameFieldMap);

        return gf;
    }

    public int getNumberOfLockedGameFieldElements() {
        return numberOfLockedGameFieldElements;
    }
}
