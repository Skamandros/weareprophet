/*
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2014 Rick Hoppmann
 *  Copyright (c) 2014 Marius Schneider
 *  Copyright (c) 2023 Stefan Herbst
 *  Copyright (c) 2023-2024 André Fichtner-Winkler
 *  Copyright (c) 2014-2024 Martin Grimmer
 *  Copyright (c) 2014-2024 Matthias Kricke
 *  Copyright (c) 2014-2024 Michael Schmeißer
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package de.ggj14.wap.common.communication;

import de.ggj14.wap.common.communication.message.MoveAction;
import de.ggj14.wap.common.datamodel.GameState;
import de.ggj14.wap.common.datamodel.GameStateProvider;
import de.ggj14.wap.common.gamefield.objects.Minion;
import de.ggj14.wap.common.server.ProphetConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * <p>
 * This class provides smooth game state updates and asynchronous move action
 * handling.
 * </p>
 * <p>
 * When using this class, please follow these guidelines:
 * <ul>
 * <li>The method {@link #getGameState()} returns the best-known real game state
 * when it is called. The object returned by this method may be modified. As
 * soon as a new server state is acquired, best effort is tried to merge it with
 * the previous game state to avoid game state leaps. However, game states
 * already returned to the user before are not modified. Therefore, users of
 * this class must not keep the game state returned for any longer period of
 * time, but rather regularly get the recent game state using
 * {@link #getGameState()}.</li>
 * <li>Whenever sending a move action, use this class'
 * {@link #sendMoveAction(MoveAction)} method.</li>
 * <li>Objects of this class keep thread pools and <strong>must be
 * closed</strong> with {@link #close()}.</li>
 * <li><strong>Do not reuse</strong> instances of this class.</li>
 * </ul>
 * </p>
 *
 * @author SharkOfMetal
 */
public final class ServerCommunicationHelper implements Closeable, GameStateProvider, ProphetConnector.GameStateUpdateListener {

    private static final Logger LOG = LogManager.getLogger(ServerCommunicationHelper.class);
    private static final double CATCHUP_DISTANCE = 30;
    private static final double TARGET_EPSILON = 3;
    private final ProphetConnector psc;
    private final ExecutorService executor;

    private final AtomicReference<GameState> gameState;
    private final AtomicLong lastServerGameStateTime = new AtomicLong(0);

    public ServerCommunicationHelper(final ProphetConnector connector) throws InterruptedException {
        this.psc = connector;
        this.gameState = new AtomicReference<>(connector.getCurrentGameState());
        this.psc.addListener(this);
        executor = Executors.newFixedThreadPool(6);
    }

    private void mergeGameStates(GameState serverState) {
        final long lastState = lastServerGameStateTime.get();
        if(serverState.getTime() <= lastState && lastState != 0L) {
            return;
        }
        for(Minion minion : serverState.getMinionMap().values()) {
            final Minion localMinionState = getGameState().getMinionMap().get(minion.getId());
            if(localMinionState.distanceToMinion(minion) > CATCHUP_DISTANCE) {
                // If there has been a lag, we speed up minions to their correct position
                minion.setTargetPosX((int) minion.getX());
                minion.setTargetPosY((int) minion.getY());

                minion.speedBuff();
            } else if(localMinionState.distanceToMinion(minion) > TARGET_EPSILON && !minion.canMove()) {
                // After a conversion starts, local representations may still catch up if too far away
                minion.setTargetPosX((int) minion.getX());
                minion.setTargetPosY((int) minion.getY());

                minion.moveAgain();
            }
            // this is needed to make minions move smoothly in the local representation
            minion.setX(localMinionState.getX());
            minion.setY(localMinionState.getY());
        }
        if(lastServerGameStateTime.compareAndExchange(lastState, serverState.getTime()) == lastState) {
            gameState.set(serverState);
        }
    }

    @Override
    public void update(GameState receivedFromServer, boolean firstUpdate) {
        if(firstUpdate) {
            gameState.set(receivedFromServer);
        } else {
            executor.submit(() -> {
                this.mergeGameStates(receivedFromServer);
            });
        }
    }

    /**
     * async send move action
     */
    private class SendMoveAction implements Runnable {
        private final MoveAction moveActionToSend;

        SendMoveAction(final MoveAction moveActionToSend) {
            this.moveActionToSend = moveActionToSend;
        }


        public void run() {
            try {
                psc.handleMoveAction(moveActionToSend);
            } catch(Exception e) {
                LOG.error("Move action could not be sent", e);
            }

        }
    }

    public void sendMoveAction(MoveAction toSend) {
        executor.submit(new SendMoveAction(toSend));
    }

    public GameState getGameState() {
        return gameState.get();
    }

    @Override
    public void close() {
        this.executor.shutdownNow();
        this.psc.removeListener(this);
    }

}
