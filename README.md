We are Prophet
==============

[Visit our official website](https://weareprophet.de)

This Real-Time-Online-Multiplayergame is about persuading other people to not see the things as they are, but to see the
things as we are. You battle another human player for becoming the greatest persuader. To do so, you try to spread your
influence to the given sectors. But players can pull neutral people to their sides too... It's your choice what tactic
you will use to spread your influence to everyone.

Module overview
--------

```plantuml
package "LibGDX Client" {
    [core]
    [desktop]
}
[server] ..> [common] : depend
[core] ..> [common] : depend
[desktop] ..> [core] : depend
```

Instructions
------------
First, clone this repository. You will find the most recent stable release on the master branch and the latest beta release on the develop branch.

Starting the server
-------------------
To start the server, execute the following Gradle task:

    ./gradlew :server:run

Starting the client
-------------------
To start the client, execute the following Gradle task:

    ./gradlew :desktop:run

Builing a client bundle
-----------------------
To build a client bundle (with a JRE and an executable), please download the latest 3.x release of [packr](https://github.com/libgdx/packr) and run:

    ./gradlew clean build
    cd desktop
    java -jar /path/to/packr-all-3.0.3.jar build/resources/main/packr-win64.json

You will then find the result in the *build/packr-win64* directory of the *desktop* submodule.

If you want, you can change the executable icon to the prophet logo with [rcedit](https://github.com/electron/rcedit):

    /path/to/rcedit-x64.exe desktop/build/packr-win64/weareprophet-client.exe --set-icon core/assets/wap-logo.ico


Current Maintainer
------------------
Michael Schmeißer

Active contributors
-------------------
* André Fichtner-Winkler
* Matthias Kricke

Original authors and contributors
---------------------------------
Created during the Global Game Jam 2014 (http://globalgamejam.org/2014/games/prophet)

* Marius Schneider
* Martin Grimmer
* Matthias Kricke
* Michael Schmeißer
* Rick Hoppmann

Special thanks go to
--------------------
* NoArtistAvailable, for the nice prophet-design
* Rat King, for managing the Jam-Location
* HTWK Leipzig, for the warm rooms and the big boards
* [Raymond "Raeleus" Buckley](https://www.badlogicgames.com/forum/viewtopic.php?f=22&t=21568) for the LibGDX Shade UI
    Skin
* Basislager Coworking Leipzig for hosting the GGJ 2023 jam site
* [ElevenLabs](https://elevenlabs.io/) Speech Synthesis

and especially to every prejudice, that may or may not be interpreted while playing :)